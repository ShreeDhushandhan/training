function hideAndSeek() {
  var addButton = document.getElementById('addEmployeeButton');
  if (addButton.style.display === 'block') {
    addButton.style.display = 'none';
  } else {
    addButton.style.display = 'block';
  }
}

function validate() {
  var nameField = document.getElementById('nameField').value;
  var areaField = document.getElementById('areaField').value;

  if (nameField === '' || nameField === null) {
    if (areaField === '' || areaField === null) {
      alert('Name and Area cannot be Empty or null');
    }
  } else {
    // document.getElementById('tab1Name').innerHTML = nameField;
    // document.getElementById('tab1Area').innerHTML = areaField;
    addEmployee();
  }
}


function employeeHide() {
  var container = document.getElementById('employeeContainer');

  if (container.style.display === 'block') {
    container.style.display = 'none';
  } else {
    container.style.display = 'block';
  }
}
