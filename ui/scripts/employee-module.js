import {doGetAll, doCreateEmployee, doDeleteEmployee} from './employee-service.js';

function displayer (data) {
    let employeeGrid = document.getElementById('grid-container');
    let employeeName = document.createElement('div');
    let employeeArea = document.createElement('div');
    let employeeId = document.createElement('div');
    let employeeActions = document.createElement('div');

    //Declaration of Table Columns//
    employeeGrid.innerHTML = '';
    employeeId.innerHTML = 'ID';
    employeeName.innerHTML = 'Name';
    employeeArea.innerHTML = 'Area';
    employeeActions.innerHTML = 'Actions';

    //Name Header and item//
    employeeName.classList.add('grid-header');
    employeeName.classList.add('grid-item');

    //Area Header and item//
    employeeArea.classList.add('grid-header');
    employeeArea.classList.add('grid-item');

    //ID Header and item//
    employeeId.classList.add('grid-header');
    employeeId.classList.add('grid-item');

    //Actions Header and item//
    employeeActions.classList.add('grid-header');
    employeeActions.classList.add('grid-item');


    //Adding the Table Columns//
    employeeGrid.appendChild(employeeId);
    employeeGrid.appendChild(employeeName);
    employeeGrid.appendChild(employeeArea);
    employeeGrid.appendChild(employeeActions);


    data.forEach((employee) => {
        let id = document.createElement('div');
        let name = document.createElement('div');
        let area = document.createElement('div');
        let actions = document.createElement('div');
        let deleteButton = document.createElement('button');

        name.classList.add('grid-container');
        id.classList.add('grid-item');
        area.classList.add('grid-item');
        actions.classList.add('grid-item');

        id.innerHTML = employee.id;
        name.innerHTML = employee.name;
        area.innerHTML = employee.area;
        deleteButton.innerHTML = 'Delete';
        id.onclick = () => {
            document.getElementById('id').innerHTML = employee.id;
            document.getElementById('name').innerHTML = employee.name;
            document.getElementById('area').innerHTML = employee.area;
        };
        deleteButton.onclick = () => doDeleteEmployee(employee.id);

        actions.appendChild(deleteButton);
        employeeGrid.appendChild(id);
        employeeGrid.appendChild(name);
        employeeGrid.appendChild(area);
        employeeGrid.appendChild(actions);
    });

}

function addEmployee() {
    let employeeDataGot = {};
    doCreateEmployee(employeeDataGot);
}


export default {displayer, addEmployee};
