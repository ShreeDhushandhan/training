const url = "localhost:3004/students";

doGetAll = () => {
    const http = XMLHttpRequest();
    http.open('GET', url, false);
    http.onreadystatechange = function() {
        if(this.readyState !== 4 && this.status !== 200) {return};
        displayer(JSON.parse(http.responseText));
    };
    http.send();
}

doDeleteEmployee = (id) => {
    let responseText;
    const http = XMLHttpRequest();
    http.open('DELETE', url, true);
    http.onreadystatechange = () => {
        if(this.readyState !==4 && this.status !== 200) {return};
        responseText = this.responseText;
    };
    http.send();
    getAll();
}

doCreateEmployee = (data) => {
    let jsonData = JSON.stringify(data);
    let responseText;
    const http = XMLHttpRequest();
    http.open('POST', url, true);
    http.onreadystatechange = () => {
        if(this.readyState !== 4 && this.status !== 200) {return};
        responseText = this.responseText;
    }
    http.send(jsonData);
}


export default {doGetAll, doCreateEmployee, doDeleteEmployee};