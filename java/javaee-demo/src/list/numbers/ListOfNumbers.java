package list.numbers;

import java.util.Scanner;
/**
 * Problem Statement 
 * 1.Write a program ListOfNumbers (using try and catch block)
 * 
 * Requirement
 * 1.Write a program ListOfNumbers (using try and catch block)
 * 
 * Method Signature 
 * 
 * Entity
 * 1.ListOfNumbers 
 * 
 * Jobs to be Done
 * 1.Create a integer List and passed some values to it..
 * 2.Get a value from the user to check whether the value is in list or not.
 * 3.Here if the value entered by the user is valid inside the list then it prints the value 
 * 4.else throw the ArrayIndexOutOfBound Exception .
 *  
 * Pseudo Code
 * class ListOfNumbers {
 *	
 *	public static void main(String[] args) {
 *		
 *		try {
 *			int[] ListNumbers = {12,134,34,90};
 *			System.out.println("Enter the Number to Find from the List"); 
 *			Scanner scan = new Scanner(System.in);
 *			int value = scan.nextInt();
 *			System.out.println(ListNumbers[value]);
 *		} catch(ArrayIndexOutOfBoundsException e) {
 *			System.out.println("List Size is not much Bigger");
 *		}
 *	} 
 *
 *}
 * @author UKSD
 *
 */
public class ListOfNumbers {
	
	public static void main(String[] args) {
		
		try {
			int[] ListNumbers = {12,134,34,90};
			System.out.println("Enter the Number to Find from the List");
			Scanner scan = new Scanner(System.in);
			int value = scan.nextInt();
			System.out.println(ListNumbers[value]);
		} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("List Size is not much Bigger");
		}
	}

}
