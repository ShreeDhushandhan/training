package list.exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Iterator;

/**
 * Problem Statement
 *  Create a list
 *    => Add 10 values in the list
 *    => Create another list and perform addAll() method with it
 *    => Find the index of some value with indexOf() and lastIndexOf()
 *    => Print the values in the list using 
 *        - For loop
 *        - For Each
 *        - Iterator
 *        - Stream API
 *    => Convert the list to a set
 *    => Convert the list to a array
 *  Explain about contains(), subList(), retainAll() and give example
 *  
 *  
 *  Requirement
 *  Create a list
 *    => Add 10 values in the list
 *    => Create another list and perform addAll() method with it
 *    => Find the index of some value with indexOf() and lastIndexOf()
 *    => Print the values in the list using 
 *        - For loop
 *        - For Each
 *        - Iterator
 *        - Stream API
 *    => Convert the list to a set
 *    => Convert the list to a array
 *  Explain about contains(), subList(), retainAll() and give example
 *  
 * Method Signature
 *  
 * Entity
 * 1.ListExercise 
 * 
 * Jobs to be Done
 * 1.Create a new List and add some values
 * 2.Create another list and add some values
 * 3.Add all the items of list 1 to list 2
 * 4.get the input from user 
 * 5.Find the index of the input value from the list
 * 6.Find the last index value in the list.
 * 7.Iterate the list and print the values of the list using different methods
 * 8.Convert the List to Array
 * 9.Convert the List to Set
 * 10.Get the value from the user and check the value is present in the list or not
 * 	10.1)If the value is present in the list then print the value
 * 	10.2)If not print the value does not exist.
 * 11.Get the two values as range from the user
 * 12.print the list of items only inclusive of the range  
 *   
 * 
 * @author UKSD
 *
 */



public class ListExercise {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		//First List
		List<String> firstList = new ArrayList<String>();
		
		firstList.add("Apple");
		firstList.add("Samsung");
		firstList.add("Blackberry");
		firstList.add("Oppo");
		firstList.add("Vivo");
		firstList.add("Sony");
		firstList.add("Micromax");
		firstList.add("Lava");
		firstList.add("Moto");
		firstList.add("One Plus");
		firstList.add("One Plus");
		
		//Second List
		List<String> secList = new ArrayList<String>();
		
		secList.add("Rog");
		secList.add("Samsung Fold");
		secList.add("Ipad");
		secList.add("Tablet");
		secList.add("Macbook Air");
		secList.add("Vivobook");
		
		//addAll method
		firstList.addAll(secList);
		
		
		//IndexOf() and lastIndexOf()
		System.out.println("Enter the Value to Find");
		String valueIn = scan.nextLine();
		System.out.println(firstList.indexOf(valueIn));
		
		//lastIndexOf()
		System.out.println("Enter the Duplicate value to find the last occurance");
		String dupliValue = scan.nextLine();
		System.out.println(firstList.lastIndexOf(dupliValue));
		
		
		
		//Printing the Values
		//For Loop
		System.out.println("Iterating using For Loop");
		for(int i =0;i<firstList.size();i++) {
			System.out.println(firstList.get(i));
		}
		
		
		//For each loop
		System.out.println("Iterating using For each loop");
		
		for(String val:secList) {
			System.out.println(val);
		}
		
		
		//Using Iterator
		System.out.println("Iterating using Iterator");
		
		Iterator it = secList.iterator();
		
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		
		
		//Using Stream API
		
		List<Integer> numValue = new ArrayList<Integer>();
		numValue.add(1);
		numValue.add(14);
		numValue.add(18);
		numValue.add(11);
		numValue.add(11);
		numValue.add(10);
		
		System.out.println(numValue);
		
		List<Integer> marks = numValue.stream().map(x->x+x).collect(Collectors.toList());
		System.out.println(marks);
		
		List<Integer> findValue = numValue.stream().filter(s->s.equals(99)).collect(Collectors.toList());
		
		System.out.println(findValue);
		
		//Iterating using Stream API
		
		numValue.stream().forEach(y->System.out.println(y));
		
		//List to Set
		System.out.println("List to Set");
		Set<Integer> newSet = numValue.stream().collect(Collectors.toSet());
		
		for(Integer a: newSet) {
			System.out.println(a);
		}
		
		//List to Array
		System.out.println("List to Array");
		Integer[] intArr = numValue.toArray(new Integer[0]);
		
		for(Integer f : intArr) {
			System.out.println(f);
		}
		
		// Contains(), subList(), retainAll() Methods
		
		//Contains
		if(numValue.contains(11)) {
			System.out.println("11 Exists");
		} else {
			System.out.println("11 does not Exists");
		}
		//subList
		List<Integer> subLists = numValue.subList(2, 5);
		System.out.println(subLists);
		
		
		//retainAll
		
		List<String> listOne = new ArrayList<String>();
		listOne.add("Orange");
		listOne.add("Apple");
		listOne.add("Muskmelon");
		
		List<String> listTwo = new ArrayList<String>();
		listTwo.add("Apple");
		listTwo.add("Grape");
		listTwo.add("Orange");
		listTwo.add("JackFruit");
		
		listTwo.retainAll(listOne);
		
		listTwo.stream().forEach(s->System.out.println(s));
		//scan.close();
		
	}

}
