package stack.exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;



/**
 * Problem Statement
 * 1.Create a stack using generic type and implement
 * -> Push at least 5 elements
 * -> Pop the peek element
 * -> search a element in stack and print index value
 * -> print the size of stack
 * -> print the elements using Stream
 * 
 * Requirement
 * 1.Create a stack using generic type and implement
 * -> Push at least 5 elements
 * -> Pop the peek element
 * -> search a element in stack and print index value
 * -> print the size of stack
 * -> print the elements using Stream
 * 
 * Method Signature
 * 1.public <S> void pushIn(Stack<S> stack ,S inValue);
 * 2.public <S> void popOut(Stack<S> stack);
 * 3.public <S> void peekTop(Stack<S> stack);
 * 4.public <S> boolean isStackEmpty(Stack<S> stack);
 * 5.public <S> void searchStack(Stack<S> stack, S findValue);
 * 6.public <S> int sizeOfStack(Stack<S> stacklist);
 * 
 * Entity
 * 1.StackExcercise 
 * 
 * Jobs to be Done
 * 1. Create a new Stack called newStack of NewStack Generic type of Integer.
 * 2. Now push the values into the Stack with the method pushIn created before in the NewStack Class.
 * 3. Then remove the Element with the PopOut method created before in the NewStack class,
 * 		3.1)The top most element is removed from the Stack and it is printed.
 * 4. Then view the element in the Top Level with the Peek method created before. 
 * 		4.1)This method shows the Top Element of the Stack to the console.
 * 5. The Search method is used to search for the index value of the specified value to the method in the Stack. 
 * 		5.1)Print the Searched Value.
 * 6.Iterate the Stack and print the values.
 * 
 * Pseudo Code
 * class StackMain {
 *	
 *	public <S> void pushIn(Stack<S> stack,S inValue) {
 *		stack.push(inValue);
 *	}
 *	
 *	public <S> void popOut(Stack<S> stack) {
 *		System.out.println("Popped : "+stack.pop());
 *	}
 *	
 *	public <S> void peekTop(Stack<S> stack) {
 *		System.out.println(stack.peek());
 *	}
 *	
 *	public <S> boolean isStackEmpty(Stack<S> stack) {
 *		if(!stack.isEmpty()) {
 *			return false;
 *		} else {
 *			return true;
 *		}
 *		
 *	}
 *	
 *	public <S> void searchStack(Stack<S> stack, S findValue) {
 *		if(stack.contains(findValue)) {
 *			System.out.println(findValue + " Exists");
 *		} else {
 *			System.out.println(findValue + "Does not Exists");
 *		}
 *	}
 *	
 *	public <S> int sizeOfStack(Stack<S> stacklist) {
 *		return stacklist.size();
 *	}
 *	
 *	public static void main(String[] args) {
 *		
 *		try {
 *			
 *			StackMain stackMain = new StackMain();
 *			Scanner scan = new Scanner(System.in);
 *			//Generic Stack Creation
 *			Stack<Integer> newStack = new Stack<Integer>();
 *			
 *			//Pushing Elements into Stack
 *			stackMain.pushIn(newStack, 60);
 *			stackMain.pushIn(newStack, 50);
 *			stackMain.pushIn(newStack, 80);
 *			stackMain.pushIn(newStack, 70);
 *			stackMain.pushIn(newStack, 10);
 *			
 *			//Pop Element Removes the Top most Element in Stack 
 *			stackMain.popOut(newStack);
 *			
 *			//Peek shows the Top Element in Stack
 *			stackMain.peekTop(newStack);
 *			
 *			//Index Value of a Value in the Stack
 *			//System.out.println("Enter the Value to Find the Index");
 *			//Integer valueIn = scan.netInt();
 *			stackMain.searchStack(newStack, 70);
 *			
 *			//Size of the Stack
 *			stackMain.sizeOfStack(newStack);
 *			
 *			//Printing the Elements using Stream
 *			newStack.stream().forEach(x -> System.out.println(x));
 *			
 *		} catch (ArrayIndexOutOfBoundsException e ) {
 *			System.out.println("End of Stack");
 *		}
 *		
 *	}
 *
 *}
 * @author UKSD
 *
 */
public class StackMain {
	
	public <S> void pushIn(Stack<S> stack,S inValue) {
		stack.push(inValue);
	}
	
	public <S> void popOut(Stack<S> stack) {
		System.out.println("Popped : "+stack.pop());
	}
	
	public <S> void peekTop(Stack<S> stack) {
		System.out.println(stack.peek());
	}
	
	public <S> boolean isStackEmpty(Stack<S> stack) {
		if(!stack.isEmpty()) {
			return false;
		} else {
			return true;
		}
		
	}
	
	public <S> void searchStack(Stack<S> stack, S findValue) {
		if(stack.contains(findValue)) {
			System.out.println(findValue + " Exists");
		} else {
			System.out.println(findValue + "Does not Exists");
		}
	}
	
	public <S> int sizeOfStack(Stack<S> stacklist) {
		return stacklist.size();
	}
	
	public static void main(String[] args) {
		
		try {
			
			StackMain stackMain = new StackMain();
			Scanner scan = new Scanner(System.in);
			//Generic Stack Creation
			Stack<Integer> newStack = new Stack<Integer>();
			
			//Pushing Elements into Stack
			stackMain.pushIn(newStack, 60);
			stackMain.pushIn(newStack, 50);
			stackMain.pushIn(newStack, 80);
			stackMain.pushIn(newStack, 70);
			stackMain.pushIn(newStack, 10);
			
			//Pop Element Removes the Top most Element in Stack 
			stackMain.popOut(newStack);
			
			//Peek shows the Top Element in Stack
			stackMain.peekTop(newStack);
			
			//Index Value of a Value in the Stack
			//System.out.println("Enter the Value to Find the Index");
			//Integer valueIn = scan.netInt();
			stackMain.searchStack(newStack, 70);
			
			//Size of the Stack
			stackMain.sizeOfStack(newStack);
			
			//Printing the Elements using Stream
			newStack.stream().forEach(x -> System.out.println(x));
			
			
			
		} catch (ArrayIndexOutOfBoundsException e ) {
			System.out.println("End of Stack");
		}
		
		
		
	}

}
