package stack.exercise;

import java.util.List;
import java.util.Stack;

/**
 * Entity
 * 1.StackFunctions 
 * 
 * 
 * @author UKSD
 *
 * @param <S>
 */
public interface StackFunctions<S> {
	
	public <S> void pushIn(Stack<S> stack ,S inValue);
	
	public <S> void popOut(Stack<S> stack);
	
	public <S> void peekTop(Stack<S> stack);
	
	public <S> boolean isStackEmpty(Stack<S> stack);
	
	public <S> void searchStack(Stack<S> stack, S findValue);
	
	public <S> int sizeOfStack(Stack<S> stacklist);
	


}
