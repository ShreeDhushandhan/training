package time.date;

import java.time.Month;
import java.time.Year;
import java.util.Scanner;

/**
 * Problem Statement
 * 1.Write an example that, for a given year, reports the length of each month within that particular year.
 * 2.Which class would you use to store your birthday in years, months, days, seconds, and nanoseconds
 * 
 * Requirement
 * 1.Write an example that, for a given year, reports the length of each month within that particular year.
 * 2.Which class would you use to store your birthday in years, months, days, seconds, and nanoseconds
 * 
 * Entity
 * 1.MonthLengthFinder
 * 
 * Method Signature
 * 1.public static int yearLength(Year year);
 * 2.public static int monthLength(Month month);
 * 
 * Jobs to be Done
 * 1.Get the month from the User.
 * 2.Pass the month to the monthLength method
 * 3.In monthLength return the maxLength of the month
 * 4.Store the current year to the yearNow
 * 5.Pass the yearNow to the yearLength method
 * 6.In yearLength method return the length of the year.
 * 7.Print  
 * 3.Print the length of the month
 * 
 * Pseudo Code
 * class MonthLengthFinder {
 *	 
 *	public static int yearLength(Year year) {
 *		return year.length();
 *	}
 *	
 *	public static int monthLength(Month month) {
 *		return month.maxLength();
 *	}
 *	
 *	public static void main(String[] args ) {
 *		
 *		Year yearNow = Year.of(2020);
 *		Scanner scanner = new Scanner(System.in);
 *		System.out.println("Enter the Month..");
 *		int monthIn = scanner.nextInt();
 *		Month month = Month.of(monthIn);
 *		
 *		System.out.println(MonthLengthFinder.yearLength(yearNow));
 *		System.out.println(MonthLengthFinder.monthLength(month));
 *	} 
 *
 *}
 *
 * Answer:
 * 2.LocalDateTime Class is used to store the years, months, days, seconds and nanoseconds
 *
 * 
 * @author UKSD
 *
 */

public class MonthLengthFinder {
	
	public static int yearLength(Year year) {
		return year.length();
	}
	
	public static int monthLength(Month month) {
		return month.maxLength();
	}
	
	public static void main(String[] args ) {
		
		Year yearNow = Year.of(2020);
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Month..");
		int monthIn = scanner.nextInt();
		Month month = Month.of(monthIn);
		
		System.out.println(MonthLengthFinder.yearLength(yearNow));
		System.out.println(MonthLengthFinder.monthLength(month));
	}

}
