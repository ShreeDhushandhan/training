package time.date;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

/**
 * Problem Statement
 * 1. Given a random date, how would you find the date of the previous Friday?
 * 
 * Requirement
 * 1. Given a random date, how would you find the date of the previous Friday?
 * 
 * Method Signature
 * 
 * Entity
 * 1.TimeTravel
 * 
 * Jobs to be Done
 * 1.Get the Date from the which the previous date from the User and store in timeNow.
 * 2.Find the previous Friday from the date given
 * 3.Store the date in timeFound.
 * 4.Print the timeFound.
 * 
 * 
 * Pseudo Code
 * 
 * class TimeTravel {
 * 
 * 		public static void main(String[] args ){
 * 			Scanner scanner = new Scanner(System.in);	
 * 			System.out.println("Enter the Date");
 * 			int year = scanner.nextInt();
 * 			int month = scanner.nextInt();
 * 			int date = scanner.nextInt();
 * 			LocalDate dateNow = LocalDate.of(year,month,date);
 * 			LocalDate dateFound = timeNow.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY));
 * 			System.out.println("The Previous Friday from the date provided is :" + dateFound);
 * 		}
 * }
 * @author UKSD
 *
 */

public class TimeTravel {
	
	public static void main(String[] args ) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter the Date");
		
		int date = scanner.nextInt();
		int month = scanner.nextInt();
		int year = scanner.nextInt();
		
		LocalDate dateNow = LocalDate.of(year, month, date);
		
		LocalDate dateFound = dateNow.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY));
		
		System.out.println("The Previous Friday from the date provided is :" + dateFound);
	}

}
