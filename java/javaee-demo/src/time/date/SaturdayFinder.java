package time.date;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

/**
 * Problem Statement
 * 1. Write an example that, for a given month of the current year, lists all of the Saturdays in that month.
 * 
 * Requirement
 * 1. Write an example that, for a given month of the current year, lists all of the Saturdays in that month.
 * 
 * Method Signature
 * 
 * Entity
 * 1.SaturdayFinder
 * 
 * Jobs to be Done
 * 1.Get the Month from the User and store it in monthGot
 * 2.Get the Saturday of each month on the current year and store in presentYear.
 * 3.From the presentYear , get the each month and store in presentMonth.
 * 4.Change the monthGot to Uppercase and store it in MonthGotUpper
 * 5.While the user entered month and the presentMonth is same  
 * 6.Get the consecutive Saturday from the presenYear 
 * 7.Store the got month from the presentYear to presentMonth 
 * 8.Print thePresentYear
 * 
 * Pseudo Code
 * class SaturdayFinder {
 * 	
 * 		public static void main(String[] args) {
 * 
 * 			Scanner scanner = new Scanner(System.in);
 * 			System.out.println("Enter the Month");
 * 			String monthGot = scanner.nextLine();
 * 
 * 			LocalDate presentYear = Year.now()
 * 										.atMonth(monthGot)
 * 										.atDay(1)
 * 										.with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
 * 			Month presentMonth = presentYear.getMonth();
 * 			while(presentMonth == monthGot) {
 * 				presentYear = presentYear.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
 * 				prsenetMonth = presentYear.getMonth();
 * 				System.out.println(presentYear);
 * 			}
 * 		}
 * 
 * }
 * 
 * @author UKSD
 *
 */

public class SaturdayFinder {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Month");
		String monthGot = scanner.nextLine();
		Month monthGotUpper = Month.valueOf(monthGot.toUpperCase());
		  
		LocalDate presentYear = Year.now()
									.atMonth(monthGot)
									.atDay(1)
									.with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
		Month presentMonth = presentYear.getMonth();
		while(presentMonth == monthGotUpper) {
			presentYear = presentYear.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
			presentMonth = presentYear.getMonth();
			System.out.println(presentYear);
		}
		
	}

}
