package time.date;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQuery;
import java.util.Scanner;

/**
 * Problem Statement
 * 1.Write an example that tests whether a given date occurs on Tuesday the 11th
 * 
 * Requirement
 * 1.Write an example that tests whether a given date occurs on Tuesday the 11th
 * 
 * Entity
 * 1.TuesdayFinder
 * 2.isTuesday
 * 
 * Method Signature
 * 1.public boolean queryFrom(TemporalAccessor date);
 * 
 * Jobs to be Done
 * 1.Create a isTuesday class with qeryFrom method.
 * 2.return true if both the date is the equal to 11th and equal to second of the week.
 * 3.return false if both the date is the equal to 11th and equal to second of the week.
 * 4.Get the input from the User
 * 5.parse the date with and print true if the user given date is tuesday and 11th of the month
 * 6.print false if the user given date is not an tuesday and 11th of the month
 * 
 * Pseudo Code
 * class isTuesday implements TemporalQuery<Boolean> {
 *	public Boolean queryFrom(TemporalAccessor date) {
 *
 *		return ((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2));
 *	}
 *}
 *
 *public class TuesdayFinder {
 *	
 *	public static void main(String[] args ) {
 *		Scanner scanner = new Scanner(System.in);
 *		String date = scanner.next();
 *		LocalDate todayDate = LocalDate.parse(date);
 *		System.out.println(todayDate.query(new isTuesday()));
 *	}
 *
 *}
 * @author UKSD
 *
 */

class isTuesday implements TemporalQuery<Boolean> {
	public Boolean queryFrom(TemporalAccessor date) {

		return ((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2));
	}
}

public class TuesdayFinder {
	
	public static void main(String[] args ) {
		Scanner scanner = new Scanner(System.in);
		String date = scanner.next();
		LocalDate todayDate = LocalDate.parse(date);
		System.out.println(todayDate.query(new isTuesday()));
	}

}
