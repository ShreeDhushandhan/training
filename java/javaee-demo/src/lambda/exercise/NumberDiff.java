package lambda.exercise;

/**
 * Problem Statement
 * 1.write a program to print difference of two numbers using lambda expression and the 
 * single method interface.
 * 
 * Requirement
 * 1.write a program to print difference of two numbers using lambda expression and the 
 * single method interface.
 * 
 * Entity
 * 1.CustomNumbers Interface
 * 2.NumberDiff Class
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a new Interface called CustomNumbers and created a new abstract method with two parameters
 *   val1 and val2.
 * 2.In Main Function, create a new lambda expression for the CustomNumbers interface called as custom 
 *   and passing the val1 and val2 as the parameters to the expression and making the expression to 
 *   print the Difference of the 2 Numbers to the Console.
 *   
 * Pseudo Code
 * 	interface CustomNumbers {
 *		void printDiff(int val1, int val2);
 *	}
 *
 * 	class NumberDiff {
 *	
 *		public static void main(String[] args) {
 *			CustomNumbers custom = (val1,val2) -> {
 *				System.out.println(val1 - val2);
 *			};
 *		 
 *			custom.printDiff(99,45);
 *		} 
 *
 *	}
 * @author UKSD
 *
 */

interface CustomNumbers {
	
	void printDiff(int val1, int val2);
}

public class NumberDiff {
	
	public static void main(String[] args) {
		CustomNumbers custom = (val1,val2) -> {
			System.out.println(val1 - val2);
		};
		
		custom.printDiff(99,45);
	}

}
