package lambda.exercise;

/**
 * Problem Statement
 * 1.Code a functional program that can return the sum of elements of varArgs, passed into the 
 *     method of functional interface
 *     
 * Requirement
 * * 1.Code a functional program that can return the sum of elements of varArgs, passed into the 
 *     method of functional interface
 *     
 * Entity
 * 1.FunctionalVarargs
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.Get the Input of any type.
 * 2.Add the values and Print the Sum of the Values. 
 * 
 * Pseudo Code
 * interface Functional<S,D> {
 *	void adder(S s, D d);
 * }
 *
 * class FunctionalVarargs {
 *	
 *	public static void main(String[] args) {
 *		Functional<Integer,Integer> valueSet = (Integer a, Integer b) -> {
 *			System.out.println(a+b);
 *		};
 *		
 *	 	valueSet.adder(124, 8658);
 *	} 
 *
 * } 
 *
 * 
 * @author UKSD
 *
 */

interface Functional<S,D> {
	void adder(S s, D d);
}

public class FunctionalVarargs {
	
	public static void main(String[] args) {
		Functional<Integer,Integer> valueSet = (Integer a, Integer b) -> {
			System.out.println(a+b);
		};
		
		valueSet.adder(124, 8658);
	}

}
