package lambda.exercise;

/**
 * Problem Statement
 * 1.What's wrong with the following program? And fix it using TypeInference
 * 
 * Requirement
 * 1.What's wrong with the following program? And fix it using TypeInference
 * 
 * Entity
 * 1.BiFunction 
 * 2.TypeInferenceExercise
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Get any type of data from the user to print.
 * 2.Check whether the data is not null and 0.
 * 3.If it satisfies the rules then print the Sum of the Values.
 * 
 * Answer
 * 1.Here the Functional Interface must be declared in the separate File else it must be abstract.
 * 2.While calling the interface methods there is no need to specify the type of the value we pass
 * 3.Since while declaring the lambda function of functional interface we provide the datatype.
 * 
 * Pseudo Code
 * interface BiFunction<S,D> {
 *	  int print(S number1, D number2);
 * }
 *
 * public class TypeInferenceExercise {
 *	
 *	public static void main(String[] args) {
 *		
 *		BiFunction<Integer,Integer> function = (Integer number1,Integer number2) -> {
 *			int res = (number1+number2);
 *			return res;
 *		};
 *		
 *		System.out.println(function.print(800, 135246));
 *		
 *	}
 * } 
 * 
 * Code Given
 * public interface BiFunction{
 *    int print(int number1, int number2);
 * } 
 *
 * public class TypeInferenceExercise {
 *	
 *	public static void main(String[] args) {
 *
 *       BiFunction function = (int number1, int number2) ->  { 
 *       return number1 + number2;
 *       };
 *       
 *       int print = function.print(int 23,int 32);
 *       
 *       System.out.println(print);
 *   }
 *
 * }
 *
 * @author UKSD
 *
 */



interface BiFunction<S,D> {
	int print(S number1, D number2);
}

public class TypeInferenceExercise {
	
	public static void main(String[] args) {
		
		BiFunction<Integer,Integer> function = (Integer number1,Integer number2) -> {
			int res = (number1+number2);
			return res;
		};
		
		System.out.println(function.print(800, 135246));
		
	}
}
