package lambda.exercise;

import java.util.Scanner;

/**
 * Problem Statement
 * 1.What is Method Reference and its types.Write a program for each types 
 *		with suitable comments
 *
 * Requirement
 * 1.What is Method Reference and its types.Write a program for each types 
 *		with suitable comments
 *
 * Entity
 * MethodReference
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Get the Age from the User
 * 		1.1)Check the User Age is greater than 18
 * 			1.1.1)If Age greater than 18 then print Eligible
 * 			1.1.2)If not greater than 19 then print Not Eligible.
 * 2.Get the Name from the User
 * 		2.1)If the User name starts with S then
 * 			2.1.1)If yes print Very Good Boy
 * 			2.1.2)If not print Very Bad Boy 
 * @author UKSD
 *
 */

class Finder {
	static void finder(String name, int age) {
		if((age > 18) && (name.startsWith("s"))) {
			System.out.println("Eligible");
			System.out.println("Very Good Boy");
		} else {
			System.out.println("Not Eligible");
			System.out.println("Very Bad Boy");
		}
	}
}

public class MethodReference extends Finder{
	
	public static void main(String[] args) {
		
		MethodReference method = new MethodReference();
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter your Name");
		System.out.println("Enter your Age");
		String name = scanner.nextLine();
		int age = scanner.nextInt();
	
	}

}
