package lambda.exercise;


/**
 * Problem Statement
 * 1.Write a Lambda expression program with a single method interface to concatenate two strings.
 * 
 * Requirement
 * 1.Write a Lambda expression program with a single method interface to concatenate two strings.
 * 
 * Method Signature
 * 
 * Entity
 * 1.Functions Interface
 * 2.StringConcat
 * 
 * Jobs to be Done
 * 1.Create a new interface called Functions and has a abstract method called addString takes two 
 *   parameters called name1 and name2
 * 2.Create a new StringConcat Class.
 * 3.Create a Functional Expression calling function of Lambda Expression with two parameter as an
 *   implementation and adding the two Strings and printing it to the Console.
 *   
 * Pseudo Code
 * 
 * 	interface Functions {
 *    	void addString(String name1, String name2);
 *	}
 *
 *	class StringConcat{
 *	
 *		public static void main(String[] args) {
 *			Functions function = (String name1, String name2) -> {
 *				System.out.println(name1+" "+name2);
 *			};
 *			function.addString("Shree", "Dhushandhan");
 *		}
 *	}
 *
 * @author UKSD
 *
 */
interface Functions {
	
    void addString(String name1, String name2);
}

public class StringConcat{
	
	public static void main(String[] args) {
		Functions function = (String name1, String name2) -> {
			System.out.println(name1+" "+name2);
		};
		
		function.addString("Shree", "Dhushandhan");
	}

}
