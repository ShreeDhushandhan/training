package lambda.exercise;

/**
 * Problem Statement
 * 1.Write a program to print the volume of  a Rectangle using lambda expression
 * 
 * Requirement
 * 1.Write a program to print the volume of  a Rectangle using lambda expression
 * 
 * Method Signature
 * 
 * Entity
 * 1.Rectangle Interface
 * 2.VolumeFinder Class
 * 
 * Jobs to be Done
 * 1.Create a new Interface called Rectangle which has abstract method called volume which takes
 *   three parameters as length,width,height
 * 2.In main method create a new Lambda Expression called rect for the Rectangle Interface and passing
 *   the three parameters and making it print the product of the three values.
 * 3.Call the Lambda Expression with the object created and passing the values.
 * 
 * Pseudo Code
 * 
 * 	interface Rectangle {
 *		void volume(int length,int width, int height);
 *	} 
 *
 *	class VolumeFinder {
 *	
 *		public static void main(String[] args) {
 *			Rectangle rect = (length,width, height) -> {
 *			System.out.println("Volume"+" "+length*width*height);
 *			}; 
 *		
 *			rect.volume(40, 16, 17);
 *		}
 *	}
 * @author UKSD
 *
 */


interface Rectangle {
	void volume(int length,int width, int height);
}

public class VolumeFinder {
	
	public static void main(String[] args) {
		Rectangle rect = (length,width, height) -> {
			System.out.println("Volume"+" "+length*width*height);
		}; 
		
		rect.volume(40, 16, 17);
	}

}
