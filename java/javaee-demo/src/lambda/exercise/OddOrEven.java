package lambda.exercise;

/**
 *Given Program//
 *
 * interface CheckNumber {
 *   public boolean isEven(int value);
 * }
 *
 * public class EvenOrOdd {
 *   public static void main(String[] args) {
 *       CheckNumber number = new CheckNumber() {
 *           public boolean isEven(int value) {
 *               if (value % 2 == 0) {
 *                   return true;
 *               } else return false;
 *           }
 *       };
 *       System.out.println(number.isEven(11));
 *   }
 * }
 * 
 * Problem Statement
 *	1.Convert the following anonymous class into lambda expression.
 *
 * Requirement
 *	1.Convert the following anonymous class into lambda expression.
 *
 * Method Signature
 *
 * Entity
 * 1.CheckNumber Interface
 * 2.OddOrEven Class
 *
 * Jobs to be Done
 * 1.Converted the Class to a lambda Expression by changing creating number the CheckNumber functional
 *   interface.
 * 2.And passing the value as the parameter to the lambda Expression and floor dividing the value by 2
 *   if it returns 0 then the condition returns true else false.  
 *   
 * Pseudo Code
 * 
 * 	interface CheckNumber {
 *		boolean isEven(int value);
 *	}
 *	class OddOrEven {
 *	
 *		public static void main(String[] args) {
 *			CheckNumber number = (value) -> {
 *				if(value%2==0) {
 *					return true;
 *				} else return false;
 *			};
 *			boolean flag = number.isEven(10);
 *			
 *			System.out.println(flag);
 *		}
 *	}
 * 
 * 
 * @author UKSD
 *
 */
interface CheckNumber {
	boolean isEven(int value);
}
public class OddOrEven {
	
	public static void main(String[] args) {
		CheckNumber number = (value) -> {
			if(value%2==0) {
				return true;
			} else return false;
		};
		
		boolean flag = number.isEven(10);
		
		System.out.println(flag);
	}

}
