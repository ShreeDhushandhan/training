package generics.exercise;

/**
 * Entity
 * 1.Employee
 * 
 * Method Signature
 * 1.public String getFirstName();
 * 2.public String getLastName() ;
 * 3.public int getAge() ;
 * 4.public void setFirstName(String firstName) ;
 * 5.public void setLastName(String lastName) ;
 * 6.public void setAge(int age) ;
 *
 * Jobs to be Done
 * 1.Create a new Class called Employee which has the Custom Data Structure of the Employee
 * 2.Create a new String called FName and LName and new int called age.
 * 3.Create a Empty Constructor and parameterized constructor.
 * 4.Create a new getter method called getFirstName which return FName of the Employee
 * 5.Create a new getter method called getLastName which return LName of the Employee
 * 6.Create a new getter method called getAge which return age of the Employee
 * 7.Create a new setter method called setFirstName which sets the FName of the Employee
 * 8.Create a new setter method called getLastName which sets the  LName of the Employee
 * 9.Create a new setter method called setAge which sets the  Age of the Employee
 * @author UKSD
 *
 */


public class Employee {
	private String FName, LName;
	private int age;

	Employee() {
		
	}
	
	Employee(String FName, String LName, int age) {
		this.FName = FName;
		this.LName = LName;
		this.age = age;
	}
	
	public String getFirstName() {
		return FName;
	}
	
	public String getLastName() {
		return LName;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setFirstName(String firstName) {
		this.FName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.LName = lastName;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
}
