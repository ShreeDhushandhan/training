package generics.exercise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Problem Statement
 * 1.Write a program to demonstrate Generic - for loop, for list, set and map.
 * 
 * Entity
 * 1.GenericList Class
 * 2.GenericSet Class
 * 3.GenericMap Class
 * 4.GenericTypes Class
 * 
 * Work Done
 * 1.Created Generic class called GenericList.
 * 2.Created a new List of Generic type called list.
 * 3.Created a new method of void type called addEle that takes a value to add the value to list.
 * 4.Created a new method of void type called remEle that takes a value to be removed from the list
 * 
 * 5.Created another Class for Set called Generic Set.
 * 6.Created a new Generic Set called newSet.
 * 7.Created a method that takes a value and returns void, this method add the value to the Set.
 * 8.Another method is created that takes to remove from the Set and returns void.
 * 
 * 9.Created another Class for Map and called as Generic Map
 * 10.Created a new Generic Map which takes two parameters as key and value.
 * 11.Created a void method that takes two Generic type as parameter and adds to the Map with the Key
 *    and Value.
 * 12.Created another void method that takes two Genetic type as parameter and removes the specific 
 *    value form the Map with the value and key provided.
 *    
 * 13.In the Main Function created a new Object of Generic List of Integer type and 
 * 	  added values to the List by the addEle method of the GenericList.
 * 14.Printing the Values from the List with the For each Loop and printing it to the Console.
 * 15.In the Main Function created a new Object of Generic Set of String type and 
 * 	  added values to the List by the addSetEle method of the GenericSet.
 * 16.Printing the Values from the Set with the For each Loop and printing it to the Console.
 * 17.In the Main Function created a new Object of Generic Map of Integer and String type and 
 * 	  added values to the Map by the addSetEle method of the GenericMap.
 * 18.Printing the Values from the Map with the Iterator Class Object and checking the Map for the 
 * Next Value if the values is exist then value is printed using the entryKey and entryValue 
 * method to the Console.
 * @author UKSD
 *
 * @param <S>
 */

class GenericList<S> {
	
	public GenericList () {
		
	}
	public List<S> list = new ArrayList<S>();
	
	public void addEle(S val) {
		list.add(val);
	}
	
	public void remEle(S val) {
		list.remove(val);
	}
	
}

class GenericSet<S> {
	
	public Set<S> newSet = new HashSet<S>();
	
	public GenericSet () {
		
	}
	
	public void addSetEle(S val) {
		newSet.add(val);
	}
	
	public void remSetEle(S val) {
		newSet.remove(val);
	}
	
}

class GenericMap<S,K> {
	
	public Map<S,K> newMap = new HashMap<S,K>();
	
	public GenericMap() {
		
	}
	
	public void addMapEle(S pos, K val) {
		newMap.put(pos,val);
	}
	
	public void remMapEle(S pos, K val) {
		newMap.remove(pos, val);
	}
}

public class GenericTypes {
	
	public static void main(String[] args) {
		
		//Generic List
		GenericList<Integer> geneType = new GenericList<Integer>();

		geneType.addEle(10);
		geneType.addEle(20);
		geneType.addEle(30);
		geneType.addEle(40);
		//Printing the Elements
		System.out.println("Printing Elements using For Loop");
		for(Integer s : geneType.list) {
			System.out.println(s);
		}
		
		//Generic Set
		GenericSet<String> geneSet = new GenericSet<String>();
		
		geneSet.addSetEle("Shree");
		geneSet.addSetEle("Sanjay");
		geneSet.addSetEle("Shree Dhushandhan");
		geneSet.addSetEle("Shree Sanjay");
		
		System.out.println("Printing the Set Elements using for each Loop");
		
		for(String e : geneSet.newSet) {
			System.out.println(e);
		}
		
		
		//Generic Map
		GenericMap<Integer,String> geneMap = new GenericMap<Integer,String>();
		
		geneMap.addMapEle(1,"Shree");
		geneMap.addMapEle(2,"Sanjay");
		geneMap.addMapEle(3,"Shree Dhushandhan");
		geneMap.addMapEle(4,"Shree Sanjay");
		
		Iterator<Map.Entry<Integer,String>> iterator = geneMap.newMap.entrySet().iterator();
		
		while(iterator.hasNext()) {
			Map.Entry<Integer, String> entry = iterator.next();
			System.out.println(entry.getKey() +" "+entry.getValue());
		}
		
		
	}

}
