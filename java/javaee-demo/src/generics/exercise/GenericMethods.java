package generics.exercise;

import java.util.ArrayList;
import java.util.List;

/**
 * Problem Statement
 * 1.Write a generic method to count the number of elements in a collection that have a specific
 *  property (for example, odd integers, prime numbers, Palindrome).
 *  
 * Requirements
 * 1.Write a generic method to count the number of elements in a collection that have a specific
 *  property (for example, odd integers, prime numbers, Palindrome).
 *  
 * Entity
 * 1.GenericMethods
 * 
 * Method Signature
 * 1.public static <S> genericMethod(List<S> list);
 * 
 * Jobs to be Done.
 * 1.Create a Generic Method which takes Generic List as Parameter.
 * 	  1.1)By using stream iterate the List and print the values.
 * 2.Create a Generic List and add values.
 *    2.1)Pass the List to the Generic method created. 
 *    
 * Pseudo Code
 * class GenericMethods {
 * 
 * 		<S> void genericMethod(List<S> list) {
 * 			list.stream().forEach(x -> System.out.println(x));
 * 		}
 * 
 * 		public static void main(String[] args) {
 * 			List<Integer> newList = new ArrayList<Integer>();
 * 			
 * 			newList.add(10);
 * 			newList.add(20);
 *			newList.add(30);
 *			newList.add(40);
 *
 *			GenericMethods generic = new GenericMethods();
 *			generic.genericMethod(newList);
 * 		}
 * }
 * @author UKSD
 *
 */

public class GenericMethods {
	
	<S> void genericMethod(List<S> list) {
		list.stream().forEach(x -> System.out.println(x));
	}
	
	public GenericMethods() {
		
	}
	
	public static void main(String[] args) {
		List<Integer> newList = new ArrayList<Integer>();
		
		newList.add(10);
		newList.add(20);
		newList.add(30);
		newList.add(40);
		
		GenericMethods generic = new GenericMethods();
		
		generic.genericMethod(newList);
		
	}

}
