package generics.exercise;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Problem Statement
 * 1.Write a generic method to exchange the positions of two different elements in an array.
 * 
 * Requirement
 * 1.Write a generic method to exchange the positions of two different elements in an array.
 * 
 * Entity
 * 1.Swapping
 * 
 * Method Signature
 * 1.public <S> S void swap(List<S> list, S value1, S value2);
 * 
 * Jobs to be Done
 * 1.Create a Generic method which takes a List and 2 values as parameter
 * 		1.1)Swap the two values of the List.
 * 2.Create a Generic List.
 *   	2.1)Add Values to the List.
 * 3.Pass the List and two index of the value to swap method.
 * 4.Print the List.
 * 
 * Pseudo Code
 * class Swapping() {
 * 		
 * 		public <S> List swapList(List<S> list, int value1, int value2) {
 * 			Collections.swap(list,value1,value2);
 * 			return list;
 * 		}
 * 
 * 		public static void main(String[] args) {
 * 			List<Integer> newList = new ArrayList<Integer>();
 * 
 *			newList.add(10);
 *			newList.add(20);
 *			newList.add(30);
 *			newList.add(40);
 *			newList.add(50);
 *			newList.add(60);
 *			newList.add(70);
 *			newList.add(80);
 *
 *			Swapping swapping = new Swapping();
 *			System.out.println(swapping.swapped(newList, 1, 4));
 *
 * 		}
 * }
 * @author UKSD
 *
 */

public class Swapping {
	
	public <S> List swapped(List<S> list, int value1, int value2) {
		Collections.swap(list, value1, value2);;
		return list;
    }
	
	public static void main(String[] args ){
		List<Integer> newList = new ArrayList<Integer>();
		
		newList.add(10);
		newList.add(20);
		newList.add(30);
		newList.add(40);
		newList.add(50);
		newList.add(60);
		newList.add(70);
		newList.add(80);
		
		Swapping swapping = new Swapping();
		System.out.println(swapping.swapped(newList, 1, 4));
	}

}
