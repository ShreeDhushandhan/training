package generics.exercise;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Problem Statement
 * 1.Write a program to print employees name list by implementing iterable interface.
 * 2.Why except iterator() method in Iterable interface, are not necessary to define in the implemented class.
 * 
 * Requirement
 * 1.Write a program to print employees name list by implementing iterable interface.
 * 2.Why except iterator() method in Iterable interface, are not necessary to define in the implemented class.
 * 
 * Entity
 * 1.GenericExcercise
 * 
 * Method Signature
 * 1.public void addEmployee(Employee emp);
 * 2.public void removeEmployee(Employee emp);
 * 
 * Jobs to Done
 * 1.Create a global declared ArrayList named employeeList of Employee type.
 * 2.Create a empty constructor.
 * 3.Create a addEmployee method which takes Employee instance as the parameter and adds the instance to the List Created.
 * 4.Create another method called removeEmployee to remove the Employee instance in the list created.
 * 5.Here overridden the Iterator method of the Iterable Interface to make the custom dataStructure Iterable so that the values in 
 *   the Custom Class can be iterated and printed out.
 * 6.In Main Function created a new Object of the GenericExercise Class.
 * 7.Created a 3 new Instance of the Employee type as employee1, employee2, employee3 and passed values
 * 8.Now Adding 3 objects of  the Employee instance to the List Created by calling the addEmployee method.
 * 9.And Printed the Employee List values by Iterator method overridden, by calling the for each method the values of the List 
 *   are called and printed to the console.
 *   
 * Answer:
 * 2.The Iterator method is necessary to be overridden from the Iterable interface to the implementing Class because the iterator 
 *   method makes the Class or the custom data type iterable but the rest of the methods such as spliterator does not makes the 
 *   class iterable..
 *   
 * Pseudo Code
 * public class GenericExcercise implements Iterable<Employee>{
 * 		
 * 		public static void main(String[] args) {
 * 		
 *		//Class Object
 *		GenericExcercise gene = new GenericExcercise();
 *		
 *		//Creating Employee Instances
 *		Employee employee1 = new Employee("Shree", "Dhushandhan", 19);
 *		Employee employee2 = new Employee("Shree", "Sanjay", 16);
 *		Employee employee3 = new Employee("Peter", "Parker", 26);
 *		
 *		//Adding Employees to the List
 *		gene.addEmployee(employee1);
 *		gene.addEmployee(employee2);
 *		gene.addEmployee(employee3);
 *		
 *		//Before Removing Employee
 *		System.out.println("Bofore Removing Employee");
 *		for(Employee emp1 : gene) {
 *			System.out.println(emp1.getFirstName() +" "+ emp1.getLastName() +" "+ emp1.getAge());
 *		}
 *		
 *		gene.removeEmployee(employee3);
 *		//After Removing Employee		
 *		System.out.println("After Removing Employee");
 *		for(Employee emp1 : gene) {
 *			System.out.println(emp1.getFirstName() +" "+ emp1.getLastName() +" "+ emp1.getAge());
 *		}
 *				
 *	} 
 *
 * 
 * 
 * }
 * 
 * @author UKSD
 *
 */
public class GenericExcercise implements Iterable<Employee>{
	//ArrayList of Person Data type
	public List<Employee> employeeList = new ArrayList<Employee>();
	
	//Constructor
	public void GenericsExcercise() {
		
	}
	
	//Add Employee 
	public void addEmployee(Employee emp) {
		employeeList.add(emp);
	}
	
	//RemoveEmployee
	public void removeEmployee(Employee emp) {
		employeeList.remove(emp);
	}
	
	//Iterator
	@Override
	public Iterator<Employee> iterator() {
		return employeeList.iterator();
	}
	
	//Main Function
	public static void main(String[] args) {
		
		//Class Object
		GenericExcercise gene = new GenericExcercise();
		
		//Creating Employee Instances
		Employee employee1 = new Employee("Shree", "Dhushandhan", 19);
		Employee employee2 = new Employee("Shree", "Sanjay", 16);
		Employee employee3 = new Employee("Peter", "Parker", 26);
		
		//Adding Employees to the List
		gene.addEmployee(employee1);
		gene.addEmployee(employee2);
		gene.addEmployee(employee3);
		
		//Before Removing Employee
		System.out.println("Bofore Removing Employee");
		for(Employee emp1 : gene) {
			System.out.println(emp1.getFirstName() +" "+ emp1.getLastName() +" "+ emp1.getAge());
		}
		
		gene.removeEmployee(employee3);
		//After Removing Employee		
		System.out.println("After Removing Employee");
		for(Employee emp1 : gene) {
			System.out.println(emp1.getFirstName() +" "+ emp1.getLastName() +" "+ emp1.getAge());
		}
				
	}

	

}
