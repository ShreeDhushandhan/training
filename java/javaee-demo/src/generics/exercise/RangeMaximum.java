package generics.exercise;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * Problem Statement
 * 1.Write a generic method to find the maximal element in the range [begin, end) of a list.
 * 
 * Requirement
 * 1.Write a generic method to find the maximal element in the range [begin, end) of a list.
 * 
 * Method Signature
 * 1.public <S> int maximumReturner(List<S> list, int start, int end);
 * 
 * Entity
 * 1.RangeMaximum
 * 
 * Jobs to be Done
 * 1.Create a List and add elements.
 * 2.Get the range from the user.
 * 3.Pass the List and two values to the maximumReturner method created. 
 * 
 * Pseudo Code
 * 
 * 		
 *		
 *		Set<S> newSet = new HashSet<S>(secList);
 *		
 *		for(S i: newSet) {
 *			count = Collections.frequency(list, i);
 *		}
 *		
 * 		return count;
 *	}
 *	
 *	public static void main(String[] args) {
 *		Scanner scanner = new Scanner(System.in);
 *		
 *		RangeMaximum range = new RangeMaximum();
 *		List<Integer> newList = new ArrayList<Integer>();
 *		
 *		newList.add(3);
 *		newList.add(2);
 *		newList.add(1);
 *		newList.add(3);
 *		newList.add(3);
 *		newList.add(3);
 *		newList.add(3);
 *		newList.add(3);
 *		newList.add(1);
 *		newList.add(1);
 *		newList.add(3);
 *		
 *		
 *		System.out.println("Enter the Range");
 *		int startVal = scanner.nextInt();
 *		int endVal = scanner.nextInt();
 *		
 *		System.out.println(range.maximumReturner(newList, startVal, endVal));
 *		
 *	}
 *
 *}
 * 
 * @author UKSD
 *
 */

public class RangeMaximum {
	
	public <S> int maximumReturner(List<S> list, int start, int end) {
		int count = 1;
		
		List<S> secList = new ArrayList<S>();
		
		for(int i=start; i<=end; i++) {
			secList.add(list.get(i));
		}
		
		
		Set<S> newSet = new HashSet<S>(secList);
		
		for(S i: newSet) {
			count = Collections.frequency(list, i);
		}
		
		return count;
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		RangeMaximum range = new RangeMaximum();
		List<Integer> newList = new ArrayList<Integer>();
		
		newList.add(3);
		newList.add(2);
		newList.add(1);
		newList.add(3);
		newList.add(3);
		newList.add(3);
		newList.add(3);
		newList.add(3);
		newList.add(1);
		newList.add(1);
		newList.add(3);
		
		
		System.out.println("Enter the Range");
		int startVal = scanner.nextInt();
		int endVal = scanner.nextInt();
		
		System.out.println(range.maximumReturner(newList, startVal, endVal));
		
	}

}
