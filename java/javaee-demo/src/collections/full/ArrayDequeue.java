package collections.full;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

/**
 * Problem Statement
 * 1.Use addFirst(),
 *       addLast(),
 *       removeFirst(),
 *       removeLast(),
 *       peekFirst(),
 *       peekLast(),
 *       pollFirst(),
 *       poolLast() 
 * methods to store and retrieve elements in ArrayDequeue.
 * 
 * Requirement
 *  1.Use addFirst(),
 *       addLast(),
 *       removeFirst(),
 *       removeLast(),
 *       peekFirst(),
 *       peekLast(),
 *       pollFirst(),
 *       poolLast() 
 * methods to store and retrieve elements in ArrayDequeue.
 *
 * Method Signature
 * 1.public <S> void adderAtFirst(Deque<S> queue,S val);
 * 2.public <S> void adderAtLast(Deque<S> queue,S val);
 * 3.public <S> void removeAtFirst(Deque<S> queue);
 * 4.public <S> void removeAtLast(Deque<S> queue);
 * 5.public <S> void peekAtFirst(Deque<S> queue);
 * 6.public <S> void peekAtLast(Deque<S> queue);
 * 7.public <S> void pollAtFirst(Deque<S> queue);
 * 8.public <S> void pollAtLast(Deque<S> queue);
 * 
 * Jobs to be Done
 * 1.Create a ArrayDeque
 * 2.Add Element to ArayDequeue
 * 3.Add Value at First
 * 4.Add Value at last
 * 5.Remove value at First
 * 6.Remove value at last
 * 7.print the value at First
 * 8.print the value at last
 * 9.Remove the value at First
 * 10.Remove the value at last
 * 
 * Pseudo Code
 * 
 *  class ArrayDequeue {
 *	
 *	public <S> void adderAtFirst(Deque<S> queue,S val) {
 *		queue.addFirst(val);
 *	}
 *	
 *	public <S> void adderAtLast(Deque<S> queue,S val) {
 *		queue.addLast(val);
 *	}
 *	
 *	public <S> void removeAtFirst(Deque<S> queue) {
 *		System.out.println("Removed at First : "+queue.removeFirst());
 *	}
 *	
 *	public <S> void removeAtLast(Deque<S> queue) {
 *		System.out.println("Removed at Last : "+queue.removeLast());
 *	}
 *	
 *	public <S> void peekAtFirst(Deque<S> queue) {
 *		System.out.println("Top Element : "+queue.peekFirst());
 *		
 *	}
 *	
 *	public <S> void peekAtLast(Deque<S> queue) {
 *		System.out.println("Last Element : "+queue.peekLast());
 *	}
 *	
 *	public <S> void pollAtFirst(Deque<S> queue) {
 *		System.out.println("Top Removed Element : "+queue.pollFirst());
 *	}
 *	
 *	public <S> void pollAtLast(Deque<S> queue) {
 *		System.out.println("Last Removed Element : "+queue.pollLast());
 *	}
 *	
 *	public <S> void printer(Deque<S> queue) {
 *		Iterator it = queue.iterator();
 *		while(it.hasNext()) {
 * 			System.out.println(it.next());
 *		}
 *	}
 *	
 *	public static void main(String[] args) {
 *		
 *	 	ArrayDequeue arrqueue = new ArrayDequeue();
 *		
 *		Deque<String> queue = new ArrayDeque<String>();
 *		//AddFirst
 *		arrqueue.adderAtFirst(queue, "Shree");
 *		arrqueue.adderAtFirst(queue, "Shree Dhushandhan");
 *		arrqueue.adderAtFirst(queue, "Shree Sanjay");
 *		arrqueue.adderAtFirst(queue, "Shree Shree");
 *		arrqueue.adderAtFirst(queue, "Sanjay");
 *		arrqueue.adderAtFirst(queue, "Sanjay Sanjay");
 *		//AddLast		
 *		arrqueue.adderAtLast(queue, "Apple");
 *		arrqueue.adderAtLast(queue, "Samsung");
 *		
 *		//RemoveFirst
 *		arrqueue.removeAtFirst(queue);
 *		
 *		//removeLast
 *		arrqueue.removeAtLast(queue);
 * 		
 *		//PeekFirst
 *		arrqueue.peekAtFirst(queue);
 *		
 *		//PeekLast
 *		arrqueue.peekAtLast(queue);
 *		
 *		//PollFirst
 *		arrqueue.pollAtFirst(queue);
 *		
 *		//PollLast
 *		arrqueue.pollAtLast(queue);
 *		
 *		arrqueue.printer(queue);
 *		
 *	}
 *
 *}
 * @author UKSD
 *
 */

public class ArrayDequeue {
	
	public <S> void adderAtFirst(Deque<S> queue,S val) {
		queue.addFirst(val);
	}
	
	public <S> void adderAtLast(Deque<S> queue,S val) {
		queue.addLast(val);
	}
	
	public <S> void removeAtFirst(Deque<S> queue) {
		System.out.println("Removed at First : "+queue.removeFirst());
	}
	
	public <S> void removeAtLast(Deque<S> queue) {
		System.out.println("Removed at Last : "+queue.removeLast());
	}
	
	public <S> void peekAtFirst(Deque<S> queue) {
		System.out.println("Top Element : "+queue.peekFirst());
		
	}
	
	public <S> void peekAtLast(Deque<S> queue) {
		System.out.println("Last Element : "+queue.peekLast());
	}
	
	public <S> void pollAtFirst(Deque<S> queue) {
		System.out.println("Top Removed Element : "+queue.pollFirst());
	}
	
	public <S> void pollAtLast(Deque<S> queue) {
		System.out.println("Last Removed Element : "+queue.pollLast());
	}
	
	public <S> void printer(Deque<S> queue) {
		Iterator it = queue.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
	
	public static void main(String[] args) {
		
		ArrayDequeue arrqueue = new ArrayDequeue();
		
		Deque<String> queue = new ArrayDeque<String>();
		//AddFirst
		arrqueue.adderAtFirst(queue, "Shree");
		arrqueue.adderAtFirst(queue, "Shree Dhushandhan");
		arrqueue.adderAtFirst(queue, "Shree Sanjay");
		arrqueue.adderAtFirst(queue, "Shree Shree");
		arrqueue.adderAtFirst(queue, "Sanjay");
		arrqueue.adderAtFirst(queue, "Sanjay Sanjay");
		//AddLast		
		arrqueue.adderAtLast(queue, "Apple");
		arrqueue.adderAtLast(queue, "Samsung");
		
		//RemoveFirst
		arrqueue.removeAtFirst(queue);
		
		//removeLast
		arrqueue.removeAtLast(queue);
		
		//PeekFirst
		arrqueue.peekAtFirst(queue);
		
		//PeekLast
		arrqueue.peekAtLast(queue);
		
		//PollFirst
		arrqueue.pollAtFirst(queue);
		
		//PollLast
		arrqueue.pollAtLast(queue);
		
		arrqueue.printer(queue);
		
	}

}
