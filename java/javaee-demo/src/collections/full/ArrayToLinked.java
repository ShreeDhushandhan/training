package collections.full;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Problem Statement
 * 1.create an array list with 7 elements, 
 *   and create an empty linked list add all elements of the array list to linked list, 
 *   traverse the elements and display the result.
 *   
 * Requirement
 * 1.create an array list with 7 elements, 
 *   and create an empty linked list add all elements of the array list to linked list, 
 *   traverse the elements and display the result.
 *   
 * Method Signature
 * 1.public <S> void adder(List<S> list, LinkedList<S> linked);
 * 2.public <S> void displayer(LinkedList<S> linked);
 * 
 * Entity
 * 1.ArrayToLinked
 * 
 * Jobs to be Done
 * 1.Create a list and add 7 Elements
 * 2.Create a LinkedList
 * 3.Add the list elements to the LinkedList
 * 4.Print the Elements in the linkedList. 
 * 
 * Pseudo Code
 * class ArrayToLinked {
 *	
 *	public static Scanner scanner = new Scanner(System.in);
 *	
 *	public <S> void adder(List<S> list, LinkedList<S> linked) {
 *		for(S i : list) {
 *			linked.add(i);
 *		}
 *	}
 *	
 *	public <S> void displayer(LinkedList<S> linked) {
 *		for(S i : linked) {
 *			System.out.println(i);
 *		}
 *	}
 *	
 *	public static void main(String[] args) {
 *		
 *		ArrayToLinked arrLinked = new ArrayToLinked();
 *		
 *		//ArrayList
 *		List<Integer> newList = new ArrayList<Integer>();
 *		newList.add(1);
 *		newList.add(2);
 *		newList.add(3);
 *		newList.add(4);
 *		newList.add(5);
 *		newList.add(6);
 *		newList.add(7);
 *		
 *		//LinkedList
 *		LinkedList<Integer> newLinkedList = new LinkedList<Integer>();
 *		
 *		
 *		arrLinked.adder(newList,newLinkedList);
 * 		arrLinked.displayer(newLinkedList);
 *	}
 *
 *}
 * @author UKSD
 *
 */

public class ArrayToLinked {
	
	public static Scanner scanner = new Scanner(System.in);
	
	public <S> void adder(List<S> list, LinkedList<S> linked) {
		for(S i : list) {
			linked.add(i);
		}
	}
	
	public <S> void displayer(LinkedList<S> linked) {
		for(S i : linked) {
			System.out.println(i);
		}
	}
	
	public static void main(String[] args) {
		
		ArrayToLinked arrLinked = new ArrayToLinked();
		
		//ArrayList
		List<Integer> newList = new ArrayList<Integer>();
		newList.add(1);
		newList.add(2);
		newList.add(3);
		newList.add(4);
		newList.add(5);
		newList.add(6);
		newList.add(7);
		
		//LinkedList
		LinkedList<Integer> newLinkedList = new LinkedList<Integer>();
		
		
		arrLinked.adder(newList,newLinkedList);
		arrLinked.displayer(newLinkedList);
	}

}
