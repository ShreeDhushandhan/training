package collections.full;

/**
 * Problem Statement
 * 1.Addition,Substraction,Multiplication and Division concepts are achieved using Lambda expression and functional interface
 * 
 * Requirement
 * 1.Addition,Substraction,Multiplication and Division concepts are achieved using Lambda expression and functional interface
 * 
 * Entity
 * 1.LambdaOperations
 * 2.Operations
 * 
 * Method Signature
 * 1.void adder(int a, int b);
 * 2.void sub(int a, int b);
 * 3.void multi(int a, int b);
 * 4.void divi(int a, int b);
 * 
 * Jobs to be Done
 * 1.Get two Values from the User 
 * 2.Find the Sum of the Two Numbers
 * 3.Find the Remaining value of the Two Numbers
 * 4.Find the Product of the Two Numbers
 * 5.Find the Divided value of the Two Numbers
 * 
 * Pseudo Code
 * interface Operators {
 *	void operator(int a, int b);
 * }
 *
 * class LambdaOperations {
 *	
 *	public static void main(String[] args) {
 *		Operators numAdder = (a,b) -> System.out.println(a+b);
 *		Operators numSub = (a,b) -> System.out.println(a-b);
 *		Operators numMulti = (a,b) -> System.out.println(a*b);
 *		Operators numDivi = (a,b) -> {
 *			try {
 *				System.out.println(a/b);
 * 			} catch(ArithmeticException e) {
 *				System.out.println("Enter Non 0 Number");
 *			}
 *		};
 *		
 *		numAdder.operator(5,10);
 *		numSub.operator(25,100);
 *		numMulti.operator(56,101);
 *		numDivi.operator(51,0);
 *	 }
 * }
 * @author UKSD
 *
 */

interface Operators {
	void operator(int a, int b);
}

public class LambdaOperations {
	
	public static void main(String[] args) {
		Operators numAdder = (a,b) -> System.out.println(a+b);
		Operators numSub = (a,b) -> System.out.println(a-b);
		Operators numMulti = (a,b) -> System.out.println(a*b);
		Operators numDivi = (a,b) -> {
			try {
				System.out.println(a/b);
			} catch(ArithmeticException e) {
				System.out.println("Enter Non 0 Number");
			}
		};
		
		numAdder.operator(5,10);
		numSub.operator(25,100);
		numMulti.operator(56,101);
		numDivi.operator(51,0);
	}
}
