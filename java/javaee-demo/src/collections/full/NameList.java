package collections.full;

import java.util.ArrayList;
import java.util.List;

/**
 * Problem Statement
 * 1.Display only names starting with 'A'
 * 
 * Requirement
 * 1.Display only names starting with 'A'
 * 
 * Method Signature
 * 
 * Entity
 * 1.NameList
 * 
 * Jobs to be Done
 * 1.Check the List for name starts with A
 * 		1.1)If Yes Print the Name
 * 		1.2)If Not Print as No Name Exist.
 * 
 * Pseudo Code
 * public class NameList {
 *	
 *	public static void main(String[] args) {
 *		
 *		List<String> nameList = new ArrayList<String>();
 *		
 *		nameList.add("krishnan");
 *		nameList.add("abishek");
 *		nameList.add("arun");
 *		nameList.add("vignesh");
 *		nameList.add("kiruthiga");
 *		nameList.add("murugan");
 *		nameList.add("adhithya");
 *		nameList.add("balaji");
 *		nameList.add("priya");
 *		
 *		for(String i : nameList) {
 *			if(i.startsWith("a")) {
 *				System.out.println(i.toString());
 *			}
 *		}
 *		
 *		
 *	} 
 *
 *}
 * @author UKSD
 *
 */

public class NameList {
	
	public static void main(String[] args) {
		
		List<String> nameList = new ArrayList<String>();
		
		nameList.add("krishnan");
		nameList.add("abishek");
		nameList.add("arun");
		nameList.add("vignesh");
		nameList.add("kiruthiga");
		nameList.add("murugan");
		nameList.add("adhithya");
		nameList.add("balaji");
		nameList.add("priya");
		
		for(String i : nameList) {
			if(i.startsWith("a")) {
				System.out.println(i.toString());
			}
		}
		
		
	}

}
