package collections.full;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

/**
 * problem Statement
 * 1.Code for sorting vetor list in descending order.
 * 
 * Requirement
 * 1.Code for sorting vetor list in descending order.
 * 
 * Method Signature
 * 
 * 
 * Entity
 * 1.VectorList
 * 
 * Jobs to be Done
 * 1.Create a Vector 
 * 2.Add elements to the Vetor
 * 3.Sort the Vector
 * 4.Reverse the Vector
 * 5.Print the Vector. 
 * 
 * Pseudo Code
 * 
 * 
 *  class VectorList {
 *	
 *	public void reverse(Vector vector) {
 *		Comparator comparator = Collections.reverseOrder();
 *		Collections.sort(vector,Collections.reverseOrder());
 *	}
 *	
 *	public static void main(String[] args) {
 *		
 *		VectorList vectorList = new VectorList();
 *		//Vector
 *		Vector vector = new Vector(10);
 *		
 *		vector.add(10);
 *		vector.add(20);
 *		vector.add(30);
 *		vector.add(40);
 *		vector.add(50);
 *		vector.add(60);
 *		
 *		vectorList.reverse(vector);
 *		
 *		System.out.println(vector);
 *		
 *	} 
 *
 *}
 * @author UKSD
 *
 */

public class VectorList {
	
	public void reverse(Vector vector) {
		Collections.sort(vector,Collections.reverseOrder());
	}
	
	public static void main(String[] args) {
		
		VectorList vectorList = new VectorList();
		//Vector
		Vector vector = new Vector(10);
		
		vector.add(10);
		vector.add(20);
		vector.add(30);
		vector.add(40);
		vector.add(50);
		vector.add(60);
		
		vectorList.reverse(vector);
		
		System.out.println(vector);
		
	}

}
