package collections.full;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Problem Statement
 * 1.what is the difference between poll() and remove() method of queue interface?
 * 
 * Requirement
 * 1.what is the difference between poll() and remove() method of queue interface?
 * 
 * Entity
 * QueueFunctions
 * 
 * Method Signature
 * 1.public <S> void removeHead(Queue<S>);
 * 2.public <S> void removeAny(Queue<S>, S val);
 * 
 * Jobs to be Done
 * 1.Create a Queue
 * 2.Add values to the Queue
 * 3.Remove the Top Value of the Queue and print it.
 * 4.Get the Value from the User and delete it from the Queue.
 * 
 * Pseudo Code
 * 
 *   class QueueFunctions {
 *	
 *	public <S> void removeHead(Queue<S> queue) {
 *		System.out.println("Removed : " + queue.poll());
 *		
 *	} 
 *	
 *	public <S> void removeAny(Queue<S> queue, S val) {
 *		if(queue.contains(val)) {
 *			queue.remove(val);
 *			System.out.println("Value Removed");
 *			queue.stream().forEach(x -> System.out.println(x));
 *		} else {
 *			System.out.println("Element not Found");
 *		}
 *		
 *	}
 *	
 *	public static void main(String[] args) {
 *		
 *		QueueFunctions queue = new QueueFunctions();
 *		
 *		//Queue
 *		Queue<String> newQueue = new LinkedList<String>();
 *		
 *		newQueue.add("Shree");
 *		newQueue.add("Shree Dhushandhan");
 *		newQueue.add("Shree Sanjay");
 *		newQueue.add("Shree Shree");
 *		newQueue.add("Sanjay");
 *		newQueue.add("Sanjay Sanjay");
 *		
 *		queue.removeHead(newQueue);
 *		
 *		queue.removeAny(newQueue, "Sanjay");
 *	}
 *
 *}
 *
 * @param args
 */

public class QueueFunctions {
	
	public <S> void removeHead(Queue<S> queue) {
		System.out.println("Removed : " + queue.poll());
		
	} 
	
	public <S> void removeAny(Queue<S> queue, S val) {
		if(queue.contains(val)) {
			queue.remove(val);
			System.out.println("Value Removed");
			queue.stream().forEach(x -> System.out.println(x));
		} else {
			System.out.println("Element not Found");
		}
		
	}
	
	public static void main(String[] args) {
		
		QueueFunctions queue = new QueueFunctions();
		
		//Queue
		Queue<String> newQueue = new LinkedList<String>();
		
		newQueue.add("Shree");
		newQueue.add("Shree Dhushandhan");
		newQueue.add("Shree Sanjay");
		newQueue.add("Shree Shree");
		newQueue.add("Sanjay");
		newQueue.add("Sanjay Sanjay");
		
		queue.removeHead(newQueue);
		
		queue.removeAny(newQueue, "Sanjay");
	}

}
