package collections.streams;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Problem Statement
 * 1.Consider a following code snippet:
 *           List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
 *      - Get the non-duplicate values from the above list using java.util.Stream API
 *      
 * Requirement
 * 1.Consider a following code snippet:
 *           List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
 *      - Get the non-duplicate values from the above list using java.util.Stream API
 *      
 * Entity
 * 1.NonRepeatingNumbers
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.For each value in list check
 * 		1.1)if the frequency of the number is occurred more than 1 time or equal to 1 time
 * 		1.2)If occurred remove the value from the list
 * 		1.3)If not print the value.
 * 
 * Pseudo Code
 * class NonRepeatingNumbers {
 *	
 *	public static void main(String[] args) {
 *		List<Integer> randomNumbers = new ArrayList<>(Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25));
 *		
 *		randomNumbers.stream().forEach(value -> {
 *			if(Collections.frequency(randomNumbers, value) >=1) {
 *				randomNumbers.remove(value);
 *			} else {
 *				System.out.println(value);
 *			}
 *		});
 *		
 *		
 *		System.out.println(randomNumbers);
 *		
 *		}
 *
 *	}
 * @author UKSD
 *
 */

public class NonRepeatingNumbers {
	
	public static void main(String[] args) {
		
		List<Integer> randomNumbers = new ArrayList<>(Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25));
		
		randomNumbers.stream().forEach(value -> {
			if(Collections.frequency(randomNumbers, value) >=1) {
				randomNumbers.remove(value);
			} else {
				System.out.println(value);
			}
		});
		System.out.println(randomNumbers);
		
	}

}