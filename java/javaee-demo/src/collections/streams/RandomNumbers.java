package collections.streams;

/**
 * Problem Statement
 * 1.Consider a following code snippet:
 *         List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
 *       - Find the sum of all the numbers in the list using java.util.Stream API
 *       - Find the maximum of all the numbers in the list using java.util.Stream API
 *       - Find the minimum of all the numbers in the list using java.util.Stream API
 *       
 * Requirement
 *  1.Consider a following code snippet:
 *         List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
 *       - Find the sum of all the numbers in the list using java.util.Stream API
 *       - Find the maximum of all the numbers in the list using java.util.Stream API
 *       - Find the minimum of all the numbers in the list using java.util.Stream API
 *
 * Method Signature
 * 
 * Entity
 * 1.RandomNumbers
 * 
 * Jobs to be Done
 * 1.Sum up all the items in the List
 * 2.Print the Total Value of the List
 * 3.Find the Maximum value in the list
 * 4.Print the Maximum value.
 * 5.Find the Minimum value in the List
 * 6.Print the Minimum value.
 * 
 * Pseudo Code
 * public class RandomNumbers {
 *	
 *	public static void main(String[] args) {
 *		
 *		Integer[] arr = new Integer[]{1, 6, 10, 25, 78} ; 
 *		
 *		List<Integer> randomNumbers = Arrays.asList(arr);
 *		
 *		//Sum of List
 *		Integer sum = randomNumbers.stream().mapToInt(Integer::intValue).sum();
 *		System.out.println(sum);
 *		
 *		//Max of List
 *		Integer maxi = randomNumbers.stream().max(Comparator.comparing(Integer::intValue)).get();
 *		System.out.println(maxi);
 *		
 *		Integer mini = randomNumbers.stream().min(Comparator.comparing(Integer::intValue)).get();
 *		System.out.println(mini);
 *	} 
 *
 *}
 *
 */

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class RandomNumbers {
	
	public static void main(String[] args) {
		
		Integer[] arr = new Integer[]{1, 6, 10, 25, 78} ; 
		
		List<Integer> randomNumbers = Arrays.asList(arr);
		
		//Sum of List
		Integer sum = randomNumbers.stream()
								   .mapToInt(Integer::intValue)
								   .sum();
		System.out.println(sum);
		
		//Max of List
		Integer maxi = randomNumbers.stream()
									.max(Comparator.comparing(Integer::intValue))
									.get();
		System.out.println(maxi);
		
		Integer mini = randomNumbers.stream()
								    .min(Comparator.comparing(Integer::intValue))
								    .get();
		System.out.println(mini);
	}

}
