package collections.streams;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Problem Statement
 * 1.Write a program to print minimal person with name and email address from the Person class using java.util.Stream<T> API as List
 * 
 * Requirement
 * 1.Write a program to print minimal person with name and email address from the Person class using java.util.Stream<T> API as List
 * 
 * Method Signature
 * 
 * Entity
 * 1.MinimalPersonList
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method and store the person in persons list
 * 2.Check that every person that person Name is not equal to null
 * 3.Check the person email Address is not equal to null
 * 4.print the person
 * 
 * Pseudo Code
 * 
 * class MinimalPersonList {
 * 
 * 		public static void main(String[] args ){
 * 	
 * 		List<Person> persons = Person.createRoster();
 *		
 *		List<Person> emptyPerson = persons.stream().map(person -> {
 *			if(person.getName() != null) {
 *				if(person.getEmailAddress() != null) {
 *					System.out.println(person);
 *				}
 *			}
 *		}).collect(Collectors.toList()); 
 *														 
 *		System.out.println(emptyPerson);
 * 		}
 * }
 * @author UKSD
 *
 */

public class MinimalPersonList {
	
	public static void main(String[] args ) {
		
		List<Person> persons = Person.createRoster();
		
		
		List<Person> nonEmpty = persons.stream()
									   .filter(person -> person.getName() != null)
									   .filter(person -> person.getEmailAddress() != null)
									   .collect(Collectors.toList());
		
		System.out.println(nonEmpty);
	}

}
