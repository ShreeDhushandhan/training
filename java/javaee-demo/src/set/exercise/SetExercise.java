package set.exercise;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * Problem Statement
 * 1.+ Create a set
 *    => Add 10 values
 *    => Perform addAll() and removeAll() to the set.
 *    => Iterate the set using 
 *        - Iterator
 *        - for-each
 * 2.+ Explain the working of contains(), isEmpty() and give example.
 * 
 * Requirement
 * 1.+ Create a set
 *    => Add 10 values
 *    => Perform addAll() and removeAll() to the set.
 *    => Iterate the set using 
 *        - Iterator
 *        - for-each
 * 2.+ Explain the working of contains(), isEmpty() and give example.
 *   
 * Entity
 * 1.SetExercise 
 * 
 * Method Signature
 * 
 * Work Done
 * 1.Create a hashset called newSet
 * 2.Add values to the set
 * 3.create another set called setTwo
 * 4.Add the items of the setTwo to newSet
 * 5.Remove all the items of the Set
 * 6.Iterate the set and print the items in the Set.
 * 7.Get the value from the User
 * 8.Check whether the input is present in the Set or not	
 * 		8.1)If the value is present print Exists
 * 		8.2)If not print Not Exists
 * 9.Check whether the Set is empty or not
 * 		9.1)If the Set is empty then print Empty
 * 		9.2)If not print Not Empty
 *   
 *   
 * @author UKSD
 *
 */
public class SetExercise {
	
	public static void main(String[] args) {
		
		//Creating Set
		Set<String> newSet = new HashSet<String>();
		newSet.add("Shoe");
		newSet.add("Laptop");
		newSet.add("Phone");
		newSet.add("Headphones");
		newSet.add("Pen");
		newSet.add("Mouse");
		newSet.add("Speaker");
		newSet.add("Plug");
		newSet.add("Mask");
		newSet.add("Toy");
		
		Set<String> setTwo = new HashSet<String>();
		setTwo.add("Spray");
		setTwo.add("Pillow");
		setTwo.add("Bottle");
		
		//addAll() Method
		setTwo.addAll(newSet);
		System.out.println(setTwo);
		
		//removeAll() Method
		setTwo.removeAll(newSet);
		System.out.println(setTwo);
		
		//Iterating the Set
		Iterator it = newSet.iterator();
		
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		//Iterating using for each loop
		for(String val:setTwo) {
			System.out.println(val);
		}
		
		//contains()  
		if(newSet.contains("Pencil")) {
			System.out.println("Exists");
		} else {
			System.out.println("Not Exists");
		}
		
		//isEmpty()
		if(newSet.isEmpty()) {
			System.out.println("Empty");
		} else {
			System.out.println("Not Empty");
		}
		
	}

}
