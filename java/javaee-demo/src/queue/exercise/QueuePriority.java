package queue.exercise;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Problem Statement
 * 1.Create a queue using generic type and in both implementation Priority Queue and complete following 
 * 	 -> add atleast 5 elements
 *   -> remove the front element
 *   -> search a element in stack using contains key word and print boolean value value
 *   -> print the size of stack
 *   -> print the elements using Stream 
 *   
 * Requirement
 * 1.Create a queue using generic type and in both implementation Priority Queue and complete following 
 * 	 -> add atleast 5 elements
 *   -> remove the front element
 *   -> search a element in stack using contains key word and print boolean value value
 *   -> print the size of stack
 *   -> print the elements using Stream 
 *   
 * Method Signature
 *   
 * Entity
 * 1.QueuePriority 
 * 2.QueueFunctions 
 * 
 * Jobs to be Done
 * 1.Create a new PriorityQueue as pQueue.
 * 2.Overide the method of the interface.
 * 3.Print the Queue 
 * 4.Remove the Top Element of the Queue
 * 5.Get the Value from the user
 * 6.Search for the value in the queue
 * 		6.1)If exist then print Value Exists
 * 		6.2)If not print Value does not Exist
 * 7.Print the size of the Queue
 * 8.Print the values of the queue
 * 
 * @author UKSD
 *
 * @param <S>
 */

public class QueuePriority<S> implements QueueFunctions<S> {
	
	public static void main(String[] args) {
		
		QueuePriority<Integer> pQueue = new QueuePriority<>();
		
		
		//PriorityQueue is Created.
		Queue<Integer> PQueue = new PriorityQueue<Integer>();
		
		//Adding Values to the Queue
		pQueue.addElement(PQueue,9090);
		pQueue.addElement(PQueue,9000);
		pQueue.addElement(PQueue,9080);
		pQueue.addElement(PQueue,9080);
		pQueue.addElement(PQueue,9070);
		pQueue.addElement(PQueue,9060);
		pQueue.addElement(PQueue,9050);
		
		System.out.println(PQueue);
		
		
		//Removing the Front Element
		pQueue.removeFront(PQueue);
		
		System.out.println(PQueue);
		
		//Size of the Queue 
		System.out.println("Size " + PQueue.size());
		
		//Searching the Element from the Queue
		if(pQueue.search(PQueue,8080) ) {
			System.out.println("Value Exists");
		} else {
			System.out.println("Value Does not Exists");
		}
		
		//Searching the Value with Stream
		PQueue.stream().forEach(x->System.out.println(x));
	}

	@Override
	public void addElement(Queue<S> queue, S val) {
		queue.add(val);
		
	}

	@Override
	public void removeFront(Queue<S> queue) {
		queue.remove();
		
	}

	@Override
	public boolean search(Queue<S> queue, S searchVal) {
		if(queue.contains(searchVal)) {
			return true;
		} else {
			return false;
		}
	}
	

}
