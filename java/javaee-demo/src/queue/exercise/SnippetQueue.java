package queue.exercise;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Problem Statement
 * 1.Consider a following code snippet and what will be output and complete the code.
 * 
 * Requirement
 * 1.Consider a following code snippet and what will be output and complete the code.
 * 
 * Method Signature
 * 
 * Entity
 * 1.SnippetQueue
 * 
 * Output
 * null
 * 
 * Answer
 * 1.Since the Queue bike does not have any items it returns nothing while printing the Queue
 * 2.The poll method is used to print the Queue by removing the duplicates of the elements in the Queue
 * 3.Then the peek method prints the top element of the Queue, but here nothing is inside it so it wont print Anything 
 *   to the Console.
 * @author UKSD
 *
 */

public class SnippetQueue {
	
	public static void main(String[] args) {
		Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());   
	}

}
