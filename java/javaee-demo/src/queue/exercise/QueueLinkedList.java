package queue.exercise;

import java.util.LinkedList;
import java.util.Queue;
import java.util.stream.Collectors;

/**
 * Problem Statement
 * 1.Create a queue using generic type and in both implementation Linked list and complete following 
 * 	 -> add atleast 5 elements
 *   -> remove the front element
 *   -> search a element in stack using contains key word and print boolean value value
 *   -> print the size of stack
 *   -> print the elements using Stream 
 *   
 * Requirement
 * 1.Create a queue using generic type and in both implementation Linked list and complete following 
 * 	 -> add atleast 5 elements
 *   -> remove the front element
 *   -> search a element in stack using contains key word and print boolean value value
 *   -> print the size of stack
 *   -> print the elements using Stream 
 *   
 * Method Signature
 * 1.public void addElement(Queue queue, Object val) ;
 * 2.public void removeFront(Queue queue) ;
 * 3.public boolean search(Queue queue, Object searchVal);
 *   
 * Entity
 * 1.QueueStart Generic Class implements QueueFunctions Interface.
 * 
 * Jobs to be Done
 * 1.Create a new LinkedList Queue as newQueue.
 * 2.Overide the method of the interface.
 * 3.Print the Queue 
 * 4.Remove the Top Element of the Queue
 * 5.Get the Value from the user
 * 6.Search for the value in the queue
 * 		6.1)If exist then print Value Exists
 * 		6.2)If not print Value does not Exist
 * 7.Print the size of the Queue
 * 8.Print the values of the queue
 * 
 * 
 * @author UKSD
 *
 * @param <S>
 */

public class QueueLinkedList<S> implements QueueFunctions<S>{
	
	@Override
	public void addElement(Queue queue, Object val) {
		queue.add(val);
		
	}

	@Override
	public void removeFront(Queue queue) {
		queue.remove();
		
	}

	@Override
	public boolean search(Queue queue, Object searchVal) {
		if(queue.contains(searchVal)) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public static void main(String[] args) {
		
		QueueLinkedList<String> que = new QueueLinkedList();
		
		//Linked List Generic Queue
		Queue<String> newQueue = new LinkedList<String>();
		
		//Adding Elements to Queue
		que.addElement(newQueue,"D");
		que.addElement(newQueue,"H");
		que.addElement(newQueue,"U");
		que.addElement(newQueue,"S");
		que.addElement(newQueue,"H");
		que.addElement(newQueue,"A");
		que.addElement(newQueue,"N");
		que.addElement(newQueue,"D");
		que.addElement(newQueue,"H");
		que.addElement(newQueue,"A");
		que.addElement(newQueue,"N");
		
		System.out.println("Normal Queue");
		System.out.println(newQueue);
		
		//Remove the First Element of the Queue
		que.removeFront(newQueue);
		System.out.println("Top Removed Queue");
		System.out.println(newQueue);
		
		//Search in Queue
		System.out.println("Value Search");
		if(que.search(newQueue, "H")) {
			System.out.println("Value Exists");
		} else {
			System.out.println("Value Does not Exists");
		}
		
		//Size of the Queue
		System.out.println("Size " + newQueue.size());
		
		//Printing the Queue using Stream
		System.out.println("Printing using Stream");
		newQueue.stream().forEach(x->System.out.println(x));
		
	}

}
