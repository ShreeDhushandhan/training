package queue.exercise;

import java.util.Queue;


/**
 * Entity
 * 1.QueueFunctions Interface..
 * 
 * Work Done
 * 1.Created a public addElement method which takes two parameters queue and value to insert in the Queue and returns void.
 * 2.Created a public removeFront method which takes one parameter queue to remove from the Queue and returns void.
 * 3.Created a public search method which takes two parameters queue and value to search in the Queue and returns void.
 * @author UKSD
 *
 * @param <S>
 */
public interface QueueFunctions<S> {
	
	public void addElement(Queue<S> queue, S val);
	
	public void removeFront(Queue<S> queue);
	
	public boolean search(Queue<S> queue, S searchVal);

}
