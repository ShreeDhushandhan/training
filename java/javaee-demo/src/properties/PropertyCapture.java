package properties;

/**
 * Problem Statement
 * 1.Get and print a configurable property based on following priority:
 * - args passed for java cmd
 * - if not avaialble as cmd args, try to get from System env variables
 * - if not available from Sys Env, try to get from a customized .properties file (D:\temp\customize.properties)
 * - if not available in customized props files, get from a default .properties file (\\nas\Distribution\training\temp\default.properties)
 * - if not available in default props, load from a harcoded default constant
 * Try to use Properties.setParent() heirarchy mechanism to set out the priorities
 * 
 * Requirement
 * 1.Get and print a configurable property based on following priority:
 * - args passed for java cmd
 * - if not avaialble as cmd args, try to get from System env variables
 * - if not available from Sys Env, try to get from a customized .properties file (D:\temp\customize.properties)
 * - if not available in customized props files, get from a default .properties file (\\nas\Distribution\training\temp\default.properties)
 * - if not available in default props, load from a harcoded default constant
 * Try to use Properties.setParent() heirarchy mechanism to set out the priorities
 * 
 * Entity
 * 1.PropertyCapture
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a Filereader to read the config.properties file
 * 2.Create a Properties object called properties
 * 3.load the file to the properties
 * 4.Store the user property to user String 
 * 5.Store the pass property to pass String
 * 6.Store the ip property to ip String
 * 7.Store the access property to access String    
 * 8.print the properties.
 * 
 * Pseudo Code
 * 
 * class PropertyCapture {
 *	
 *	public static void main(String[] args) throws IOException{
 *		
 *		FileReader reader = new FileReader("C:\\Users\\UKSD\\eclipse-workspace\\javaee-demo\\resource\\config.properties");
 *		
 *		Properties properties = new Properties();
 *		
 *		properties.load(reader);
 *		
 *		String user = properties.getProperty("user");
 *		String pass = properties.getProperty("pass");
 *		String ip = properties.getProperty("ip");
 *		String access = properties.getProperty("access");
 *		
 *		
 *		System.out.println(user+" "+pass+" "+ip+" "+access);
 *		
 *	} 
 *
 *}
 *
 * 
 */

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyCapture {
	
	public static void main(String[] args) throws IOException{
		
		FileReader reader = new FileReader("C:\\Users\\UKSD\\eclipse-workspace\\javaee-demo\\resource\\config.properties");
		
		Properties properties = new Properties();
		
		properties.load(reader);
		
		String user = properties.getProperty("user");
		String pass = properties.getProperty("pass");
		String ip = properties.getProperty("ip");
		String access = properties.getProperty("access");
		
		
		System.out.println(user+" "+pass+" "+ip+" "+access);
		
	}

}
