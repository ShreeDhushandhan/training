package properties;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Problem Statement
 * 1.Load and print localized greetings text based on a given Locale (in_en and in_ta)
 * 
 * Requirement
 * 1.Load and print localized greetings text based on a given Locale (in_en and in_ta)
 * 
 * Entity
 * 1.LocaleFinder
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a ResourceBundle and name it as bundle
 * 2.Pass the parameter as message and locale english
 * 3.print the bundle
 * 4.Create another ResourceBundle and name it as bundleTwo
 * 5.Pass the parameter as message and locale taiwan
 * 6.print the bundleTwo
 * 
 * Pseudo Code
 * @author UKSD
 *
 */

public class LocaleFinder {
	
	public static void main(String[] args) {
		
		ResourceBundle bundle = ResourceBundle.getBundle("messages",Locale.ENGLISH);
		System.out.println(bundle.getString("message"));
		
		
		ResourceBundle bundleTwo = ResourceBundle.getBundle("message",Locale.TAIWAN);
		
		System.out.println(bundleTwo.getString("message"));
		
	}

}
