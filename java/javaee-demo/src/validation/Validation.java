package validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Problem Statement
 * 1.Why validation is important?Explain with example.
 * 2.Is the following code can be done as it is?If not correct 
 *  the code and give the reason for the change of code.
 *  
 * Requirement
 * 1.Why validation is important?Explain with example.
 * 2.Is the following code can be done as it is?If not correct 
 *  the code and give the reason for the change of code.
 *  
 * Method Signature
 * 1.private static boolean validate(String user, String pass);
 * 
 * Entity
 * 1.Validation Class
 * 
 * 
 * Jobs to be Done.
 * 1.Get the Username and password from the user
 * 2.Check whether the Username and password provided by the user is present in the List
 * 		2.1)If the username and password is present in the list then
 * 			2.1.1)Print Access Granted
 * 			2.1.2)If the username and password does not match then print try Again
 * 
 *       
 *       
 * Answer:
 * 1.Yes, Validation is Important since without validation the provided value will 
 * 	 not be known as existing or new in the Database
 * 
 * 2.No, this code can much be modified.
 *   Here if the user and address is already available and present then the user and adddress is validated 
 *   or else the user and address is verified and added to the list..  
 * 
 * MODIFIED CODE:
 * 	PROGRAM:
 * 		check if user already exists
 * 		validate user
 * 		validate address
 * 		
 * 		else
 * 	    verify user
 * 		verify address
 * 		insert user
 * 		insert address
 * 
 * Pseudo Code
 * class Validation {
 *	
 *	public static void main(String[] args) {
 *		Scanner scan = new Scanner(System.in);
 *		System.out.println("Enter the Username");
 *		String user = scan.nextLine();
 *		System.out.println("Enter the Password");
 *		String pass = scan.nextLine();
 *		validate(user,pass);
 *		
 *		if(validate(user,pass)) {
 *			System.out.println("Access Granted");
 *		} else {
 *			System.out.println("Try Again");
 *		}
 *		
 *	} 
 *
 *	private static boolean validate(String user, String pass) {
 *		List<String> users = new ArrayList<String>();
 *		List<String> passo = new ArrayList<String>();
 *		
 *		users.add("Shree Dhushadhan");
 *		users.add("Shree Sanjay");
 *		
 * 		passo.add("qwert123");
 *		passo.add("poiuy123");
 *		
 *		
 *		if(users.contains(user) || passo.contains(pass)) {
 *			return true;
 *		} else {
 *			return false;
 *		}
 *	}
 *
 *}
 *
 * 
 * @author UKSD
 *
 */

public class Validation {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Username");
		String user = scan.nextLine();
		System.out.println("Enter the Password");
		String pass = scan.nextLine();
		validate(user,pass);
		
		if(validate(user,pass)) {
			System.out.println("Access Granted");
		} else {
			System.out.println("Try Again");
		}
		
	}

	private static boolean validate(String user, String pass) {
		List<String> users = new ArrayList<String>();
		List<String> passo = new ArrayList<String>();
		
		users.add("Shree Dhushadhan");
		users.add("Shree Sanjay");
		
		passo.add("qwert123");
		passo.add("poiuy123");
		
		
		if(users.contains(user) || passo.contains(pass)) {
			return true;
		} else {
			return false;
		}
	}

}
