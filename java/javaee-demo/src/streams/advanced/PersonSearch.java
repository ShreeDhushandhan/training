package streams.advanced;

import java.util.List;

/**
 * Problem Statement
 * 1.Contains
 *   - Consider the following Person:
 *       new Person(
 *           "Bob",
 *           IsoChronology.INSTANCE.date(2000, 9, 12),
 *           Person.Sex.MALE, "bob@example.com"));
 *   - Check if the above person is in the roster list obtained from Person class.
 *   
 * Requirement
 * 1.Contains
 *   - Consider the following Person:
 *       new Person(
 *           "Bob",
 *           IsoChronology.INSTANCE.date(2000, 9, 12),
 *           Person.Sex.MALE, "bob@example.com"));
 *   - Check if the above person is in the roster list obtained from Person class.
 *   
 * Entity
 * 1.PersonSearch
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method in the person class and store it in persons list.
 * 2.Check the list if the Person named Bob is present in the list
 * 		2.1)print the true 
 * 		2.2)if the person is not available in the list print false
 * 
 * Pseudo Code
 * class PersonSearch {
 * 	
 *	public static void main(String[] args) {
 *		
 *		List<Person> persons = Person.createRoster();
 *		
 *		System.out.println(persons.stream()
 *								  .filter(person -> persons.contains(person) ? true : null));
 *	}
 *
 *}
 * @author UKSD
 *
 */

public class PersonSearch {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		
		System.out.println(persons.stream()
								  .filter(person -> persons.contains(person) ? true : null));
	}

}
