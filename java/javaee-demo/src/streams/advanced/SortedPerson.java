package streams.advanced;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Problem Statement
 * 1. Sorting
 *   - sort the roster list based on the person's age in descending order using java.util.Stream
 *   
 * Requirement
 * 1. Sorting
 *   - sort the roster list based on the person's age in descending order using java.util.Stream
 *   
 * Entity
 * 1.SortedPerson
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method and store it in persons list.
 * 2.Sort the persons list based on the person age and store it in sortedPersons list
 * 3.print the sortedPersons list.
 * 
 * Pseudo Code
 * class SortedPerson {
 *	
 *	public static void main(String[] args) {
 *		List<Person> persons = Person.createRoster();
 *		
 *		List<Person> sortedPersons = persons.stream()
 *											.sorted((Person person1, Person person2) -> person2.getAge()- person1.getAge())
 *											.collect(Collectors.toList());
 *		
 *		sortedPersons.forEach(person -> System.out.println(person.getName()));
 *	} 
 *
 *}
 * @author UKSD
 *
 */

public class SortedPerson {
	
	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		
		List<Person> sortedPersons = persons.stream()
											.sorted((Person person1, Person person2) -> person2.getAge()- person1.getAge())
											.collect(Collectors.toList());
		
		sortedPersons.forEach(person -> System.out.println(person.getName()));
	}

}
