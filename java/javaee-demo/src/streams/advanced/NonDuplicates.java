package streams.advanced;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Problem Statement
 * 1.GroupBy, duplicate removal
 *  - Consider a following code snippet:
 *       List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
 *  - Get the non-duplicate values from the above list using java.util.Stream API
 *  
 * Requirement
 *  1.GroupBy, duplicate removal
 *  - Consider a following code snippet:
 *       List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
 *  - Get the non-duplicate values from the above list using java.util.Stream API
 *  
 * Entity
 * 1.NonDuplicates
 * 
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.Store the array values in list randomNumbers
 * 2.Use Distinct function to remove the duplicates form the list and store it in removedDuplicates list.
 * 3.Print the removedDuplicates List. 
 * 
 * 
 * Pseudo Code
 * class NonDuplicates {
 *	
 *	public static void main(String[] args) {
 *		
 *		List<Integer> randomNumbers = new ArrayList<Integer>(Arrays.asList(1,6,10,1,25,78,10,25));
 *		
 *		Map<Integer, Long> nonDuplicates = randomNumbers.stream()
 *														.collect(Collectors
 *														.groupingBy(Function.identity()
 *														,Collectors.counting()));
 *		
 *		List<Integer> removedDuplicate = randomNumbers.stream().distinct().collect(Collectors.toList());
 *		
 *		System.out.println(removedDuplicate);
 *	} 
 *
 *}
 * @author UKSD
 *
 */

public class NonDuplicates {
	
	public static void main(String[] args) {
		
		List<Integer> randomNumbers = new ArrayList<Integer>(Arrays.asList(1,6,10,1,25,78,10,25));
		
		Map<Integer, Long> nonDuplicates = randomNumbers.stream()
														.collect(Collectors
														.groupingBy(Function.identity()
														,Collectors.counting()));
		
		List<Integer> removedDuplicate = randomNumbers.stream()
													  .distinct()
													  .collect(Collectors.toList());
		
		System.out.println(removedDuplicate);
	}

}
