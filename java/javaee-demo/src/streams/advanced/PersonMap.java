package streams.advanced;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Problem Statement
 * 1.map
 *   - Write a program to print minimal person with name and email address from the Person class using java.util.Stream<T>#map API
 *   
 * Requirement
 * 1.map
 *   - Write a program to print minimal person with name and email address from the Person class using java.util.Stream<T>#map API
 *   
 * Entity
 * 1.PersonMap
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method from the Person class and store in the person list.
 * 2.Create a List called nonEmpty 
 * 3.Check each person 
 * 		3.1)Person's name is not null
 * 		3.2)Person's email address not null
 * 4.Store the person in the nonEmpty list
 * 5.print name of the Person from the nonEmpty list.
 * 
 * Pseudo Code
 * @author UKSD
 *
 */

public class PersonMap {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		
		List<Person> nonEmpty = persons.stream()
									   .filter(person -> person.getName() != null)
									   .filter(person -> person.getEmailAddress() != null)
									   .map(person -> person)
									   .collect(Collectors.toList());
		
		nonEmpty.stream().forEach(person -> System.out.println(person.getName()));
		
	}

}
