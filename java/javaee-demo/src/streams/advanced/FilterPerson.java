package streams.advanced;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Problem Statement
 * 1.filter
 *   - predicates
 *       - Write a program to filter the Person, who are male and age greater than 21
 *       
 * Requirement
 * 1.filter
 *   - predicates
 *       - Write a program to filter the Person, who are male and age greater than 21
 *       
 * Entity
 * 1.FilterPerson
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster from the Person Class and store in persons list.
 * 2.Filter the list where the person's GENDER is male and AGE is greater than 21
 * 4.Store the filtered persons in filteredPersons list.
 * 3.Print Person name from the filteredPersons list.
 * 
 * Pseudo Code
 * 
 * class FilterPerson {
 * 
 * 		public static void main(String[] args){
 * 			List<Person> persons = Person.createRoster();
 * 			List<Person> filteredPerson = persons.stream()
 * 				   .filter(person -> person.Gender == Sex.Male)
 * 				   .filter(person -> person.Age > 21)
 * 				   .mapToInt()
 * 				   .collect(Collectors.troList());
 * 			filteredPerson.stream().forEach(person -> System.out.println(person.getName()));
 * 		
 * 		}
 * 
 * }
 * @author UKSD
 *
 */

public class FilterPerson {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		List<Person> filteredPerson = persons.stream()
											 .filter(person -> person.getGender() == Person.Sex.MALE)
											 .filter(person -> person.getAge() > 21)
											 .collect(Collectors.toList());
		filteredPerson.stream().forEach(person -> System.out.println(person.getName()));
		
	}

}
