package streams.advanced;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Problem Statement
 * 1.Sum/Max/Min
 *   - Consider a following code snippet:
 *       List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
 *   - Find the sum of all the numbers in the list using java.util.Stream API
 *   - Find the maximum of all the numbers in the list using java.util.Stream API
 *   - Find the minimum of all the numbers in the list using java.util.Stream API
 *   
 * Requirement
 * 1.Sum/Max/Min
 *   - Consider a following code snippet:
 *       List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
 *   - Find the sum of all the numbers in the list using java.util.Stream API
 *   - Find the maximum of all the numbers in the list using java.util.Stream API
 *   - Find the minimum of all the numbers in the list using java.util.Stream API
 *   
 * Entity
 * 1.RandomNumbers
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.store the array as list in randomNumbers.
 * 2.Add all the items from the list and store in totalNumbers
 * 3.Check the list and store the maximum item of the list in maxi
 * 4.Check the list and store the minimum item of the list in mini
 * 5.Print the totalNumbers, maxi, mini.
 * 
 * 
 * Pseudo Code
 * 
 * class RandomNumbers {
 * 
 * 		public static void main(String[] args) {
 * 
 * 			List<Integer> randomNumbers = Arrays.asList((1, 6, 10, 25, 78));
 * 			
		
			Integer total = randomNumbers.stream()
										 .mapToInt(Integer::intValue)
										 .sum();
		
			Integer maxi = randomNumbers.stream()
										.max(Comparator.comparing(Integer::intValue))
										.get();
		
			Integer mini = randomNumbers.stream()
										.min(Comparator.comparing(Integer::intValue))
										.get();
			System.out.println(total);
			System.out.println(maxi);
			System.out.println(mini);
 * 
 * 		}	
 * 
 * }
 * 
 * @author UKSD
 *
 */

public class RandomNumbers {
	
	public static void main(String[] args) {
		
		List<Integer> randomNumbers = new ArrayList<>(Arrays.asList(1, 6, 10, 25, 78));
		
		Integer total = randomNumbers.stream()
									 .mapToInt(Integer::intValue)
									 .sum();
		
		Integer maxi = randomNumbers.stream()
									.max(Comparator.comparing(Integer::intValue))
									.get();
		
		Integer mini = randomNumbers.stream()
									.min(Comparator.comparing(Integer::intValue))
									.get();
		
		System.out.println(total);
		System.out.println(maxi);
		System.out.println(mini);
	}

}
