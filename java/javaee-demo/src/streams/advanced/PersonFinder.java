package streams.advanced;

import java.util.List;
import java.util.Optional;

import collections.streams.Person;
import collections.streams.Person.Sex;

/**
 * Problem Statement
 * 1.findFirst/last/any
 *   - Write a program to filter the Person, who are male and
 *       - find the first person from the filtered persons
 *       - find the last person from the filtered persons
 *       - find random person from the filtered persons
 *       
 * Requirement
 *  1.findFirst/last/any
 *   - Write a program to filter the Person, who are male and
 *       - find the first person from the filtered persons
 *       - find the last person from the filtered persons
 *       - find random person from the filtered persons
 *       
 * Entity
 * 1.PersonFinder
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method from the Person Class and store it in persons list.
 * 2.Filter the person who's Gender is MALE 
 * 3.Store the First person of the list in the firstPerson 
 * 4.Store the Last Person of the list in the lastPerson
 * 5.Store a random Person from the list in the randomPerson
 * 
 * Pseudo Code
 * 
 * class PersonFinder {
 * 
 * 		public static void main(String[] args) {
 * 			List<Person> persons = Person.createRoster();
 * 
 * 			Optional<Person> firstPerson = persons.stream()
 * 										.filter(person -> person.getGender() == Person.Sex.MALE)
 * 										.map(person -> person)
 * 										.findFirst();
 * 			System.out.println(firstPerson.toString());
 * 
 * 			
 * 										
 * 										
 * 										
 * 
 * 		}
 * }
 * @author UKSD
 *
 */

public class PersonFinder {
	
	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		
		Optional<Person> firstPerson = persons.stream()
											  .filter(person -> person.getGender() == Person.Sex.MALE)
											  .map(person -> person)
											  .findFirst();
		System.out.println("First Person");
		System.out.println(firstPerson.toString());
		
		Person lastPerson = persons.stream()
								   .filter(person -> person.getGender() == Sex.MALE)
								   .reduce((first,last) -> last)
								   .orElse(null);
		
		System.out.println("Last Person");
		System.out.println(lastPerson.toString());
		
		Optional<Person> randomPerson = persons.stream()
											   .filter(person -> person.getGender() == Person.Sex.MALE)
											   .findAny();
		
		System.out.println("Random Person");
		System.out.println(randomPerson.toString());
		
	}

}
