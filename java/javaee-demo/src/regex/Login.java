package regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Problem Statement
 * 1.Create a pattern for password which contains
 *    -> 8 to 15 characters in length
 *    -> Must have at least one uppercase letter
 *    -> Must have at least one lower case letter
 *    -> Must have at least one digit
 *    
 * Requirement
 * * 1.Create a pattern for password which contains
 *    -> 8 to 15 characters in length
 *    -> Must have at least one uppercase letter
 *    -> Must have at least one lower case letter
 *    -> Must have at least one digit
 * 
 *    
 * Method Signature
 * public void validator(String pass);
 * 
 * Entity
 * 1.Login
 * 
 * Jobs to be Done.
 * 1.Get the Password from the User.
 * 2.Check the password is null or space or any Special Characters if any 
 * 		2.1)Throw Exception that password should not be null or any Special Characters.
 * 3.Create a Pattern that
 * 		3.1)Checks whether the password is 8 to 15 characters long.
 * 		3.2)Checks password has one Upper case letter
 * 		3.3)Checks password has one Lower case letter
 * 		3.4)Checks password has one number.
 * 4.Check whether the password matches the pattern 
 * 		4.1)If the password matches then Print "Password Valid"
 * 		4.2)If the password does not matches then Print "Password should must be 8-15 characters long, contains 1 Upper case Letter,
 *          1 Lower case Letter and 1 Numeric and must not contain blank space." 
 *          
 * Pseudo Code
 * public class Login {
 *	
 *	public boolean validator(String pass) {
 *		
 *		String constrains =  "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" +  "(?=\\S+$).{8,15}$";
 *		
 *		Pattern pattern = Pattern.compile(constrains);
 *		Matcher matcher = pattern.matcher(pass);
 *		
 *		return matcher.matches();
 *		
 *	}
 *	
 *	public static void main(String[] args) {
 *		
 *		Login login = new Login();
 *		Scanner scan = new Scanner(System.in);
 *		System.out.println("Enter the Password");
 *		
 *		String passIn = scan.nextLine();
 *		
 *		if(login.validator(passIn)) {
 *			System.out.println("Password Valid");
 *		} else {
 *			System.out.println("Password should must be 8-15 characters long, " + "\n"
 *		                        + "contains 1 Upper case Letter,"  + "\n" 
 *								+ "contains 1 Lower case Letter," + "\n" 
 *		                        + "contains 1 Number,"  + "\n" 
 *								+ "and must not be Blank Space");
 *		}
 *		
 *		
 *	} 
 *
 *}
 * @author UKSD
 *
 */

public class Login {
	
	public boolean validator(String pass) {
		
		String constrains =  "^(?=.*[0-9])" + 
							 "(?=.*[a-z])(?=.*[A-Z])" + 
							 "(?=\\S+$).{8,15}$";
		
		Pattern pattern = Pattern.compile(constrains);
		Matcher matcher = pattern.matcher(pass);
		
		return matcher.matches();
		
	}
	
	public static void main(String[] args) {
		
		Login login = new Login();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Password");
		
		String passIn = scan.nextLine();
		
		if(login.validator(passIn)) {
			System.out.println("Password Valid");
		} else {
			System.out.println("Password should must be 8-15 characters long, " + "\n"
		                        + "contains 1 Upper case Letter,"  + "\n" 
								+ "contains 1 Lower case Letter," + "\n" 
		                        + "contains 1 Number,"  + "\n" 
								+ "and must not be Blank Space");
		}
		
		
	}

}
