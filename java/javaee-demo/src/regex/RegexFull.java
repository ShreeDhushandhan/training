package regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Problem Statement
 * 1.create a program which throws exception and fetch all the details of the exception.
 * 2.Demonstrate establish the difference between lookingAt() and matches().
 * 3.Demonstrate the different types of groups in Regex.
 * 4.Demonstrate all the fields of the pattern class.
 * 5.Find the no of occurrence of a pattern in the text and also fetch the start and end index of the the occurrence.
 * 6.Difference between replaceAll() and appendReplacement().
 * 7.Split any random text using any pattern you desire.
 *   
 * Requirement
 * 1.create a program which throws exception and fetch all the details of the exception.
 * 2.Demonstrate establish the difference between lookingAt() and matches().
 * 3.Demonstrate the different types of groups in Regex.
 * 4.Demonstrate all the fields of the pattern class.
 * 5.Find the no of occurrence of a pattern in the text and also fetch the start and end index of the the occurrence.
 * 6.Difference between replaceAll() and appendReplacement().
 * 7.Split any random text using any pattern you desire.
 *   
 * Method Signature
 * 
 * Entity
 * 1.RegexFull
 * 
 * Jobs to be Done.
 * 1.Create a Exception throwing snippet
 * 		1.1)catch the Exception and print the Detail about the Exception
 * 
 * 2.Get the input from the User.
 * 		2.1)Check the First Part of the input is same of the message or not.
 * 				2.1.1)If it is Same then print Same
 * 				2.1.2)If is is not Same the print Not Same
 * 		2.2)Check the Full input is same of the message or not.
 * 				2.2.1)If it is Same then print Same.
 * 				2.2.2)If it is not same then print Not Same.
 * 
 * 3.Get the Input from the User.
 * 		3.1)Check the Whole input from the user and print if it matches the Database.
 * 		3.2)Check and print if there is any number with character in front of it.
 * 		3.3)Check and print if there is any number with character after it.
 * 
 * 4. 1)Create a pattern that check any character in between s and ree in the input String.
 *    2)Create a pattern that check any two numbers present after the word Shree in the input String.
 *    3)Create a pattern that check any character after characters thi.
 *    4)Create a pattern that check any number afrer shr
 *    5)Create a pattern that check "This" word at the start of the input String
 *    6)Create a pattern that check "Dhushandhan" word at the end of the input String.  
 *    7)Create a pattern that check any 2 repeating character of the last of the input String.
 *    8)Create a pattern that checks the input String contains wither Shree or Dhushandhan.
 * 
 * 5.Get the Input from the User and check the number of times the input exist in the database
 * 		5.1)Print the Start Character Index 
 * 		5.2)Print the End Character Index
 * 		5.3)Print Number of Repeats
 * 
 * 6. 1)Replace the new value Sanjay to the Occurance of the word Dhushadhan in String name. 
 * 	  2)Add the New value "Working in Google" to the name and print it. 
 * 		 
 * @author UKSD
 *
 */

public class RegexFull {
	
	public static void main(String[] args) {
		/*
		Scanner scanner = new Scanner(System.in);
		
		try {
			int oldValue = 10;
			int divi = 0;
			int c = oldValue / divi; 
		} catch (ArithmeticException ae) {
			ae.printStackTrace();
		}
		//############################################################
		//LookingAt Example
		System.out.println("Enter the Message In");
		String messageIn = scanner.nextLine();
		
		String message = "hello i am Shree Sanjay";
		Pattern pattern = Pattern.compile(message);
		Matcher matcher = pattern.matcher(messageIn);
		
		if(matcher.lookingAt()) {
			System.out.println("Same");
		} else {
			System.out.println("Not Same");
		}
		
		//Matches Example
		System.out.println("Enter the Message In");
		String messageIn1 = scanner.nextLine();
		
		String message1 = "this is Shree Dhushandhan";
		Pattern pattern1 = Pattern.compile(message1);
		Matcher matcher1 = pattern1.matcher(messageIn1);
		
		if(matcher1.matches()) {
			System.out.println("Same");
		} else {
			System.out.println("Not Same");
		}
		*/
		//############################################################
		
		String valueString = "This is Shree18 Shree Dhushandhan";
		
		//Single Groups
		String firstVal = "(.*)";
		Pattern firstPattern = Pattern.compile(firstVal);
		Matcher firstMatcher = firstPattern.matcher(valueString);
		//Multiple Groups
		String secondVal = "(.*\\d)";
		Pattern secondPattern = Pattern.compile(secondVal);
		Matcher secondMatcher = secondPattern.matcher(valueString);
		
		//Nested Groups		
		String thirdVal = "((\\d.*) (.*))";
		Pattern thirdPattern = Pattern.compile(thirdVal);
		Matcher thirdMatcher = thirdPattern.matcher(valueString);
	
		if(firstMatcher.find()) {
			System.out.println(firstMatcher.group());
		}
		
		if(secondMatcher.find()) {
			System.out.println(secondMatcher.group());
		}
		
		if(thirdMatcher.find()) {
			System.out.println(thirdMatcher.group());
		}
		
		//Regex Quantifiers and Pattern types.
		
		String numRegex = "(S.ree)";
		Pattern numPattern = Pattern.compile(numRegex);
		Matcher numMatcher = numPattern.matcher(valueString);
		
		if(numMatcher.find()) {
			System.out.println(numMatcher.group());
		}
		
		String numRegex1 = "(Shree\\d\\d)";
		Pattern numPattern1 = Pattern.compile(numRegex1);
		Matcher numMatcher1 = numPattern1.matcher(valueString);
		
		if(numMatcher1.find()) {
			System.out.println(numMatcher1.group());
		}
		
		String numRegex2 = "(Thi\\D)";
		Pattern numPattern2 = Pattern.compile(numRegex2);
		Matcher numMatcher2 = numPattern2.matcher(valueString);
		
		if(numMatcher2.find()) {
			System.out.println(numMatcher2.group());
		}
		
		String numRegex3 = "(shr\\w)";
		Pattern numPattern3 = Pattern.compile(numRegex3);
		Matcher numMatcher3 = numPattern3.matcher(valueString);
		
		if(numMatcher3.find()) {
			System.out.println(numMatcher3.group());
		}
		
		String numRegex4 = "(^This)";
		Pattern numPattern4 = Pattern.compile(numRegex4);
		Matcher numMatcher4 = numPattern4.matcher(valueString);
		
		if(numMatcher4.find()) {
			System.out.println(numMatcher4.group());
		}
		
		String numRegex5 = "(Dhushandhan$)";
		Pattern numPattern5 = Pattern.compile(numRegex5);
		Matcher numMatcher5 = numPattern5.matcher(valueString);
		
		if(numMatcher5.find()) {
			System.out.println(numMatcher5.group());
		}
		
		String numRegex6 = "(Shre{2})";
		Pattern numPattern6 = Pattern.compile(numRegex6);
		Matcher numMatcher6 = numPattern6.matcher(valueString);
		
		if(numMatcher6.find()) {
			System.out.println(numMatcher6.group());
		}
		
		String numRegex7 = "(.*Shree.* | .*Dhushandhan.*)";
		Pattern numPattern7 = Pattern.compile(numRegex7);
		Matcher numMatcher7 = numPattern7.matcher(valueString);
		
		if(numMatcher7.find()) {
			System.out.println(numMatcher7.group());
		}
		
		//Word Searcher
		
		String name = "Shree Dhushandhan is a very Good Boy, Good Boy in the Sense, very Good Boy.";
		Pattern dupliPattern = Pattern.compile("Good");
		Matcher dupliMatcher = dupliPattern.matcher(name);
		int count = 0;
		
		while(dupliMatcher.find()) {
			count++;
			System.out.println("Start :"+dupliMatcher.start()+"End :"+dupliMatcher.end());
		}
		System.out.println("No of Repeats :"+count);
		
		//Word ReplaceAll
		String oldVal = "(Dhushandhan)";
		String newVal = "Sanjay";
		
		Pattern replacePattern = Pattern.compile(oldVal);
		Matcher replaceMatcher = replacePattern.matcher(name);
		
		name = replaceMatcher.replaceAll(newVal);
		
		System.out.println(name);
		
		//AppendReplacement
		Pattern appRep = Pattern.compile(oldVal);
		Matcher appMat = appRep.matcher(name);
		
		StringBuffer newString = new StringBuffer();
		
		while(appMat.find()) {
			appMat.appendReplacement(newString, "Wokring in Google");
			System.out.println(newString.toString());
		}
		
		appMat.appendTail(newString);
		System.out.println(newString.toString());
		
		
		
		
		
	}

}
