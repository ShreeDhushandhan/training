package map.exercise;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.*;

/**
 * Problem Statement
 * 1.Write a Java program to copy all of the mappings from the specified map to another map
 * 
 * Requirement
 * 1.Write a Java program to copy all of the mappings from the specified map to another map
 * 
 * Method Signature
 * 
 * Entity
 * 1.MapToMap 
 * 
 * Jobs to be Done
 * 1.Create a new HashMap called oldMap and added some values to it
 * 2.Now created mapObj Object for the Class MapToMap.
 * 3.Call mapConverter private method which has the Map as a Parameter.
 * 4.In the mapConverter method it creates a new HashMap and the HashMap passed as parameter is added to the 
 *   newMap by using putAll method provided.
 * 5.Now the newMap HashMap is printed to the console by using the entrySet method and printed the key and value.
 * 
 * Pseudo Code
 * class MapToMap {
 *	
 *	private void mapConverter(Map<Integer, String> oldMap) {
 *			
 *			//Another HashMap to store all the new HashMap values.
 *			Map<Integer,String> newMap = new HashMap<Integer,String>();
 *			
 *			//OldMap values are added to the newMap. 
 *			newMap.putAll(oldMap);
 *			
 *			for(Map.Entry<Integer, String> mapValues : newMap.entrySet()) {
 *				System.out.println(mapValues.getKey()+" "+mapValues.getValue());
 *			} 
 *	}
 *	
 *	public static void main(String[] args) {
 *		
 *		//New HashMap
 *		Map<Integer,String> oldMap = new HashMap<Integer,String>();
 *		
 *		//Values Added.
 *		oldMap.put(1,"Shree Dhushadhan");
 *		oldMap.put(2,"Shree Sanjay");
 *	 	oldMap.put(3,"Kumudha");
 *		
 *		System.out.println(oldMap);
 *		
 *		//MapToMap Class Object
 *		MapToMap mapObj = new MapToMap();
 *		
 *		//mapConverter method is called.
 *		mapObj.mapConverter(oldMap);
 *		
 *	}
 *}
 * @author UKSD
 *
 */
public class MapToMap {
	
	private void mapConverter(Map<Integer, String> oldMap) {
			
			//Another HashMap to store all the new HashMap values.
			Map<Integer,String> newMap = new HashMap<Integer,String>();
			
			//OldMap values are added to the newMap.
			newMap.putAll(oldMap);
			
			for(Map.Entry<Integer, String> mapValues : newMap.entrySet()) {
				System.out.println(mapValues.getKey()+" "+mapValues.getValue());
			}
	}
	
	public static void main(String[] args) {
		
		//New HashMap
		Map<Integer,String> oldMap = new HashMap<Integer,String>();
		
		//Values Added.
		oldMap.put(1,"Shree Dhushadhan");
		oldMap.put(2,"Shree Sanjay");
		oldMap.put(3,"Kumudha");
		
		System.out.println(oldMap);
		
		//MapToMap Class Object
		MapToMap mapObj = new MapToMap();
		
		//mapConverter method is called.
		mapObj.mapConverter(oldMap);
		
	}
}
