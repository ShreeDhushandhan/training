package map.exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
/**
 * Problem Statement
 * 1.Write a Java program to test if a map contains a mapping for the specified key
 * 
 * Requirement
 * 1.Write a Java program to test if a map contains a mapping for the specified key
 * 
 * Method Signature
 * 
 * Entity
 * 1.ValuesMap 
 * 
 * Jobs to be Done
 * 1.Created a new HashMap called newMap and added some values.
 * 2.Then getting value from the user to check from the HashMap.
 * 3.Map Interface has a method called containsValue method to find the value present in the Map.
 * 4.If the value got from the user is present in the Map it says Value Exist in the Console, else Does not Exists 
 *   is printed in the Console.
 *   
 * Pseudo Code
 * 
 * class ValuesMap {
 *	
 *	public static void main(String[] args) {
 *		
 *		Scanner scan =new Scanner(System.in);
 *		// New HashMap
 *		Map<Integer,String> newMap = new HashMap<Integer,String>();
 *		//Adding Values to the Map
 *		newMap.put(1, "Chart");
 *		newMap.put(2, "Pencil");
 *		newMap.put(3, "Box");
 *		newMap.put(4, "Pen");
 *		newMap.put(5, "Stapler");
 *		newMap.put(6, "Pin");
 *		newMap.put(7, "Eraser");
 *		newMap.put(8, "Refill");
 *		newMap.put(9, "Duct Tape");
 *		
 *		System.out.println("Enter the Value to Find from the Map!");
 *		String inValue = scan.nextLine();
 *		//containsValue method to find the value.
 *		if(newMap.containsValue(inValue)) {
 *			System.out.println("Value Exists");
 *		} else {
 *			System.out.println("Value Does not Exists");
 *		}
 *		scan.close();
 *	}
 *
 *}
 * @author UKSD
 *
 */
public class ValuesMap {
	
	public static void main(String[] args) {
		
		Scanner scan =new Scanner(System.in);
		// New HashMap
		Map<Integer,String> newMap = new HashMap<Integer,String>();
		//Adding Values to the Map
		newMap.put(1, "Chart");
		newMap.put(2, "Pencil");
		newMap.put(3, "Box");
		newMap.put(4, "Pen");
		newMap.put(5, "Stapler");
		newMap.put(6, "Pin");
		newMap.put(7, "Eraser");
		newMap.put(8, "Refill");
		newMap.put(9, "Duct Tape");
		
		System.out.println("Enter the Value to Find from the Map!");
		String inValue = scan.nextLine();
		//containsValue method to find the value.
		if(newMap.containsValue(inValue)) {
			System.out.println("Value Exists");
		} else {
			System.out.println("Value Does not Exists");
		}
		scan.close();
	}

}
