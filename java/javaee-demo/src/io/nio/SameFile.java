package io.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Problem Statement
 * 1.Create two paths and test whether they represent same path
 * 
 * Requirement
 * 1.Create two paths and test whether they represent same path
 * 
 * Entity
 * 1.SameFile
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.Create a path for the File 1 and store it to path1
 * 2.Create a path for the FIle 2 and store it to path2
 * 3.Check whether the two path are same 
 * 		3.1)If yes then Print Same
 * 		3.2)If not print Not Same
 * 
 * Pseudo Code
 * 
 * class SameFile {
 *	
 *	public static void main(String[] args) {
 *		
 *		try {
 *			Path path1 = Paths.get("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
 *			
 *			Path path2 = Paths.get("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\javaAdvanced.txt");
 *			
 *			if(Files.isSameFile(path1, path2)) {
 *				System.out.println("Same");
 *			} else {
 *				System.out.println("Not Same");
 *			}
 *		} catch(IOException e) {
 *			e.printStackTrace();
 *		}
 *		
 *	}
 *
 *}
 * 
 * @author UKSD
 *
 */

public class SameFile {
	
	public static void main(String[] args) {
		
		try {
			Path path1 = Paths.get("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
			
			Path path2 = Paths.get("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\javaAdvanced.txt");
			
			if(Files.isSameFile(path1, path2)) {
				System.out.println("Same");
			} else {
				System.out.println("Not Same");
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		
	}

}
