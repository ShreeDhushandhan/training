package io.nio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Problem Statement
 * 1. Write some String content using Writer
 * 
 * Requirement
 * 1. Write some String content using Writer
 * 
 * Entity
 * 1.FileWriter
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a FileWriter called writer and specify the path of the file
 * 2.Get the input to insert to file from user and store it in messageIn
 * 3.write the messageIn to file using writer
 * 4.close the writer
 * 
 * Pseudo Code
 * class FileWriterExample {
 *	
 *	public static void main(String[] args) {
 *		
 *		Scanner scanner = new Scanner(System.in);
 *		
 *		try {
 *			
 *			FileWriter writer = new FileWriter("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
 *			
 *			System.out.println("Enter the message");
 *			String messageIn = scanner.nextLine();
 *			
 *			writer.write(messageIn);
 *			
 *			writer.close();
 *		} catch (IOException e) {
 *			e.printStackTrace();
 *		}
 *	}
 *
 *}
 *
 * @author UKSD
 *
 */

public class FileWriterExample {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		try {
			
			FileWriter writer = new FileWriter("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
			
			System.out.println("Enter the message");
			String messageIn = scanner.nextLine();
			
			writer.write(messageIn);
			
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
