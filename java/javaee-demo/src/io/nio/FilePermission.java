package io.nio;

import java.io.File;

/**
 * Problem Statement
 * 1.Get the permission allowed for a file
 * 
 * Requirement
 * 1.Get the permission allowed for a file
 * 
 * Entity
 * 1.FilePermission
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a File object called file and specify the path of the file.
 * 2.check whether the file exists or not
 * 3.if Exists Check whether the file is readable and writable
 * 		3.1)if not set the file to writable
 * 		3.2)if yes then set the file to no readable
 * 4.Check if the file is readable or not
 * 		4.1)If not then set the file to readable
 *		4.2)If yes then set the file to not readable
 * 5.Print the Current File Permission of the File
 * 
 * 
 * Pseudo Code
 * 
 * class FilePermission {
 *	
 *	public static void main(String[] args) {
 *		
 *		File file = new File("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
 *		
 *		if(file.exists()) {
 *			if(file.canWrite()) {
 *				file.setWritable(false);
 *			}
 *			
 *			if(file.canRead()) {
 *				file.setReadable(false);
 *			}
 *		}
 *		System.out.println(file.canRead());
 *		System.out.println(file.canWrite());
 *		System.out.println(file.canExecute());
 *	}
 *
 *}
 * @author UKSD
 *
 */

public class FilePermission {
	
	public static void main(String[] args) {
		
		File file = new File("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
		
		if(file.exists()) {
			if(file.canWrite()) {
				file.setWritable(false);
			}
			
			if(file.canRead()) {
				file.setReadable(false);
			}
		}
		System.out.println(file.canRead());
		System.out.println(file.canWrite());
		System.out.println(file.canExecute());
	}

}
