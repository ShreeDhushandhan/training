package io.nio;

import java.io.File;

/**
 * Problem statement
 * 1.Given a path, check if path is file or directory
 * 
 * Requirement
 * 1.Given a path, check if path is file or directory
 * 
 * Entity
 * 1.FileOrDirectory
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a File object called file and specify the path or the directory
 * 2.check whether the given path is directory
 * 		2.1)If yes then print "It is a Directory"
 * 		2.2)If No then print "It is a File Path"
 * 
 * Pseudo Code
 * 
 * class FileOrDirectory {
 *	
 *	public static void main(String[] args) {
 *		
 *		File file = new File("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6");
 *		
 *		if(file.isDirectory()) {
 *			System.out.println("It is a Directory");
 *		} else {
 *			System.out.println("It is a File");
 *		}
 *		
 *	} 
 *
 *}
 * @author UKSD
 *
 */

public class FileOrDirectory {
	
	public static void main(String[] args) {
		
		File file = new File("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6");
		
		if(file.isDirectory()) {
			System.out.println("It is a Directory");
		} else {
			System.out.println("It is a File");
		}
		
	}

}
