package io.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Problem Statement
 * 1.Read a file using java.nio.Files using Paths
 * 
 * Requirement
 * 1.Read a file using java.nio.Files using Paths
 * 
 * Entity
 * 1.NioFiles
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a path object called path and specify the path of the file 
 * 2.In Stream with line method pass the path of the file to the line method 
 * 3.Read each line and print
 *  
 * Pseudo Code
 * 
 * class NioFiles {
 *	
 *	public static void main(String[] args) {
 *		
 *		Path path = Paths.get("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
 *		
 *		try {
 *			Files.lines(path).forEach(System.out::println);
 *		} catch(IOException e) {
 *			e.printStackTrace();
 *		}
 *		
 *	} 
 *
 *}
 * @author UKSD
 *
 */

public class NioFiles {
	
	public static void main(String[] args) {
		
		Path path = Paths.get("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
		
		try {
			Files.lines(path).forEach(System.out::println);
		} catch(IOException e) {
			e.printStackTrace();
		}
		
	}

}
