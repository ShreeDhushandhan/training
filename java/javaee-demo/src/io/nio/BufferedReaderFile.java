package io.nio;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Problem Statement
 * 1.Read a any text file using BufferedReader and print the content of the file
 * 
 * Requirement
 * 1.Read a any text file using BufferedReader and print the content of the file
 * 
 * Entity
 * 1.BufferedReaderFile
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a FileReader called fileReader and specify the File Path.
 * 2.Create a BufferedReader called reader and pass the fileReader as a parameter to BuffferedReader
 * 3.Read the content from the file and store to fileContent.
 * 4.print the content in char while the content is not equal to -1.
 * 5.Read the next character from the file.
 * 6.Close the FileReader
 * 7.Close the BufferedReader
 * 
 * 
 * Pseudo Code
 * class BufferedReaderFile {
 *	
 *	public static void main(String[] args) {
 *		
 *		try {
 *			FileReader fileReader = new FileReader("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\javaAdvanced.txt");
 *			BufferedReader reader = new BufferedReader(fileReader);
 *			
 *			int fileContent = reader.read();
 *			
 *			while(fileContent != -1) {
 *				System.out.println((char) fileContent);
 *				fileContent = reader.read();
 *			}
 *			
 *			fileReader.close();
 *			reader.close();
 *		} catch(IOException e) {
 *			e.printStackTrace();
 *		}
 *	}
 *}
 * @author UKSD
 *
 */

public class BufferedReaderFile {
	
	public static void main(String[] args) {
		
		try {
			FileReader fileReader = new FileReader("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\javaAdvanced.txt");
			BufferedReader reader = new BufferedReader(fileReader);
			
			int fileContent = reader.read();
			
			while(fileContent != -1) {
				System.out.println((char) fileContent);
				fileContent = reader.read();
			}
			
			fileReader.close();
			reader.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
