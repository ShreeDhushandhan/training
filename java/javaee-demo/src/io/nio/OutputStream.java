package io.nio;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Scanner;

/**
 * Problem Statement
 * 1.Write some String content using OutputStream
 * 
 * Requirement
 * 1.Write some String content using OutputStream
 * 
 * Entity
 * 1.OutptStream
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a FileOutputStream called outputStream and specify the file path
 * 2.Store the content to be wrote in the file to a String called contentOut
 * 3.Create a byte array called fileByte and store the bytes of the String to the array.
 * 4.write the String to the outputStream
 * 5.Close the outputStream
 * 6.Create a FileInputStream called inputStream and specify the path of the File
 * 7.Create a BufferedReader called reader and pass the fileIputStream as the parameter 
 * 8.Read the content of the file using BufferedReader and store to fileContent of int type.
 * 9.print the fileContent while the fileContent is not equal to -1.
 * 10.Read the content from the file and store to fileContent again
 * 11.Close the inputStream
 * 12.Close the bufferedReader. 
 * 
 * 
 * 
 * Pseudo Code
 * class OutputStream {
 *	
 *	public static void main(String[] args) {
 *		
 *		try {
 *			Scanner scanner = new Scanner(System.in);
 *			FileOutputStream writer = new FileOutputStream("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
 *			System.out.println("Enter the Message");
 *			String messageIn = scanner.nextLine();
 *			byte fileByte[] = messageIn.getBytes();
 *			
 *			writer.write(fileByte);
 *			
 *			writer.close();
 *			
 *			FileReader inputFile = new FileReader("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
 *			BufferedReader reader = new BufferedReader(inputFile);
 *			
 *			int fileContent = reader.read();
 *			
 *			while(fileContent != -1) {
 *				System.out.println((char)fileContent);
 *				fileContent = reader.read();
 *			}
 *			
 *			inputFile.close();
 *			reader.close();
 *			
 *		} catch(IOException e) {
 *			e.printStackTrace();
 *		}
 *		
 *	}
 *
 *}
 * @author UKSD
 *
 */

public class OutputStream {
	
	public static void main(String[] args) {
		
		try {
			Scanner scanner = new Scanner(System.in);
			FileOutputStream writer = new FileOutputStream("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
			System.out.println("Enter the Message");
			String messageIn = scanner.nextLine();
			byte fileByte[] = messageIn.getBytes();
			
			writer.write(fileByte);
			
			writer.close();
			
			FileReader inputFile = new FileReader("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
			BufferedReader reader = new BufferedReader(inputFile);
			
			int fileContent = reader.read();
			
			while(fileContent != -1) {
				System.out.println((char)fileContent);
				fileContent = reader.read();
			}
			
			inputFile.close();
			reader.close();
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		
	}

}
