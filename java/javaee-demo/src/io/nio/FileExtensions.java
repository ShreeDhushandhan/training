package io.nio;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

/**
 * Problem Statement
 * 1.Get the file names of all file with specific extension in a directory
 * 
 * Requirement
 * 1.Get the file names of all file with specific extension in a directory
 * 
 * Entity
 * 1.FileExtensions
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a File object called directory 
 * 2.Store the File Extension to extensions
 * 3.Store all the Files of the Extension to a list called fileList
 * 4.for Each file in the list print the file with canonical Path 
 * 
 * 
 * Pseudo Code
 * 
 * class FileExtensions { 
 *	
 *	public static void main(String[] args) {
 *		
 *		File directory = new File("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6");
 *		
 *		String extensions = "txt";
 *		
 *		List<File> fileList = FileUtils.listFiles(directory, extensions, true);
 *		
 *		for(File file : fileList) {
 *			System.out.println(file.getCanonicalPath());
 *		}
 *		
 *	}
 *}
 * 
 * @author UKSD
 *
 */

public class FileExtensions {
	
	public static void main(String[] args) {
		
		File directory = new File("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6");
		
		String extensions = "txt";
		
		List<File> fileList = FileUtils.listFiles(directory, extensions, true);
		
		for(File file : fileList) {
			System.out.println(file.getCanonicalPath());
		}
		
	}
}
