package io.nio;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Problem Statement
 * 1.Reading a file using InputStream
 * 2.Reading a file using Reader
 * 
 * Requirement
 * 1.Reading a file using InputStream
 * 2.Reading a file using Reader
 * 
 * Entity
 * 1.FileReading
 * 
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a inputStream called fileInputStream 
 * 2.Read the content in the file and store to content
 * 3.Check whether the content has next item or not
 * 		3.1)If next content is present then print the content
 * 4.Close the inputStream 
 * 
 * 5.Create a fileReader called fileReader
 * 6.Read the content from the file and store to fileContent
 * 7.Check whether the content is not equal to -1.
 * 		7.1)If not then print the fileContent as char
 * 8.Read the next Character and store it in fileContent.
 * 
 * Pseudo Code
 * class FileReading {
 *	
 *	public static void main(String[] arrgs) {
 *		
 *		try {
 *			FileInputStream fileInputStream = new FileInputStream("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\javaAdvanced.txt");
 *			int content = fileInputStream.read();
 *			
 *			while(content != -1) {
 *				System.out.println((char) content);
 *				content = fileInputStream.read();
 * 			}
 *			fileInputStream.close();
 *			
 *			Reader fileReader = new FileReader("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\javaAdvanced.txt");
 *			int fileContent = fileReader.read();
 *			
 *			while(fileContent != -1) {
 *				System.out.println((char) fileContent);
 *				fileContent = fileReader.read();
 *			}
 *			
 *			fileReader.close();
 *		} catch(IOException e) {
 *			e.printStackTrace();
 *		}
 *		
 *	} 
 *
 *}
 * @author UKSD
 *
 */

public class FileReading {
	
	public static void main(String[] arrgs) {
		
		try {
			FileInputStream fileInputStream = new FileInputStream("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\javaAdvanced.txt");
			int content = fileInputStream.read();
			
			while(content != -1) {
				System.out.println((char) content);
				content = fileInputStream.read();
			}
			fileInputStream.close();
			
			Reader fileReader = new FileReader("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\javaAdvanced.txt");
			int fileContent = fileReader.read();
			
			while(fileContent != -1) {
				System.out.println((char) fileContent);
				fileContent = fileReader.read();
			}
			
			fileReader.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
	}

}
