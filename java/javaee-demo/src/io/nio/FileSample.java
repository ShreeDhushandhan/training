package io.nio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Problem Statement
 * 1.Read a file using java.io.File
 * 
 * Requirement
 * 1.Read a file using java.io.File
 * 
 * Entity
 * 1.File
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a file object called file and specify the file path
 * 2.Create a buffered reader called reader and pass the file as the parameter with FileReader
 * 3.read the content of the file and store in fileContent of String type.
 * 4.print the content from the fileContent while the fileContent is not equal to null
 * 5.Read the content again from the file and store to fileContent
 * 6.Close the BufferedReader.
 * 
 * 
 * Pseudo Code
 * class FileSample {
 *	
 *	public static void main(String[] args) {
 *		
 *		try {
 *
 *			File file = new File("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
 *			
 *			BufferedReader reader = new BufferedReader(new FileReader(file));
 *			
 *			String fileContent = reader.readLine();
 *			
 *			while(fileContent != null) {
 *				System.out.println(fileContent);
 *				fileContent = reader.readLine();
 *			}
 *			reader.close();
 *		} catch(IOException e) {
 *			e.printStackTrace();
 *		}
 *	} 
 *}
 * @author UKSD
 *
 */

public class FileSample {
	
	public static void main(String[] args) {
		
		try {

			File file = new File("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6\\outputFile.txt");
			
			BufferedReader reader = new BufferedReader(new FileReader(file));
			
			String fileContent = reader.readLine();
			
			while(fileContent != null) {
				System.out.println(fileContent);
				fileContent = reader.readLine();
			}
			reader.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
