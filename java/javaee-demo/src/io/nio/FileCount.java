package io.nio;

import java.io.File;

/**
 * Problem Statement
 * 1.Number of files in a directory and number of directories in a directory
 * 
 * Requirement
 * 1.Number of files in a directory and number of directories in a directory
 * 
 * Entity
 * 1.FileCount
 * 
 * Method Signature
 * 1.public static int fileCounter(File file) 
 * 2.public static int dirCounter(File file) 
 * 
 * 
 * Jobs to be Done
 * 1.Create a File object called directory and pass the path as the parameter.
 * 2.Create a method called fileCounter 
 * 		2.1)create a int fileCount and initialize to 0
 * 		2.2)Check for each file in the path 
 * 		2.3)if the file is a File then the fileCount is increased by 1
 * 		2.4)return the fileCount
 * 3.Create a method called dirCounter
 * 		3.1)create a int dirCount and initialize as 
 * 		3.2)Check for each file in the path
 * 		3.3)if the file in the path is directory then dirCount is increased by 1
 * 		3.4)return the dirCount
 * 4.Pass the directory to the fileCounter and print the result
 * 5.Pass the directory to the dirCounter and print the result 
 * 
 * Pseudo Code
 * class FileCount {
 *	
 *	public static int fileCounter(File file) {
 *		int fileCount = 0;
 *		for(File filer : file.listFiles()) {
 *			if(filer.isFile()) {
 *				fileCount ++;
 *			} 
 *		}
 *		return fileCount;
 *	}
 *	
 *	public static int dirCounter(File file) {
 *		int dirCount = 0;
 *		for(File filer : file.listFiles()) {
 *			if(filer.isDirectory()) {
 *				dirCount ++;
 *			}
 *		}
 *		return dirCount;
 *	}
 *	
 *	public static void main(String[] args) {
 *		
 *		File directory = new File("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6");
 *		
 *		System.out.println("Files : "+fileCounter(directory));
 *		
 *		System.out.println("Directory : "+dirCounter(directory));
 *		
 *	}
 *
 *}
 * @author UKSD
 *
 */

public class FileCount {
	
	public static int fileCounter(File file) {
		int fileCount = 0;
		for(File filer : file.listFiles()) {
			if(filer.isFile()) {
				fileCount ++;
			} 
		}
		return fileCount;
	}
	
	public static int dirCounter(File file) {
		int dirCount = 0;
		for(File filer : file.listFiles()) {
			if(filer.isDirectory()) {
				dirCount ++;
			}
		}
		return dirCount;
	}
	
	public static void main(String[] args) {
		
		File directory = new File("C:\\1Dev\\Project Usage\\Java\\JavaCore-Demo Excercises\\oct 6");
		
		System.out.println("Files : "+fileCounter(directory));
		
		System.out.println("Directory : "+dirCounter(directory));
		
	}

}
