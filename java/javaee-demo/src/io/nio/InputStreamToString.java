package io.nio;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Problem Statement
 * 1.InputStream to String and vice versa
 * 
 * Requirement
 * 1.InputStream to String and vice versa
 * 
 * Entity
 * 1.InputStreamToString
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create inputStream called stream and pass the String value
 * 2.Create a StringBuilder called builder
 * 3.create a new String called content and read all the content of the inputStream to the content
 * 4.Create a BufferedReader called reader and pass the stream as the parameter 
 * 5.Do append all the characters of the content to the builder.
 * 6.Close the reader.  
 * 
 * Pseudo Code
 * class InputStreamToString {
 *	
 *	public static void main(String[] args) {
 *		
 *		InputStream stream = new ByteArrayInputStream("Shree Dhushandhan".getBytes());
 *		
 *		StringBuilder builder = new StringBuilder();
 *		
 *		try { 
 *
 *			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
 *			
 *			String content = reader.readLine();
 *			
 *			while(content != null) {
 *				builder.append(content);
 *			}
 *			
 *			reader.close();
 *			System.out.println(builder);
 *			
 *		} catch(IOException e) {
 *			e.printStackTrace();
 *		}
 *	} 
 *
 *}
 * @author UKSD
 *
 */

public class InputStreamToString {
	
	public static void main(String[] args) {
		
		InputStream stream = new ByteArrayInputStream("Shree Dhushandhan".getBytes());
		
		StringBuilder builder = new StringBuilder();
		
		try {

			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			
			String content = reader.readLine();
			
			while(content != null) {
				builder.append(content);
			}
			
			reader.close();
			
			System.out.println(builder);
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		
	}

}
