package exception.handling;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Problem Statement
 * 1.Demonstrate the catching multiple exception with examples.
 * 
 * Requirement
 * 1.Demonstrate the catching multiple exception with examples.
 * 
 * Entity
 * 1.MultipleExceptionDemo Class
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a try Block that catches more than one Exception.
 * 2.Try Block catches both IOException and ArrayIndexOutofBoundsException
 * 3.Create a FileOuputStream and a integer array containing some values.
 * 4.For Each Loop is created and iterated in the integer array.
 * 5.If the value more than the existing values are called then the ArrayIndexOutOfBoundsException
 * 6.If the FileOutputStream gives any Error like the File does not exist then the IOException Arises.
 * 
 * Pseudo Code
 * class MultilpeExceptionDemo {
 *	
 *	public static void main(String[] args) {
 *		
 *		try {
 *			OutputStream input = new FileOutputStream("C://Users/UKSD/Desktop/sample.txt");
 *			int[] Numbers = {2,5,6,1,7,0};
 *			
 *			for(int c:Numbers) {
 *				System.out.println(c);
 *			}
 *			
 *		} 
 *		catch(IOException e) {
 *			e.printStackTrace();
 *		}  
 *		catch (ArrayIndexOutOfBoundsException f) {
 *			f.printStackTrace();
 *		}
 *	} 
 *
 *}
 * 
 * @author UKSD
 *
 */

public class MultilpeExceptionDemo {
	
	public static void main(String[] args) {
		
		try {
			OutputStream input = new FileOutputStream("C://Users/UKSD/Desktop/sample.txt");
			int[] Numbers = {2,5,6,1,7,0};
			
			for(int c:Numbers) {
				System.out.println(c);
			}
			
		} 
		catch(IOException e) {
			e.printStackTrace();
		} 
		catch (ArrayIndexOutOfBoundsException f) {
			f.printStackTrace();
		}
	}

}
