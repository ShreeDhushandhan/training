package exception.throww;

/**
 * Problem Statement
 * 1.Difference between throws and throw , Explain with simple example.
 * 
 * Requirement
 * 1.Difference between throws and throw , Explain with simple example.
 * 
 * Entity
 * 1.DemoException Class
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.throw Example is given at First.
 * 2.Initialize a int variable age and assign with 18;
 * 3.Using if condition it is checked that the age greater than 18 or not if it is not 18 then the Not Eligible is 
 *   thorwed by using Arithmetic Exception says Not Eligible, else it prints Eligible in the Console.
 * 4.created Another method called adder  to Confirmly throw ArithmeticException as throws, so by using that as a object in the 
 *   main function is throws the Arithmetic Exception while using the adder method..
 *   
 *  Pseudo Code
 *  
 *  class DemoException {
 *	
 *	public static void main(String[] args) {
 *		
 *		//throw Example
 *		int age = 18;
 *		if(age<18) {
 *			throw new ArithmeticException("Not Eligible");
 *		} else {
 *			System.out.println("Eligible");
 *		}
 *		
 *		//throws Example
 *		
 *		DemoException demo = new DemoException();
 *		int a = 1;
 *		int b = 0;
 *		demo.adder(a,b);
 *	}
 *	
 *	private int adder(int a, int b) throws ArithmeticException {
 *		int s = a/b;
 *		return s;
 *	}
 *
 *}
 *
 * @author UKSD
 *
 */

public class DemoException {
	
	public static void main(String[] args) {
		
		//throw Example
		int age = 18;
		if(age<18) {
			throw new ArithmeticException("Not Eligible");
		} else {
			System.out.println("Eligible");
		}
		
		//throws Example
		
		DemoException demo = new DemoException();
		int a = 1;
		int b = 0;
		demo.adder(a,b);
	}
	
	private int adder(int a, int b) throws ArithmeticException {
		int s = a/b;
		return s;
	}

}
