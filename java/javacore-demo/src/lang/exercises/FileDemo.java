/*
    Problem Statement
    1.Print the absolute path of the .class file of the current class
    2.Open the source file of the running class in Notepad++ - do NOT hardcode/specify the source file in the code; 
      find the source file using APIs
    3.demonstrate object equality using 
      Object.equals() vs ==, using String objects
      
    Entity
    1.FileDemo Class
    
    Work Done
    1.Delcaring a File object as file and specificing the File name as Parameter.
    2.Getting the Absolute Path of a File using getAbsolutePath Function and print it.
    
    1.Getting the Current Working File without HardCoding with System Package.
    2.By using getProperty method and passing the parameter as "user.dir" to get the directory of the working File.
    
    1.Comparing the String Object with equals() Function and == Operator.
    2.Declare a String Object as fHalf and initialize as "Shree"
    3.Declare another String Object as sHalf and initialize as "Dhushandhan"
    4.Declare anothre String object as tHalf and initialize as "Shree"
    5.Comparing the String Objects fHalf and sHalf with equals method and printing True if its True.
    6.Comapring the String Objects fHalf annd tHalf with == operator and printing Flase if its True.
    
*/

package lang.exercises;

import java.io.*;

public class FileDemo {
	
    public static void main(String[] args) {
        //Getting the Absolute Path of a File//
        File file = new File("FileDemo.class");
        System.out.println(file.getAbsolutePath());
        
        //Getting the Path of a File without HardCoding//
        System.out.println(System.getProperty("user.dir"));
        
        //String Object Comparision//
        String fHalf = "Shree";
        String sHalf = "Dhushandhan";
        String tHalf = "Shree";
        
        if(fHalf.equals(sHalf)) {
            System.out.println("True");
        } else if(fHalf == tHalf) {
            System.out.println("False");
        }
    }
}