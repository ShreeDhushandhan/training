/*
    Problem Statement
    1.How many references to those objects exist after the code executes?
    2.Is either object eligible for garbage collection?
    
    1.What is the initial capacity of the following string builder?
    
    1.What is the value displayed by the expression hannah.length()?
    2.What is the value returned by the method call hannah.charAt(12)?
    3.Write an expression that refers to the letter b in the string referred to by hannah.
    
    1.How long is the string returned by the following expression? What is the string?
    
    Entity
    1.StringStuDemo Class
    
    Work Done
    1.Created the student array and added a item to the 
      student string array
    2.made the string object null so that the pointer goes to garbage collection
    
    
*/
/*
    Answers
    1.There are no references created after the code executes
    2.the studentName object is eligible for the garbage collection
      since the object is pointed as NULL.
      
    1.Since the Initial capacity of a empty StrinBuilder class object is 16,
      but when a String is provided as a parameter,
      it takes the number of characters as the capacity of the string builder object.
      
    1.32
    2.e
    3.String b = hannah;
    
    1.String : car
    2.String Length : 3
*/

package strings.exercises;

import java.util.*;

public class StringStuDemo {
	
    public static void main(String[] args) {
        
        //String Reference and Garbage Collection Exercise//
        String[] students = new String[10];
        System.out.println(students.length);
        
        String studentName = "Peter Parker";
        
        students[0] = studentName;
        studentName = null;
        
        System.out.println(studentName);
        //String Builder Exercise//
        System.out.println("String Builder Class");
        StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");
        System.out.println(sb.length());//OUTPUT : 26//
        
        //String Exercise//
        String hannah = "Did Hannah see bees? Hannah did.";
        System.out.println(hannah.length());//OUTPUT : 32 //
        System.out.println(hannah.charAt(12));//OUTPUT : e //
        String b = hannah;
        System.out.println(b);
        System.out.println(hannah);
        
        //String Expression Exercise//
        String esp = "Was it a car or a cat I saw?";
        String hal = esp.substring(9,12);
        System.out.println("String : "+hal);//String : car//
        System.out.println("String Length : "+hal.length());//String Length : 3//
    }
}