/*
    Problem Statement
    1.what is the value of result after each numbered line executes?
    
    1.Show two ways to concatenate the following two strings together to get the string "Hi, mom."
    
    Entity
    1.ComputeResult Class
    
    1. 2 Strings
    
    
    Work Done
    1.Used StringBuilder class to build a mutable String.
    2.Getting the index of the character at a.
    3.Using the setCharAt Function of StringBuilder to replace the character of the String
      with the given Character at the given index position.
    4.Using the insert Function of the StringBuilder to insert a character or a given String
      at the index position specified in the String..
    5.Using Append Function of StringBuilder to append the given character or String at the 
      end of the existing String..
      
    1.By executing in Two Ways
      1.concat Function of String Class
      2.+ COncatenation Operator.
*/

package strings.exercises;

public class ComputeResult {
	
    public static void main(String[] args) {
        String original = "software";
        StringBuilder result = new StringBuilder("hi");
        int index = original.indexOf('a');
        
        /*1*/ result.setCharAt(0, original.charAt(0));
        //System.out.println(result); //OUTPUT : si//
        
        /*2*/ result.setCharAt(1, original.charAt(original.length()-1));
        //System.out.println(result); //OUTPUT : se//
        
        /*3*/ result.insert(1, original.charAt(4));
        //System.out.println(result); //OUTPUT : swe//
        
        /*4*/ result.append(original.substring(1,4));
        //System.out.println(result); //OUTPUT : sweoft//
        
        /*5*/ result.insert(3, (original.substring(index, index+2) + " "));
        //System.out.println(result); //OUTPUT : swear oft//
        
        System.out.println(result); //OUTPUT : swear oft//
        
        //String Concatenation Exercise//
        
        String hi = "Hi, ";
        String mom = "mom.";
        //Two Ways to Concatenate the Strings//
        String res = hi.concat(mom);//Using concat Function of String Class//
        System.out.println(res);
        String res1 = hi + mom; //Using + (String Concatenation) operator //
        System.out.println(res1);
    }
}