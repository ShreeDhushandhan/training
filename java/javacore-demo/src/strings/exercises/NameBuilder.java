/*
    Problem Statement
    1.Write a program that computes your initials 
      from your full name and displays them.
    
    Entity
    1.NameBuilder Class
    
    Work Done
    1.NameBuilder Class Created.
    2.Got the name from the User.
    3.Using substring function of java.util slicing the 
      given input from 0th index to the 3rd index to get the Initial 
      of the Name Given..
*/

package strings.exercises;

import java.util.Scanner;

public class NameBuilder {
	
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your Full Name");
        String name = scan.nextLine();
        //INTIAL SEPERTOR//
        System.out.println("Initial :");
        System.out.println(name.substring(0,3));
        //NAME SEPERATOR//
        System.out.println("Name :");
        System.out.println(name.substring(4,name.length()));
        
    }
}