/*
    Problem Statement
    1.sort and print following String[] alphabetically ignoring case. 
    Also convert and print even indexed Strings into uppercase
    
    Entity
    1.StringDemo Class
    
    Work Done
    1.Creating a Arraylist and Adding up the wanted items into the list
    2.Creating a Iterator tool for iterating over the list
    3.Sorting the Arraylist with Collections Package SORT Function
    4.Using replaceAll function of java.util Package to convert the Characters to CAPITAL
    5.Again using the Iterator tool to iterate over the list and displaying it..
*/

package strings.exercises;

import java.util.ArrayList;
import java.util.*;

public class StringDemo {
	
    public static void main (String[] args) {
        ArrayList<String> city = new ArrayList<String>();
        ArrayList<String> upperlist = new ArrayList<String>();
        city.add("madurai");
        city.add("Thanjavur");
        city.add("TRICHY");
        city.add("Karur");
        city.add("Erode");
        city.add("trichy");
        city.add("Salem");
        Iterator iter = city.iterator();
        System.out.println("List Before Sorting");
        while(iter.hasNext()) {
            System.out.println(iter.next());
        }
        Collections.sort(city);
        
        System.out.println("List After Sorting");
        
        city.replaceAll(String::toUpperCase);
        
        Iterator it = city.iterator();
        while(it.hasNext()) {
            System.out.println(it.next());
        }
    }
}