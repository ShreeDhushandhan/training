/*
    Problem Statement
    Debug the Code with Compound Assignments
    
    Entites
    1.ArithmeticDemo Class
    
    Work Done
    1.Debugging the Code with Compound Assignments
    to Shorten the Syntax for Arithmetic and Bitwise Operator
    2.Since the Compound Assignments does the operation and 
    assignment at the same time..
*/

package operator.classes;

public class ArithmeticDemo {
	
    public static void main(String[] args) {
        int result = 1+2; // Output: 3 //
        System.out.println(result);
        
        result -= 1; // Output: 2 //
        System.out.println(result);
        
        result *= 2; // Output: 4//
        System.out.println(result);
        
        result /= 2; // Output: 2//
        System.out.println(result);

        result += 8; // Output: 10//
        System.out.println(result);
        
        result %= 7; // Output: 3//
        System.out.println(result);
    }
}