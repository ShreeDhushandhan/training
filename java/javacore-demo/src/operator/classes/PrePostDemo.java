/* 
    Problem Statement
    Explain why the value 6 is printed twice in a row
    
    Entites
    1.PrePostDemo Class
    
    Work Done
    1.Since the value of i in 21 line is 5 
      the pre increment operator increase the value to 6 and then sees the value of in i
    2.Since the value of i in 22 line is first uses as 5
      and then the value is increased to 6.
*/

package operator.classes;

public class PrePostDemo {
	
    public static void main(String[] args) {
        int i = 3;
        i++;
        System.out.println(i);    // "4"
        ++i;
        System.out.println(i);    // "5"
        
        System.out.println(++i);  // "6"
        //Pre Increment Operator increases the value first and then assigns //
        
        System.out.println(i++);  // "6"
        //Post Increment Operator assigns the value first and then increase second//
        
        System.out.println(i);    // "7"
    }
}