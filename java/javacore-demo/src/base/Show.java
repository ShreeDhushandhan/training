package base;

abstract class statusChecker {
	
    public String statusOn = "Available";
    public String statusOff = "Not Available";
    public abstract void status ();
}

public class Show extends statusChecker {
	
    public String name;
    public String time;
    public String date;
    public int ticketPrice;
    public String seatNumber;
    public String seatClass;
    public String seatAvailability;
    
    public void status() {
        System.out.println("Status Available for All Movies");
    }
    
    //UnParameterized Constructor//
    public Show() {
        
    }
    //Parameterized Constructor//
    public Show(String name,String time,String date,int ticketPrice,String seatNumber,String seatClass,
    String seatAvailability) {
        this.name = name;
        this.time = time;
        this.date = date;
        this.ticketPrice = ticketPrice;
        this.seatNumber = seatNumber;
        this.seatClass = seatClass;
        this.seatAvailability = seatAvailability;
    }
    //Methods//
    public void book(String name,String date,String time,String seatNumber) {
        if (seatAvailability == "True") {
            System.out.println("Status :"+statusOn);
            System.out.println("Movie :"+name);
            System.out.println("Date :" +date);
            System.out.println("Time :" +time);
            System.out.println("Seat Number :"+seatNumber);
            System.out.println("Ticket Price" + ticketPrice);
        } else {
            System.out.println("Status :"+statusOff);
            System.out.println("Movie :"+name);
            System.out.println("Date :" +date);
            System.out.println("Time :" +time);
            System.out.println("Seat Number :"+seatNumber);
        }
    }
    
    public void cancel(String name,String date,String time) {
            System.out.println("Movie :"+name);
            System.out.println("Date :" +date);
            System.out.println("Time :" +time);
            System.out.println("Status : Cancelled");
    }
    
    //Main Class//
    public static void main(String [] args) {
    	
        System.out.println("File Execution Success");
    }

}