package abstracts.exercise;

/**
 * Problem Statement
 *   1.Demonstrate abstract classes using Shape class.
 *   2.Shape class should have methods to calculate area and perimeter
 *   3.Define two more class Circle and Square by extending Shape class
 *     and implement the calculation for each class respectively
 *     
 *   Entity
 *   1.Shape Class
 *   2.Circle Class
 *   3.Rectangle Class
 *   
 *   Work Done
 *   1.Created Abstract Shape class with area Calculaing method as areaCalc
 *     and Perimeter Calculaing method as periCalc for both circle and rectangle
 *   2.Creating Circle class and implementing the area and perimeter calculating methods
 *     since we are extending abstract class we need to implement all the methods.
 *   3.Creaing Rectangle class and implementing the are and perimeter calculating
 *     methods.
 *   4.Creating Instance for the Circle and Rectangle class and calling the area and 
 *     perimeter methods and passing the parameter to calculate..
 * @author UKSD
 *
 */

abstract class Shape {
    abstract double areaCalc(int radius);
    abstract double periCalc(int radius);
    abstract int areaCalc(int width , int height);
    abstract int periCalc(int width , int height);
}

class Circle extends Shape {
    public double areaCalc(int radius) {
        return 3.14*radius*radius;
    }
    
    public double periCalc(int radius ) {
        return 2*3.14*radius;
    }
    int periCalc(int width , int height) {
        return 0;
    }
    int areaCalc(int width , int height) {
        return 0;
    }
}

class Rectangle extends Shape {
    int width;
    int height;
    public int areaCalc(int width,int height) {
        return width*height;
    }
    
    public int periCalc(int width,int height) {
        int x = width+height;
        return 2*x;
    }
    double areaCalc(int radius) {
        return 0;
    }
    double periCalc(int radius) {
        return 0;
    }
}

public class AbstractClass {
    public static void main(String[] args) {
        Shape circle = new Circle();
        int radius = 12;
        System.out.println("Area of Circle :"+circle.areaCalc(radius));
        System.out.println("Perimeter of Circle :"+circle.periCalc(radius));
        
        Rectangle rect = new Rectangle();
        int width = 4;
        int height = 9;
        System.out.println("Area of Rectangle :"+rect.areaCalc(width,height));
        System.out.println("Perimeter of Rectangle :"+rect.periCalc(width,height));
        
    }
}