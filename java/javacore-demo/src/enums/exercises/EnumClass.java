/*
    Problem Statement
    Compare Enum Values with 
    1.Equal Function
    2. == Operator
    
    Entity
    1.EnumClass
    
    Work Done
    1.Creating Enum Class with Default Values
    2.Get User Value
    3.Comparing the ENUM Values with == Operator
    4.Comparing the ENUM Values with equals function
    
    
*/

package enums.exercises;

import java.util.Scanner;

public class EnumClass {
	
    public enum Days {MON,TUE,WED,THU,FRI,SAT,SUN}
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Select the Day to get the Past Weather Forcast");
        System.out.println("MON\n"
                           +"TUE\n"
                           +"WED\n"
                           +"THU\n"
                           +"FRI\n"
                           +"SAT\n"
                           +"SUN\n");
        String getday = scan.nextLine();//Get User Value//
        Days days = Days.valueOf(getday.toUpperCase());
        if (days.equals(Days.MON)) {
            System.out.println("Cloudy");
        }
        else if (days.equals(Days.TUE)) {
            System.out.println("Weather :"+"Partial Cloudy");
        }
        else if (days.equals(Days.WED)) {
            System.out.println("Weather :"+"Humid");
        }
        else if (days.equals(Days.THU)) {
            System.out.println("Weather :"+"Hot");
        }
        else if (days == Days.FRI) {
            System.out.println("Weather :"+"Humid");
        }
        else if (days == Days.SAT) {
            System.out.println("Weather :"+"Sunny");
        }
        else if (days == Days.SUN) {
            System.out.println("Weather :"+"Partial Cloudy");
        }
        else {
            System.out.println("Enter a Valid One!!");
        }
        
    }
}