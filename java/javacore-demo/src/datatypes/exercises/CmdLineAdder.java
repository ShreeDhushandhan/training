/*
    Problem Statement
    1.Create a program that reads an unspecified number of integer arguments 
      from the command line and adds them together
    
    Entity
    1.CmdLineAdder Class
    
    Work Done
    1.Create a CmdLineAdder Class and initialize two values as sum and val and declare as 0.
    2.Check the Command Line arguments is greater than 4 with if Statment,
    3.If the Valus passed are greater than 4 then using the for loop convert the String arguments to INteger using 
      parseInt of Integer Class.
    4.Then add the argument values into sum and print the sum Value.
    5.If the Arguments passed are less than 4, Print the Error Message as Enter 4 Values.
*/
package datatypes.exercises;

public class CmdLineAdder {
	
    public static void main(String[] args) {
        int sum = 0;
        int val = 0;
        if(args.length > 1) {
            for (int i = 0;i<args.length;i++) {
               val = Integer.parseInt(args[i]);
               sum = sum +val;
            }
            System.out.println(sum);
        } else {
            System.out.println("Enter More than 4 Values");
        }
    }
}