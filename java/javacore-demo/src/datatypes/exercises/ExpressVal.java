/*
    Problem Statement
    1.What is the value of the following expression, and why?
    
    Entity
    1.ExpressVal Class
    
    Work Done
    1.Comparing the Values fo Integer and Long type of Wrapper Class
    2.Even the values of the Integer 1 look similar to Long 1,
      the Reference Value and the Memory Size of the Integer and Long is 
      far differ from both..
    3.Here Comparing the Integer and long with if Statement and checking the 
      value. It prints Equal if the Integer 1 is equals to Long 1, else it prints Not Equals.
*/

package datatypes.exercises;

public class ExpressVal {
	
    public static void main(String[] args) {
        if(Integer.valueOf(1).equals(Long.valueOf(1))) {
            System.out.println("Values are Equal");
        } else {
            System.out.println("Values are Not Equal");
        }
        System.out.println(Integer.valueOf(1));
        System.out.println(Long.valueOf(1));
    }
}