/*
    Problem Statement
    1.What methods would a class that implements the java.lang.CharSequence 
      interface have to implement?
    2.Debug the Syntax.
    3.Implement CharSequence Class and print the String
      backwards and use all four methods.
      
    Entity
    1.InterfaceDemo Class
    2.CharSequence Interface 
    
    Work Done
    1.Created a Class and implemented CharSequence interface
                                                  
    2.Debug the Syntax.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }
    A: The interface methods are implemented. Interface 
      should have unimplemented methods..
      
    3.Validate the Interface
    public interface Marker {}
    A:The Interface is Valid since the methods are not necessary.
    
*/

package interfaces.exercises;

import java.lang.CharSequence;

public class InterfaceDemo implements CharSequence{
	
    public static String name;
    
    //Length Method Overridded//
    public int length() {
        return name.length();
    }
    //toString Method Overridded//
    public String toString() {
        return name;
    }
    //charAt Method Overridded//
    public char charAt(int index) {
        return name.charAt(index);
    }
    //subSequence Method Overridded//
    public CharSequence subSequence(int start,int end) {
        String str = name.substring(start,end);
        StringBuffer buffer = new StringBuffer(str);
        buffer.reverse();
        str = buffer.toString();
        return str;
    }
    
    public static void main(String[] args) {
        InterfaceDemo demo = new InterfaceDemo();
        demo.name = "Dhushandhan";
        System.out.println(demo.name.length()); // Length of String//
        System.out.println(demo.subSequence(0,name.length())); // Reverse of a String//
        System.out.println(demo.charAt(5)); // charAt in a String //
        System.out.println(demo.toString()); // to String Method //
        
    }
}