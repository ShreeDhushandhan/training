package interfaces;

public interface MovieWatchTester {
	
    public void watch (String movieName,String seatNumber);
    public void review(String movieName);
}