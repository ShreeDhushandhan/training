/*
    Problem Statement
    1.What output do you think the code will produce if aNumber is 3?
    2.Using only spaces and line breaks, reformat the code snippet
      to make the control flow easier to understand.
    3.Use braces, { and }, to further clarify the code.
    
    Entity
    1.User Input aNumber;
    
    Work Done
    1.Control Flow is made easy to Understand
    2.Used Braces and {,} and clarified the Code.
    
*/

package control.flow.exercises;

import java.util.Scanner;

public class NumberFlow {
    
    public static void main(String[] args){
        System.out.println("Making the aNumber = 3");
        // if aNumber = 3//
        // Output: Second String//
        int aNumber = 3;
        if(aNumber >=0) {
            if(aNumber == 0) {
                System.out.println("First String");
            }
            else {
                System.out.println("Second String");
            }
        }
        else {
            System.out.println("Third String");
        }
    }
}