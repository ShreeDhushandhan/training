/*
    Problem Statement
    1.Complete Fibonacci Series with 
        for loop
        while loop
        Recursion
    
    Entities
    1.Total range of Fibonacci
    2.initial values a and b
    
    Work Done
    Sum the last two values to produce a Fibonacci Series.
    Used For loop 
    Used While Loop
    Used Recursion
*/
package control.flow.exercises;

import java.util.Scanner;

public class Fibno {
    
    static void fiboCalc(int num) {
        int s = 0 , d = 1;
        for(int q = 1;q <= num; q++) {
            System.out.println(s);
            int sum2 = s + d;
            s = d;
            d = sum2;
        }
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        //Fibonacci Series using For Loop//
        
        System.out.println("Fibnocci Series with FOR LOOP");
        System.out.println("Enter the Range");
        int sum;
        int ran = scan.nextInt();
        int a = 0 ,b = 1;
        for(int i = 1;i <=ran; i++) {
            System.out.println(a);
            sum = a + b;
            a = b;
            b = sum;
        }
        
        // Fibonacci Series using While Loop //
        
        System.out.println("Fibnocci Series with WHILE LOOP");
        System.out.println("Enter the Range");
        int sum1,o = 1;
        int ran1 = scan.nextInt();
        int c = 0, d = 1;
        while(o<=ran1) {
            System.out.println(c);
            sum1 = c + d;
            c = d;
            d = sum1;
            o++;
        }
        
        // Fibonacci Series using Recursion //
        System.out.println("Fibnnocci Series using Recursion");
        System.out.println("Enter the Range");
        int ran2 = scan.nextInt();
        fiboCalc(ran2);
        
        scan.close();
    }
}