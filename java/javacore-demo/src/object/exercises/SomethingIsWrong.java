/*
    Problem Statement
    Debugging the Code..

    Entities
    SomethingIsWrong Class
    
    Work Done
    1.Since the public Class has a Rectangle Class incomplete
      object created so it will not create a space for the class 
      members declared in the program memory..
    2.Creating the Object for the Rectangle as myRect
    3.Creating a Rectangle class 
    4.Declaring two variables as
      height and width
      and public method area to return area of rectangle
    
    
*/

package object.exercises;

class Rectangle {
    int width;
    int height;
    
    public int area() {
        return height*width;
    }
}

public class SomethingIsWrong {
	
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
        //Output : myRect's area is 2000//
    }
}