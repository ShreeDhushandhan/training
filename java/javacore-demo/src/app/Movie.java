package app;

import java.util.ArrayList;
import java.util.Scanner;

import base.Show;
import interfaces.MovieWatchTester;

class Rating {
	
    private String RatingOn = "Rotten Tomatos : 89%";
    int star;
    private class TomatoRating {
        public void statusteller(int star) {
            if(star >3) {
                String RatingOff = "Rotten Tomatos : 75%";
                System.out.println(RatingOff);
            } else {
                System.out.println(RatingOn);
            }
        }
    }
    
    public void rate(String name,String genre) {
        TomatoRating tomato = new TomatoRating();
        tomato.statusteller(star);
    }
}

public class Movie extends Show implements MovieWatchTester {
	
    private String genre;
    private String theaterName;
    private long theaterContactNumber;
    private String theaterAddress;
    public int screenNumber;
    public static ArrayList<Integer> screen;
    
    
    //UnParameterized Constructor//
    public Movie() {
        
    }
    //Parameterized Constructor//
    public Movie(String genre, String theaterName, String theaterAddress, long theaterContactNumber,
    		int screenNumber) {
        this.genre = genre;
        this.theaterName = theaterName;
        this.theaterAddress = theaterAddress;
        this.theaterContactNumber = theaterContactNumber;
        this.screenNumber = screenNumber;
    }
    //Methods//
    public void screenAvailability(int screenNumber) {
        if (screen.contains(screenNumber)) {
                System.out.println("Screen Available");
            } else {
                System.out.println("Screen Not Available");

            }
    }
    
    public void review(String name, String genre) {
        if (name == "Dil Bechara") {
            System.out.println("Rating : 8.8");
        } else if(name == "Sadak 2") {
            System.out.println("Rating : 4.0");
        }
    }
    
    public void searchMovie() {
        
    }
    
    public void watch(String name,String seatNumber) {
        if(seatNumber != "NIL") {
            System.out.println(name+" "+"Watched");
        } else {
            System.out.println(name+" "+"Unwatched");
        }
    }
    
    public void review(String name) {
    
    }
    
    public static void main(String[] args) {
    	
        screen = new ArrayList<Integer>();
        screen.add(5);
        screen.add(2);
        screen.add(8);
        Scanner scan = new Scanner(System.in);
        
        Movie movie = new Movie("Romance","Fun Cinemas","Ops Coimbatore",908,5);
        Show show = new Show("Dil Bechara","7:30PM","17 August",199,"7A","First","True");
        
        Movie fmovie = new Movie();
        fmovie.genre = "Romance";
        fmovie.theaterName = "INOX";
        fmovie.theaterAddress = "Siva Colony,Coimbatore";
        fmovie.theaterContactNumber = 789;
        fmovie.screenNumber = 9;
        
        //Second Object
        Show shownew = new Show("Sadak 2","10:00PM","20 December",199,"NIL","NIL"," False");
        
        System.out.println("Choose the Movie..\n"
                           + "1.Dil Bechara\n" 
                           + "2. Sadak 2");
        String getMovie = scan.nextLine();
        if(getMovie.equals("Dil Bechara")) {
            show.book(movie.name,movie.date,movie.time,movie.seatNumber);
            movie.watch(movie.name,movie.seatNumber);
            Rating rater = new Rating();
            rater.star = 4;
            rater.rate(movie.name,movie.genre);
        } else if(getMovie.equals("Sadak 2")) {
            shownew.book(fmovie.name,fmovie.date,fmovie.time,fmovie.seatNumber);
            movie.watch(fmovie.name,fmovie.seatNumber);
            // Inner Class Object 2//
            Rating rater1 = new Rating();
            rater1.star = 1;
            rater1.rate(fmovie.name,fmovie.genre);
        } else {
            System.out.println("Movie Not Available");
        }
        scan.close();
   }
}