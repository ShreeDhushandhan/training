package com.kpr.training.jdbc.model;

import java.util.ArrayList;

/**
 * Problem Statement
 * 1.POJO Class for Person
 * 
 * Entity
 * 1.Person
 * 
 * Method Signature
 * 1.public public Person(String name, String email, Date birth_date);
 * 2.public long getId();
 * 3.public String getName();
 * 4.public String getEmail();
 * 5.public long getAddressId();
 * 6.public Date getBirthDate();
 * 7.public Date getCreatedDate()
 * 
 * Jobs to be Done
 * 1.Create a id field of long type
 * 2.Create a name field of String type
 * 3.Create a email field of String type
 * 4.Create a address_id of int type
 * 5.Create a birth_date of Date type
 * 6.Create a created_date of Date type.
 * 5.Create a public constructor take all the Address fields
 * 6.Create the getter and setter for the fields
 * 7.Override the toString method and append the concat String with String builder
 * 
 * 
 * Pseudo Code
 * class Person {
 *	
 *	private long id;
 *	private String firstName;
 *	private String lastName;
 *	private String email;
 *	private Date birthDate;
 *	private Date createdDate;
 *	private Address address;
 *	
 *	 
 *	public Person(String firstName, String lastName, String email, Date birthDate, Address address) {
 *		
 *		this.firstName = firstName;
 *		this.lastName = lastName;
 *		this.email = email;
 *		this.birthDate = birthDate;
 *		this.address = address;
 *	}
 *	
 *	public Person(String firstName, String lastName, String email, Date newdate) {
 *		
 *		this.firstName = firstName;
 *		this.lastName = lastName;
 *		this.email = email;
 *		this.birthDate = newdate;
 *	}
 *	
 *	public Person(long id, String firstName, String lastName, String email, Date birthDate) {
 *		
 *		this.id = id;
 *		this.firstName = firstName;
 *		this.lastName = lastName;
 *		this.email = email;
 *		this.birthDate = birthDate;
 *	}
 *	
 *	public Person(long id, String firstName, String lastName, String email, Date birthDate, Address address, Date createdDate) {
 *		
 *		this.id = id;
 *		this.firstName = firstName;
 *		this.lastName = lastName;
 *		this.email = email;
 *		this.birthDate = birthDate;
 *		this.address = address;
 *		this.createdDate = createdDate;
 *	}
 *	
 *	public Person(long id, String firstName, String lastName, String email, long addressId, Date birthDate, Date createdDate) {
 *		
 *		this.id = id;
 *		this.firstName = firstName;
 *		this.lastName = lastName;
 *		this.email = email;
 *		address.setId(addressId);
 *		this.createdDate = createdDate;
 *	}
 *	
 *	public Person(long id, String firstName, String lastName, String email, long addressId, Date birthDate, Date createdDate, Address address) {
 *		
 *		this.id = id;
 *		this.firstName = firstName;
 *		this.lastName = lastName;
 *		this.email = email;
 *		address.setId(addressId);
 *		this.birthDate = birthDate;
 *		this.createdDate = createdDate;
 *		this.address = address;
 * 	}
 *	
 *	public long getId() {
 *		return id;
 *	}
 *	
 *	public String getFirstName() {
 *		return firstName;
 *	}
 *	
 *	public String getLastName() {
 *		return lastName;
 *	}
 *	
 *	public String getEmail() {
 *		return email;
 *	}
 *	
 *	public long getAddressId() {
 *		return address.getId();
 *	}
 *	
 *	public Date getBirthDate() {
 *		return birthDate;
 *	}
 *	
 *	public Date getCreatedDate() {
 *		return createdDate;
 *	}
 *	
 *	public void setId(long id) {
 *		this.id = id;
 *	}
 *	
 *	public void setFirstName(String firstName) {
 *		this.firstName = firstName;
 *	}
 *	
 *	public void setLastName(String lastName) {
 *		this.lastName = lastName;
 *	}
 *	
 *	public void setEmail(String email) { //email
 *		this.email = email;
 *	}
 *	
 *	public void setAddressId(long id) { // id
 *		address.setId(id); 
 *	}
 *	
 *	public void setBirthDate(Date birthDate) {
 *		this.birthDate = birthDate;
 *	}
 *	
 *	public void setCreatedDate(Date createdDate) {
 *		this.createdDate = createdDate;
 *	}
 *	
 *	
 *	public Address getAddress() {
 *		return address;
 *	}
 *	
 *	public void setAddress(Address address) {
 *		this.address = address;
 *	} 
 *
 *	@Override
 *	public String toString() {
 *		
 *		return new StringBuilder("Person [id =")
 *						.append(id)
 *						.append(", firstName =")
 *						.append(firstName)
 *						.append(", lastName =")
 *						.append(lastName)
 *						.append(", email =")
 *						.append(email)
 *						.append(", addressId =")
 *						.append(address.getId())
 *						.append(", birthDate=")
 *						.append(birthDate)
 *						.append(", createdDate=")
 *						.append(createdDate)
 *						.append(", address=")
 *						.append(address)
 *						.append("]").toString();
 *	}
 *	
 *}
 */

import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.kpr.training.constants.Constants;
import com.kpr.training.constants.QueryStatement;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;
import com.kpr.training.service.ConnectionService;
import com.kpr.training.service.PersonService;
import com.kpr.training.validator.Validator;

import java.io.FileReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;

public class Person {
	
	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private Date birthDate;
	private Date createdDate;
	private Address address;
	
	
	public Person(String firstName, String lastName, String email, Date birthDate, Address address) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthDate = birthDate;
		this.address = address;
	}
	
	public Person(String firstName, String lastName, String email, Date newdate) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthDate = newdate;
	}
	
	public Person(long id, String firstName, String lastName, String email, Date birthDate) {
		
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthDate = birthDate;
	}
	
	public Person(long id, String firstName, String lastName, String email, Date birthDate, Address address, Date createdDate) {
		
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthDate = birthDate;
		this.address = address;
		this.createdDate = createdDate;
	}
	
	public Person(long id, String firstName, String lastName, String email, long addressId, Date birthDate, Date createdDate) {
		
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		address.setId(addressId);
		this.createdDate = createdDate;
	}
	
	public Person(long id, String firstName, String lastName, String email, long addressId, Date birthDate, Date createdDate, Address address) {
		
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		address.setId(addressId);
		this.birthDate = birthDate;
		this.createdDate = createdDate;
		this.address = address;
	}
	
	public long getId() {
		return id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public long getAddressId() {
		return address.getId();
	}
	
	public Date getBirthDate() {
		return birthDate;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setEmail(String email) { //email
		this.email = email;
	}
	
	public void setAddressId(long id) { // id
		address.setId(id); 
	}
	
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		
		return new StringBuilder("Person [id =").append(id)
						.append(", firstName =").append(firstName)
						.append(", lastName =").append(lastName)
						.append(", email =").append(email)
						.append(", addressId =").append(address.getId())
						.append(", birthDate=").append(birthDate)
						.append(", createdDate=").append(createdDate)
						.append(", address=").append(address).append("]")
						.toString();
	}
	
}