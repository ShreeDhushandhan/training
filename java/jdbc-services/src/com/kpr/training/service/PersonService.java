package com.kpr.training.service;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.kpr.training.constants.Constants;
import com.kpr.training.constants.QueryStatement;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.validator.Validator;

/**
 * Problem Statement
 * 
 * 1.Perform CRUD Operations for Person Service
 * 
 * Entity 
 *  1.Person 
 *  2.PersonService 
 *  3.AppException 
 *  4.ErrorCode
 * 
 * Method Signature 1.public long insert(Person person); 2.public Person
 * readPerson(long id); 3.public List<Person> readAllPerson(); 4.public long
 * update(Person person); 5.public long delete(long id);
 * 
 * Jobs to be Done 1.Create a Address 2.Read an Address 3.Read all Addresses
 * 4.Update an Address 5.Delete an Address
 * 
 * @author UKSD
 *
 */

public class PersonService {

	public AddressService addressService = new AddressService();

	public Person person;

	// Insert Person
	public long create(Person person) {

		nullChecker(person);

		if (person.getAddress() != null) {
			if (person.getAddress().getPostalCode() == 0) {
				throw new AppException(ErrorCodes.E406);
			}
			try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.INSERT_PERSON_QUERY,
					Statement.RETURN_GENERATED_KEYS)) {
				
				validateEmail(person);
				personValidator(person);

				//this.person.getAddress().setId(addressService.create(person.getAddress()));
				ps.setString(1, person.getFirstName());
				ps.setString(2, person.getLastName());
				ps.setString(3, person.getEmail());
				ps.setLong(4, addressService.create(person.getAddress()));
				ps.setDate(5, (Date) person.getBirthDate());

				ResultSet resultSet;

				if (ps.executeUpdate() == 0 || !(resultSet = ps.getGeneratedKeys()).next()) {
					ConnectionService.get().rollback();
					throw new AppException(ErrorCodes.E407);
				}

				return resultSet.getLong(Constants.GENERATED_KEY);

			} catch (Exception e) {
				throw new AppException(ErrorCodes.E406, e);
			}
		} else {

			try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.INSERT_PERSON_QUERY,
					java.sql.Statement.RETURN_GENERATED_KEYS)) {

				statementValueSetter(ps, person);

				ResultSet resultSet = null;

				if (ps.executeUpdate() == 0 && !(resultSet = ps.getGeneratedKeys()).next()) {
					throw new AppException(ErrorCodes.E406);
				}

				return resultSet.getLong(Constants.GENERATED_KEY);

			} catch (Exception e) {
				throw new AppException(ErrorCodes.E406, e);
			}

		}

	}

	// Read Person
	public Person read(long id, boolean includeAddress) {

		Person person = null;

		if (id == 0) {
			throw new AppException(ErrorCodes.E404);
		}

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_PERSON_QUERY)) {
			
			ps.setLong(1, id);

			ResultSet resultSet = ps.executeQuery();

			if (!resultSet.next()) {
				return null;
			}

			person = valueSetter(resultSet);

			if (includeAddress) {
				person.setAddress(addressService.read(person.getAddressId()));
			}

			return person;

		} catch (Exception e) {
			throw new AppException(ErrorCodes.E422, e);
		}

	}

	// ReadAll Person

	public List<Person> readAll() {

		try (ResultSet resultSet = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_PERSON_QUERY)
				.executeQuery()) {

			List<Person> persons = new ArrayList<>();

			while (resultSet.next()) {
				persons.add(valueSetter(resultSet));
			}
			return persons;

		} catch (Exception e) {
			throw new AppException(ErrorCodes.E423, e);
		}
	}

	// Update Person

	public void update(Person person) {

		if (person.getId() == 0) {
			throw new AppException(ErrorCodes.E404);
		}

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.UPDATE_PERSON)) {

			validateEmail(person);
			personValidator(person);

			statementValueSetter(ps, person);

			if (ps.executeUpdate() == 0) {
				throw new AppException(ErrorCodes.E410);
			}

		} catch (Exception e) {
			throw new AppException(ErrorCodes.E410, e);
		}
	}

	// Delete Person

	public void delete(long id) {

		if (id == 0) {
			throw new AppException(ErrorCodes.E404);
		}

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.DELETE_PERSON)) {

			ps.setLong(1, id);

			this.person = read(id, false);

			if (addressIdChecker(this.person.getAddressId())) {
				addressService.delete(this.person.getAddressId());
			}

			ps.executeUpdate();

		} catch (Exception e) {
			throw new AppException(ErrorCodes.E409, e);
		}
	}

	private boolean addressIdChecker(long addressId) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.ADDRESS_ID_CHECKER)) {

			ps.setLong(1, addressId);

			if (ps.executeQuery().getRow() == 1) {
				return false;
			}
			return true;

		} catch (Exception e) {
			throw new AppException(ErrorCodes.E426, e);
		}
	}

	// Unique Checker

	public void validateEmail(Person person) throws Exception {

		PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.EMAIL_CHECKER);
		ps.setString(1, person.getEmail());
		ps.setLong(2, person.getId());

		if (ps.executeQuery().next()) {
			throw new AppException(ErrorCodes.E412);
		}

	}

	private static void nullChecker(Person person) {
		try {
			if (person.getFirstName() == null 
					|| person.getFirstName() == " " 
					|| person.getFirstName() == ""
					// lastName
					|| person.getLastName() == null 
					|| person.getLastName() == " " 
					|| person.getLastName() == ""
					// Email
					|| person.getEmail() == " " 
					|| person.getEmail() == "" 
					|| person.getEmail() == null
					// BirthDate
					|| person.getBirthDate() == null) {
				throw new AppException(ErrorCodes.E433);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E433, e);
		}
	}

	public void createPersonWithCSV(String csvfile) {

		try (CSVParser parser = new CSVParser(new FileReader(csvfile),
				CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim())) {

			PersonService personService = new PersonService();

			for (CSVRecord data : parser) {
				Person person = new Person(data.get(Constants.FIRST_NAME),
						data.get(Constants.LAST_NAME),
						data.get(Constants.EMAIL), 
						(Date) Validator.dateChanger(data.get(3)),
						new Address(data.get(Constants.STREET), 
									data.get(Constants.CITY),
									Integer.parseInt(data.get(Constants.POSTAL_CODE))));
				personService.create(person);

			}
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E406, e);
		}
	}

	private static void personValidator(Person person) throws Exception {

		PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.PERSON_CHECKER);
		ps.setString(1, person.getFirstName());
		ps.setString(2, person.getLastName());

		while (ps.executeQuery().next()) {
			throw new AppException(ErrorCodes.E433);
		}

	}

	private static Person valueSetter(ResultSet resultSet) {

		try {
			return new Person(resultSet.getLong(Constants.ID), 
					resultSet.getString(Constants.FIRST_NAME),
					resultSet.getString(Constants.LAST_NAME), 
					resultSet.getString(Constants.EMAIL),
					resultSet.getDate(Constants.BIRTH_DATE), 
					new Address(resultSet.getLong(Constants.ID)),
							resultSet.getDate(Constants.CREATED_DATE));
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E429, e);
		}

	}

	private static void statementValueSetter(PreparedStatement ps, Person person) {
		try {
			ps.setString(1, person.getFirstName());
			ps.setString(2, person.getLastName());
			ps.setString(3, person.getEmail());
			ps.setDate(5, (Date) person.getBirthDate());
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E428, e);
		}
	}

}
