package com.kpr.training.service;


import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import com.kpr.training.constants.Constants;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Problem Statement
 * 1.Create a Connection Class
 * 
 * Entity
 * 1.ConnectionService
 * 
 * Method Signature
 * 1.public void init();
 * 2.public static Connection get();
 * 3.public static void release();
 * 4.public static void commitRollback(boolean result);
 * 
 * Jobs to be Done
 * 1.Create a method called init() 
 * 2.Create a HikariConfig object called config and pass the ConnectionService.getConfig() method
 * 3.Create a HikariDataSource object called dateSource and pass the config object to dataSource
 * 4.Set the dataSource.getConnection() method to threadPool
 * 
 * 5.Create a method called getConfig()
 * 6.Create a Hikari object called config
 * 7.Create a Properties object called dbProperty
 * 8.Pass the inputStream object with ConnectionService.get object with db.properties file in try
 * 9.Load the inputStream to dbProperty
 * 10.Set the Hikari Properties to config object
 * 11.return config
 * 12.catch the Exception
 *  
 * 13.Create a method called  get()
 * 14.return the threadPool.get() object 
 * 
 * 15.Create a method called release
 * 16.Call the threadPool.remove() method and return it 
 * 
 * 17.Create a method called commitRoolback with boolean as parameter
 * 18.Check whether the boolean is true
 * 		18.1)if yes then Commit the get method
 * 		18.2)Else rollback the get method
 * 
 * Pseudo Code
 * 
 * class ConnectionService {
 *	
 *	public static void init() {
 *		
 *		HikariConfig config = ConnectionService.getConfig();
 *		
 *		try{
 *			HikariDataSource dataSource = new HikariDataSource(config);
 *			threadPool.set(dataSource.getConnection());
 *		} catch (SQLException e) {
 *			e.printStackTrace();
 *		}
 *	}
 *	
 *	private static HikariConfig getConfig() {
 *		
 *		HikariConfig config = new HikariConfig();
 *		
 *		Properties dbProperty = new Properties();
 *		
 *		try (InputStream inputStream = 
 *				ConnectionService.class.getClassLoader().getResourceAsStream("db.properties")) {
 *			
 *			dbProperty.load(inputStream);
 *			config.setJdbcUrl(dbProperty.getProperty(Constants.JDBCURL));
 *			config.setUsername(dbProperty.getProperty(Constants.USER));
 *			config.setPassword(dbProperty.getProperty(Constants.PASSWORD));
 *			config.addDataSourceProperty(Constants.CACHEPREPSTMT
 *					, dbProperty.getProperty(Constants.CACHEPREPSTMT));
 *			config.addDataSourceProperty(Constants.PREPSTMTCACHESIZE
 *					, dbProperty.getProperty(Constants.PREPSTMTCACHESIZE));
 *			config.addDataSourceProperty(Constants.PREPSTMTCACHESQLLIMIT
 *					, dbProperty.getProperty(Constants.PREPSTMTCACHESQLLIMIT));
 *			config.setJdbcUrl(dbProperty.getProperty(Constants.JDBCURL));
 *			
 *			config.setMaximumPoolSize(Constants.MAX_CONNECTION);
 *			
 *			config.setAutoCommit(false);
 *			
 *			return config;
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E401, e);
 *		}
 *		
 *	}
 *	
 *	public static Connection get() { 
 *		return threadPool.get();
 *	}
 *	
 *	public static void release() {
 *		try {
 *	 		threadPool.remove();
 *		} catch(Exception e) {
 *			throw new AppException(ErrorCodes.E416);
 *		}
 *	}
 *	
 *	public static void commitRollback(boolean result) throws SQLException{
 *		if(result) {
 *			get().commit();
 *		} else {
 *			get().rollback();
 *		}
 *	}
 *	
 *}
 * 
 */

public class ConnectionService {
	
	private static ThreadLocal<Connection> threadPool = new ThreadLocal<>();
	
	
	/*public void initConnection() {
		
		Properties dbProperty = new Properties();
		try (InputStream inputStream = 
				ConnectionService.class.getClassLoader().getResourceAsStream("db.properties")) {
			
			dbProperty.load(inputStream);
		
			threadPool.set((DriverManager.getConnection(dbProperty.getProperty(Constants.URL)
												   , dbProperty.getProperty(Constants.USER)
												   , dbProperty.getProperty(Constants.PASSWORD))));
			
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E401, e);
		}
		
	}*/
	
	public static void init() {
		
		HikariConfig config = ConnectionService.getConfig();
		
		try{
			HikariDataSource dataSource = new HikariDataSource(config);
			threadPool.set(dataSource.getConnection());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static HikariConfig getConfig() {
		
		HikariConfig config = new HikariConfig();
		
		Properties dbProperty = new Properties();
		
		try (InputStream inputStream = 
				ConnectionService.class.getClassLoader().getResourceAsStream("db.properties")) {
			
			dbProperty.load(inputStream);
			config.setJdbcUrl(dbProperty.getProperty(Constants.JDBCURL));
			config.setUsername(dbProperty.getProperty(Constants.USER));
			config.setPassword(dbProperty.getProperty(Constants.PASSWORD));
			config.addDataSourceProperty(Constants.CACHEPREPSTMT
					, dbProperty.getProperty(Constants.CACHEPREPSTMT));
			config.addDataSourceProperty(Constants.PREPSTMTCACHESIZE
					, dbProperty.getProperty(Constants.PREPSTMTCACHESIZE));
			config.addDataSourceProperty(Constants.PREPSTMTCACHESQLLIMIT
					, dbProperty.getProperty(Constants.PREPSTMTCACHESQLLIMIT));
			config.setJdbcUrl(dbProperty.getProperty(Constants.JDBCURL));
			
			config.setMaximumPoolSize(Constants.MAX_CONNECTION);
			
			config.setAutoCommit(false);
			
			return config;
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E401, e);
		}
		
	}
	
	public static Connection get() { 
		if(threadPool.get() == null) {
			init();
		}
		return threadPool.get();
	}
	
	public static void release() {
		try {
			threadPool.remove();
		} catch(Exception e) {
			throw new AppException(ErrorCodes.E416);
		}
	}
	
	public static void commitRollback(boolean result) throws SQLException{
		if(result) {
			get().commit();
		} else {
			get().rollback();
		}
	}
}

