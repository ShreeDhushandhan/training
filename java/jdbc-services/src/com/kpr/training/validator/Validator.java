package com.kpr.training.validator;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.kpr.training.constants.QueryStatement;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.service.PersonService;


/**
 * Problem Statement
 * 1.Create a validator Class to validate the person and date
 * 
 * Entity
 * 1.Validator
 * 2.AppException
 * 3.ErrorCode
 * 
 * Method Signature
 * 1.public static Date dateChanger(String date);
 * 
 * Jobs to be Done
 * 1.Create a method called dateChanger with string paramter
 * 2.Create a SimpleDateFormat object called dateFormat with "dd-MM-yyyy" pattern
 * 3.Parse the LocalDate with "dd-MM-yyyy"
 * 4.return the parsed date with dateFormat
 * 5.Catch the Exception
 * 
 * Pseudo Code
 * public class Validator {
 *	
 *	public static Date dateChanger(String date) {
 *		
 *		try {
 *			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
 *			
 *			LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy")
 *					.withResolverStyle(ResolverStyle.STRICT));
 *			
 *			return (Date) dateFormat.parse(date);
 *		} catch (Exception e ) {
 *			throw new AppException(ErrorCodes.E427, e);
 *		}
 *	}
 *}
 * 
 * @author UKSD
 *
 */
public class Validator {
	
	/*public static Date dateChanger(String date) {
		
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			
			LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy")
					.withResolverStyle(ResolverStyle.STRICT));
			
			return (Date) dateFormat.parse(date);
		} catch (Exception e ) {
			throw new AppException(ErrorCodes.E427, e);
		}
	}*/
	
	public static Date dateChanger(String date) {
		try {
			java.util.Date dateFormat = new SimpleDateFormat("dd-MM-yyyy").parse(date);
			java.sql.Date sqlDate = new java.sql.Date(dateFormat.getTime());
			return sqlDate;
		} catch (Exception e ) {
			throw new AppException(ErrorCodes.E427, e);
		}
	}
	
	public static Date dateConverter(String date) {
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date newDate = null;
		dateFormat.setLenient(false);
		
		try {
			dateFormat.parse(date);
			
			newDate = dateFormat.parse(date);
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E427, e);
		}
		return newDate;
	}
}
