Requirement
1.To implement doDelete for AddressService

Entity
1.AddressServlet
2.AddressService

Method Signature
1.public void doDelete(HttpServletRequest req, HttpServletResponse res) throws Exception ;

Jobs to be Done
1.Get the address JSON from Servlet request and store it in addressJSON
2.Prepare the jsonUtil to JsonUtil
3.Parse the addressJSON to addressId of type int
4.Prepare the AddressService Object called addressService
5.invoke the delete method from AddressService and pass the addressId.
6.Parse the HttpServletResponse body.

Pseudo Code

public class AddressServlet {
    
    public void doDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
        int addressId = req.getParameter("id");
        
        AddressService addressService = new AddressService();;
        
        AddressService.delete(addressId);
        
        res.sendResponse();
    }
}