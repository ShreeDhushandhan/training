Requirement
1.To implement the doGet() for Person Servlet

Entity
1.PersonServlet

Method Signature
1.public void doGet(HttpServletRequest req, HttpServletResponse res) throws Exception ;

Jobs to be Done
1.Get the person JSON from servlet request and store it in personJSON
2.Prepare jsonUtil to JsonUtil
3.Parse the personJson to personId of type int and includeAddress of type Boolean
4.Prepare the PersonService object called personService
5.invoke the read method from personService and pass the personId and includeAddress 
  object and store the result in the Person Object called personGot
6.Parse the personGot object to the HttpServletResponse body.

Pseudo Code

public class PersonServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws Exception  {
        int personId = req.getParameter("id");
        Boolean includeAddress = req.getParameter("includeAddress");
        
        PersonService personService = new PersonService();
        
        Person personGot = personService.read(personId, includeAddress);
        
        res.setValue(personGot);
    }
}

}