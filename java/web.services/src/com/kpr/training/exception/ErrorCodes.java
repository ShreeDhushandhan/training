package com.kpr.training.exception;

/**
 * Problem Statement
 * 1.Create ErrorCode with Enum
 * 
 * Entity
 * 1.ErrorCodes
 * 
 * Method Signature
 * 1.ErrorCodes(int code, String message);
 * 2.public int getCode();
 * 3.public String getMessage();
 * 
 * Jobs to be Done
 * 1.Create variable called code of type int
 * 2.Create a variable called message of type String
 * 3.Create a Constructor with code and message as parameter
 * 4.Create another method called getCode of return type int that returns code
 * 5.Create another method called getMessage of return type String that returns message
 * 6.Create Enum type ErrorCodes with Code and Message
 * 
 * Pseudo Code
 * 
 * enum ErrorCodes {
 *	
 *	E401("Connection Failure"),
 *	E402("Postal Code should not be Empty"),
 *	E403("Address Creation Failure"),
 *	E404("The Id Field cannot be Empty"),
 *	E405("Updation Failure"),
 *	E406("Person Creation Failure"),
 *	E407("Person Does not Exist"),
 *	E408("Person id cannot be Empty"),
 *	E409("Person Delete Failure"),
 *	E410("Person Updation Failure"),
 *	E411("Address Not Found"),
 *	E412("Email ID is not Unique"),
 *	E413("Email already Exists"),
 *	E414("Person already Exists"),
 *	E415("Properties Fetch Error"),
 *	E416("Facing Error while Closing Connection"),
 *	E417("Nothing Found...."),
 *	E418("Address Found"),
 *	E419("Address Not Found"),
 *	E420("Person with the Same Name already Exist"),
 *	E421("Date Format does not Match, Valid Format(dd-mm-yyyy)"),
 *	E422("Read Address Failed."),
 *	E423("Read Person Failed.."),
 *	E424("Email Validate Error.."),
 *	E425("Address Read Error.."),
 *	E426("Address Id Retreival Failed"),
 *	E427("Date Format is not Valid"),
 *	E428("Statement value Setting Error"),
 *	E429("Value Setting Error"),
 *	E430("Address Validation Error"),
 *	E431("Address Deletion Error"),
 *	E432("Read All Address Failed"),
 *	E433("Person Validation Error");
 *	
 *	public String message;
 *	
 *	ErrorCodes(String message) {
 *		this.message = message;
 *	} 
 *
 *	public String getMessage() {
 *		return message;
 *	}
 *
 *}
 * @author UKSD
 *
 */

public enum ErrorCodes {
	
	E401("Connection Failure"),
	E402("Postal Code should not be Empty"),
	E403("Address Creation Failure"),
	E404("The Id Field cannot be Empty"),
	E405("Updation Failure"),
	E406("Person Creation Failure"),
	E407("Person Does not Exist"),
	E408("Person id cannot be Empty"),
	E409("Person Delete Failure"),
	E410("Person Updation Failure"),
	E411("Address Not Found"),
	E412("Email ID is not Unique"),
	E413("Email already Exists"),
	E414("Person already Exists"),
	E415("Properties Fetch Error"),
	E416("Facing Error while Closing Connection"),
	E417("Nothing Found...."),
	E418("Address Found"),
	E419("Address Not Found"),
	E420("Person with the Same Name already Exist"),
	E421("Date Format does not Match, Valid Format(dd-mm-yyyy)"),
	E422("Read Address Failed."),
	E423("Read Person Failed.."),
	E424("Email Validate Error.."),
	E425("Address Read Error.."),
	E426("Address Id Retreival Failed"),
	E427("Date Format is not Valid"),
	E428("Statement value Setting Error"),
	E429("Value Setting Error"),
	E430("Address Validation Error"),
	E431("Address Deletion Error"),
	E432("Read All Address Failed"),
	E433("Person Validation Error"),
	E434("User id creation Failure"),
	E435("User creation Failure"),
	E436("User Validation Failure"),
	E437("UserName and Password Validation Failure"),
	E438("Transaction Filter Error"),
	E439("Authentication Filter Error"),
	E440("UserName and Password cannot be Null or Empty"),
	E441("User not Found");
	
	public String message;
	
	ErrorCodes(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
