package com.kpr.training.servlet;

import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.validator.Validator;

public class JsonUtil {
	
	public Object deSerialize(String personJson, Object typeObj) {
		
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		
		return gson.fromJson( personJson, typeObj.getClass());
	}
	
	public String serialize(Object typeObj) {
		
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		
		return gson.toJson(typeObj);
	}

}
