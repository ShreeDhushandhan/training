package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.kpr.training.service.ConnectionService;


import com.kpr.training.constants.Constants;
import com.kpr.training.constants.QueryStatement;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;

/**
 * Problem Statement 
 * 1.Perform CRUD Operations for Address Service
 * 
 * Entity 
 * 1.Address 
 * 2.AddressService 
 * 3.AppException 
 * 4.ErrorCode
 * 
 * Method Signature 
 * 1.public long create(Address address); 
 * 2.public Address read(long addressId); 
 * 3.public List<Address> readAll(); 
 * 4.public void update(Address address); 
 * 5.public void delete(long id); 
 * 6.public List<Address> search(Address address);
 * 7.public long addressChecker(Address address, Connection connection);
 * 8.private void addressValidator(Address address);
 * 9.private Address valueSetter(ResultSet resultSet);
 * 10.private void statementValueSetterAddress(PreparedStatement ps, Address address);
 * 
 * 
 * Jobs to be Done 
 * 1.Create a Address 
 * 2.Read an Address 
 * 3.Read all Addresses
 * 4.Update an Address 
 * 5.Delete an Address 
 * 6.Search Address
 * 
 * 
 * @author UKSD
 *
 */

public class AddressService {

	public long addressId;
	public int updateCount;
	public long deletedCount;
	
	public Address address;


	public long create(Address address){
		
		addressValidator(address);

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.INSERT_ADDRESS_QUERY,
				java.sql.Statement.RETURN_GENERATED_KEYS)) {
			
			long existAddressId = addressChecker(address);

			if (existAddressId != 0) {
				return existAddressId;
			}

			statementValueSetterAddress(ps, address);
			ps.executeUpdate();

			ResultSet resultSet = ps.getGeneratedKeys();

			while (!resultSet.next()) {
				throw new AppException(ErrorCodes.E406);
			}
			return resultSet.getLong(Constants.GENERATED_KEY);

		} catch (Exception e) {
			throw new AppException(ErrorCodes.E403,e);
		}
	}
	// ReadAddress

	public Address read(long addressId) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_ADDRESS_QUERY)){
			
			if (addressId == 0) {
				throw new AppException(ErrorCodes.E404);
			}

			ps.setLong(1, addressId);

			ResultSet resultSet = ps.executeQuery();

			if(!resultSet.next()) {
				return null; 
			}
			
			return valueSetter(resultSet);

		} catch (Exception e) {
			throw new AppException(ErrorCodes.E425, e);
		}
		
	}

	// ReadAlladdress

	public List<Address> readAll() {
		
		try (ResultSet resultSet = ConnectionService.get()
				.prepareStatement(QueryStatement.READ_ALL_ADDRESS_QUERY)
				.executeQuery()) {

			List<Address> addresses = new ArrayList<>();

			while (resultSet.next()) {
				addresses.add(new Address(resultSet.getLong(Constants.ID),
						   resultSet.getString(Constants.STREET),
						   resultSet.getString(Constants.CITY),
						   resultSet.getLong(Constants.POSTAL_CODE)));
			}
			
			return addresses;
			
		} catch (SQLException e) {
			throw new AppException(ErrorCodes.E425, e);
		}
		
	}

	// UpdateAddress

	public void update(Address address) {
		
		addressValidator(address);
		
		try (PreparedStatement ps = ConnectionService.get()
				.prepareStatement(QueryStatement.UPDATE_ADDRESS_QUERY)) {
			
			statementValueSetterAddress(ps, address);
			ps.setLong(4, address.getId());

			if (ps.executeUpdate() == 0) {
				throw new AppException(ErrorCodes.E403);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E405,e);
		}
	}

	// DeleteAddress

	public void delete(long id) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.DELETE_ADDRESS_QUERY)) {

			ps.setLong(1, id);

			if (ps.executeUpdate() == 0) {
				throw new AppException(ErrorCodes.E405);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E431, e);
		}
	}

	public List<Address> search(Address address) {

		addressValidator(address);

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.SEARCH_QUERY)) {

			statementValueSetterAddress(ps, address);

			ResultSet resultSet = ps.executeQuery();
			
			List<Address> addresses = new ArrayList<>();
			
			while(resultSet.next()) {
				this.address = valueSetter(resultSet);
				this.address.setId(resultSet.getLong(Constants.ID));
				addresses.add(this.address);
			}
			
			return addresses;
			
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E432, e);
		}
		
	}

	public long addressChecker(Address address) {
		
		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.ADDRESS_CHECK)) {

			//new method
			statementValueSetterAddress(ps, address);

			ResultSet resultSet = ps.executeQuery();

			while (!resultSet.next()) {
				return 0;
			}
			
			return resultSet.getLong(Constants.ID);
			
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E417, e);
		}
		
	}

	private void addressValidator(Address address) {
		if (address.getPostalCode() == 0 
				|| address.getStreet() == null //trim()
				|| address.getCity() == null
			    || address.getCity() == " "
				|| address.getCity() == ""
				|| address.getStreet() == " "
				|| address.getStreet() == "" ) {
			throw new AppException(ErrorCodes.E430);
		} 
	}
	
	private Address valueSetter(ResultSet resultSet) {
		try {
			return new Address(resultSet.getString(Constants.STREET)
					   ,resultSet.getString(Constants.CITY)
					   ,resultSet.getLong(Constants.POSTAL_CODE));
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E429, e); 
		}
		
	} 
	
	private void statementValueSetterAddress(PreparedStatement ps, Address address) {
		try {
			ps.setString(1, address.getStreet());
			ps.setString(2, address.getCity());
			ps.setLong(3, address.getPostalCode());
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E428, e);
		}
	}
}
