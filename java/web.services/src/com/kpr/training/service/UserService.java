package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.constants.Constants;
import com.kpr.training.constants.QueryStatement;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;
import com.kpr.training.jdbc.model.User;
import com.mysql.cj.jdbc.PreparedStatementWrapper;

public class UserService {

	public User user;

	public long create(User user) {
		
		UserService userService = new UserService();

		if (userService.validateUser(user)) {
			try {
				Connection connection = ConnectionService.get();
				
				PreparedStatement ps = connection.prepareStatement(QueryStatement.INSERT_USER_QUERY,
						Statement.RETURN_GENERATED_KEYS);

				ps.setString(1, user.getRole());
				ps.setString(2, user.getUserName());
				ps.setString(3, user.getPassword());
				
				ResultSet resultSet;

				if (ps.executeUpdate() == 0 ||!(resultSet = ps.getGeneratedKeys()).next()) {
					ConnectionService.commitRollback(false);
					throw new AppException(ErrorCodes.E434);
				}

				ConnectionService.commitRollback(true);
				return resultSet.getLong(Constants.GENERATED_KEY);

			} catch (Exception e) {
				throw new AppException(ErrorCodes.E435);
			}

		} else {
			throw new AppException(ErrorCodes.E436);
		}

	}

	public User read(long id) {
		
		UserService userService = new UserService();

		if (id == 0) {
			throw new AppException(ErrorCodes.E404);
		}

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_USER_QUERY)) {

			ps.setLong(1, id);

			ResultSet resultSet = ps.executeQuery();

			if (!resultSet.next()) {
				return null;
			}

			user = userService.userValueSetter(resultSet);

			return user;
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E429);
		}
	}

	public List<User> readAll() {

		try (ResultSet resultSet  = ConnectionService.get().prepareStatement(QueryStatement.READ_ALLUSER_QUERY).executeQuery()) {

			
			List<User> userList= new ArrayList<>();
			
			if (resultSet.next()) {
				userList.add(userValueSetter(resultSet));
			}

			return userList;
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E429);
		}
	}
	

	public User userValueSetter(ResultSet resultSet) {

		try {
			return new User(resultSet.getLong("id"),
					resultSet.getString("user_name"),
					resultSet.getString("role"));
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E429);
		}
	}

	private boolean validateUser(User user) {
		if (user.getUserName() != null && user.getPassword() != null && user.getRole() != null) {
			return true;
		}
		return false;
	}

	private PreparedStatement statementSetter(User user, PreparedStatement ps) {
		try {
			ps.setString(1, user.getRole());
			ps.setString(2, user.getUserName());
			ps.setString(3, user.getPassword());
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E428, e);
		}

		return ps;
	}
	
	public static void main(String[] args) {
		
		User user = new User("Admin", "dhushandhan_sd", "qqerfw32432");
		
		UserService userService = new UserService();
		
		if((userService.create(user)) != 0) {
			System.out.println("User Created Success");
		} else {
			System.out.println("User Creation Failed");
		}
		
	}

}
