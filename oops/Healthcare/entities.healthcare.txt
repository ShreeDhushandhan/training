ENTITIES

Hospital
Staff
Doctor
Nurse
AlliedHealthStaff
SupportStaff
Patient
Salary
Address
Surgery
Treatment
Medicine
Admission
_______________________________________________________     

class Hospital
	1. hospitalName String
	2. hospitalAddress Address
	3. hospitalContact int

	1. calcSuccessRate()
	2. calcAnnualIncome()
___________________________________________                                     

class Staff
	1. name String
	2. id int
	3. contact int
	4. address Address
	5. salary Salary
	6. role String
	7. experience String
	8. gender String
	9. age int

	1. fetchStaffDetail()
	2. updateStaffDetail()
___________________________________________   
                                  
class Doctor
	1. specialization String
	2. qualification String
	3. training String

	1. attendPatient()
	2. performSurgery()
___________________________________________

class Nurse
	1. nurseType String
	2. qualification String
	3. training String

	1. assistDoctor()
	2. diagnosePatient()
___________________________________________

class AlliedStaff
	1. qualification String
	2. expertise String

	1. assitTreatment()
	2. prepareReport()
___________________________________________

class SupportStaff
	1. staffType String

	1. manageShift()
	2. manageShift(name)
___________________________________________

class Patient
	1. patientId int
	2. patientName String 
	3. patientAddress Address
	4. contact int
	5. treatment Treatment
	6. surgery Surgery
	7. healthIssue String
			
	1. consultDoctor()
	2. payFees()

___________________________________________

class Salary
	1. netSalary int 
	2. incentive int
	3. offDay int
			
	1. calcSalary()	
	2. lossOfPay()
___________________________________________

class Address
	1. doorNo string
	2. street String 
	3. city String 
	4. pincode int	

	1. validateAddress() 	
	2. updateAddress()
___________________________________________

class Surgery
	1. surgeryType String
	2. surgeryResult String
	3. surgeryDate String
	4. surgeryTime String
	5. surgeryDuration String

	1. prepareSurgery()
	2. doAnaesthetic()
___________________________________________

class Treatment
	1. treatmentType String
	2. treatmentDuration String
	3. treatmentResult String
	4. prescription String

	1. diagnosePatient()
	2. manageTreatment()
___________________________________________

class Medicine
	1. drugName String
	2. drugType String
	3. expDate String
			
	1. checkAvail()
	2. getSpecificity()
___________________________________________

class Admission
	1. admitType String
	2. admitTime String
	3. admitDate String
	4. admitPurpose String
		
	1. countDays()
	2. admitBill()






