SELECT emp_id
      ,first_name
      ,surname
      ,dept
      ,address
  FROM database.employee emp
 INNER JOIN database.department dept
    ON emp.dept_id_emp = dept.dept_id
 WHERE dept_id = 1
   AND address = 'Tirupur'