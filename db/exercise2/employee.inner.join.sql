SELECT emp.emp_id
      ,emp.first_name
	  ,emp.surname
      ,emp.dept_id
  FROM employee emp
 INNER JOIN employee empl
    ON emp.emp_id = empl.emp_id
 GROUP BY dept_id;