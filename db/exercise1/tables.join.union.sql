SELECT * 
  FROM database1.students stu
 INNER JOIN database1.college coll
    ON stu.college_id = coll.college_id
 INNER JOIN database1.university univ
    ON coll.university_code = univ.university_code
    
SELECT DISTINCT university_code
  FROM database1.college
 UNION 
SELECT university_code
  FROM database1.university; 
  