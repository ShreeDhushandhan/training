ALTER TABLE `database1`.`university` 
 ADD COLUMN `address` VARCHAR(45) NOT NULL 
      AFTER `university_name`;
      
ALTER TABLE `database1`.`university` 
CHANGE COLUMN `address` `university_id` VARCHAR(45) NOT NULL ;

ALTER TABLE `database1`.`university`
DROP COLUMN university_id;
      
