SELECT university_name
	  ,university_code
  FROM database1.university
 WHERE reputation > 80 
   AND student_count <600000;
 
SELECT university_name
      ,university_code
  FROM database1.university
 WHERE reputation > 80 
    OR student_count <600000; 
 
SELECT university_name
      ,university_code
  FROM database1.university
 WHERE reputation > 80 
   NOT student_count <600000;
 
SELECT university_name
      ,university_code
  FROM database1.university
 WHERE location 
    IN ('Chennai','Trichy');
 
SELECT university_code
      ,university_name,location
  FROM database1.university
 WHERE location 
  LIKE 'Coim%';
 
SELECT university_code
      ,university_name
      ,location
  FROM database1.university
 WHERE location 
  LIKE 'Che_nai';  