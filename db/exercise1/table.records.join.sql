SELECT *
  FROM database1.university univ
 INNER JOIN database1.college coll
	ON univ.university_code = coll.university_code;
    
SELECT *
  FROM database1.university 
 CROSS JOIN database1.college 
    
SELECT *
  FROM database1.university univ
  LEFT JOIN database1.college coll
	ON univ.university_code = coll.university_code;
    
SELECT *
  FROM database1.university univ
 RIGHT JOIN database1.college coll
	ON univ.university_code = coll.university_code;    