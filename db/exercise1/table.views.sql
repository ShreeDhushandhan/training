CREATE VIEW student_details
AS
SELECT id
      ,stu_name
      ,dept
      ,college_id
      ,college_name
  FROM students
 INNER JOIN 
       database1.college
 USING (college_id);