SELECT university_code
      ,university_name
      ,location
  FROM database1.university
 WHERE university_code 
    IN (SELECT university_code
          FROM database1.college
		 WHERE city='Coimbatore');