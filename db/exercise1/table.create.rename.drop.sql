CREATE TABLE `database1`.`university` (
  `university_code` INT NOT NULL
  ,`university_name` VARCHAR(45) NOT NULL
  ,PRIMARY KEY (`university_code`));
  
ALTER TABLE `database1`.`university` 
  RENAME TO  `database1`.`college` ;
  
DROP TABLE university;

