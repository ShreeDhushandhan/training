SELECT college_code
      ,college_name
      ,city
      ,state
      ,year_opened
      ,dept_name
      ,emp_name
  FROM database2.college college
  LEFT JOIN database2.university university
    ON college.univ_code = university.univ_code
 RIGHT JOIN database2.department department
    ON university.univ_code = department.univ_code
  LEFT JOIN database2.employee employee 
    ON college.college_id = employee.college_id
 WHERE dept_name IN ('CSE','IT')
   AND desig_id = '3'
 GROUP BY college_code
 