SELECT roll_no
	  ,stud_name
      ,stud_dob
      ,gender
      ,stud_email
      ,stud_phone
      ,stud_address
      ,dept_name
      ,college_name
  FROM database2.student student
  LEFT JOIN database2.college_department college_department
    ON student.cdept_id = college_department.cdept_id
  LEFT JOIN database2.department department
    ON college_department.udept_code = department.dept_code
  LEFT JOIN database2.college college
    ON college_department.college_id = college.college_id
 WHERE stud_id 
    IN (SELECT stud_id
          FROM database2.semester_result
		 WHERE semester = '8')
 GROUP BY roll_no         