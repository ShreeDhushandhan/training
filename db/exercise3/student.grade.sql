SELECT roll_no
      ,stud_name
      ,gender
      ,stud_email
      ,stud_phone
      ,stud_address
      ,semester
      ,grade
      ,credits
      ,GPA
  FROM database2.student student
  LEFT JOIN database2.semester_result semester_result
    ON student.stud_id = semester_result.stud_id
  LEFT JOIN database2.college college
    ON student.college_id = college.college_id
 ORDER BY college.college_id
   AND semester