SELECT emp_name
      ,dob
      ,email
      ,phone
      ,college_name
      ,city
      ,dept_code
      ,dept_name
      ,desig_name
      ,desig_rank
  FROM database2.employee employee
  LEFT JOIN database2.college college
    ON employee.college_id = college.college_id
  LEFT JOIN database2.college_department
    ON employee.cdept_id_emp = college_department.cdept_id
  LEFT JOIN database2.department department
    ON college_department.udept_code = department.dept_code
  LEFT JOIN database2.designation designation
    ON employee.desig_id = designation.desig_id
 ORDER BY desig_rank