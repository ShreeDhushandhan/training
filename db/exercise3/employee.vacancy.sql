SELECT semester
	  ,desig_name
      ,desig_rank
      ,college_name
      ,dept_name
      ,city
      ,university_name
      ,university_name
  FROM database2.professor_syllabus professor_syllabus
 INNER JOIN database2.syllabus syllabus
    ON professor_syllabus.syllabus_id = syllabus.id
 INNER JOIN database2.college_department college_department
    ON syllabus.cdept_id = college_department.cdept_id
 INNER JOIN database2.department department
    ON college_department.udept_code = department.dept_code
 INNER JOIN database2.employee employee
    ON college_department.college_id = employee.college_id
 INNER JOIN database2.designation designation
    ON employee.desig_id = designation.desig_id
 INNER JOIN database2.college college
	ON college_department.college_id = college.college_id
 INNER JOIN database2.university university
    ON college.univ_code = university.univ_code
 WHERE emp_id IS NULL
 GROUP BY syllabus_id