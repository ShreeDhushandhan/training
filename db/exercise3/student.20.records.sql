SELECT roll_no
      ,stud_name
      ,gender
      ,stud_dob
      ,stud_email
      ,stud_phone
      ,stud_address
      ,college_name
      ,dept_name
      ,emp_name
  FROM database2.student student
  LEFT JOIN database2.college_department college_department
    ON student.cdept_id = college_department.cdept_id
  LEFT JOIN database2.college college
    ON student.college_id = college.college_id
 INNER JOIN database2.department department
    ON college_department.udept_code = department.dept_code
 INNER JOIN database2.employee employee
    ON college.college_id = employee.college_id
 WHERE desig_id = '3'
 GROUP BY roll_no
 LIMIT 20
