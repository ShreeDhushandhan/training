SELECT student.stud_id
	  ,roll_no
      ,stud_name
      ,gender
      ,semester
      ,grade
      ,credits
      ,GPA
  FROM database2.student student
  LEFT JOIN database2.semester_result semester_result
    ON student.stud_id = semester_result.stud_id
 WHERE GPA >8
   AND semester = 8
 ORDER BY stud_id 

SELECT student.stud_id
	  ,roll_no
      ,stud_name
      ,gender
      ,semester
      ,grade
      ,credits
      ,GPA
  FROM database2.student student
  LEFT JOIN database2.semester_result semester_result
    ON student.stud_id = semester_result.stud_id
 WHERE GPA >5
   AND semester = 8
 ORDER BY stud_id 